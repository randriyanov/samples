package com.ra.algorithm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        List<PairDto> pairDto = new ArrayList<>();
        Set<Pair> pairs = new HashSet<>();

        for (int i = 0; i < pairDto.size(); i++) {
            PairDto pairOne = pairDto.get(i);
            for (int j = 0; j < pairDto.size(); j++) {
                PairDto pairTwo = pairDto.get(j);
                if(pairOne != pairTwo) {
                    Pair pair = new Pair(pairOne.getName(), pairTwo.getName());
                    if(!pairs.contains(pair)) {
                        pairOne.getInterests().retainAll(pairTwo.getInterests());
                        pair.setMatching(pairOne.getInterests().size());
                    }
                }
            }
        }
    }
}
