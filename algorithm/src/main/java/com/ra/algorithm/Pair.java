package com.ra.algorithm;

import java.util.Objects;

public class Pair {
    private String firstUser;
    private String secondUser;
    private int matching;

    public Pair(String firstUser, String secondUser) {
        this.firstUser = firstUser;
        this.secondUser = secondUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pair pair = (Pair) o;
        return
                Objects.equals(firstUser, pair.firstUser) &&
                Objects.equals(secondUser, pair.secondUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstUser, secondUser);
    }

    public void setMatching(int matching) {
        this.matching = matching;
    }
}
