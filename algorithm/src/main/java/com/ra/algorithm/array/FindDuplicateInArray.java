package com.ra.algorithm.array;

import java.util.List;

public class FindDuplicateInArray {

    public static void main(String[] args) {

        System.out.println(repeatedNumber(List.of(1, 100, 100)));
    }

    public static int repeatedNumber(final List<Integer> list) {
        if (list.size() <= 1) {
            return -1;
        }
        int max = list.stream().max(Integer::compareTo).get();

        int[] count = new int[max];

        for (int i = 0; i < list.size(); i++) {
            int n = list.get(i) - 1;
            count[n]++;

            if (count[n] > 1) {
                return list.get(i);
            }
        }

        return -1;
    }
}
