package com.ra.algorithm.array;

import java.util.Arrays;

public class SortWithTwoElementsAllowed {

    public void sortSwapTwoNumbers() {

    }
    public static void main(String[] args) {
        int[] arr =  new int[]{1, 3, 2, 6, 5, 8, 7};
        int[] sorted = Arrays.copyOf(arr, arr.length);
        Arrays.sort(sorted);

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 2; j++) {
                int diff = Math.abs(arr[j + 1] - arr[j + 2]);
                if(diff == 1 && arr[j + 1] > arr[j + 2]) {
                    int temp = arr[j + 1];
                    arr[j + 1] = arr[j + 2];
                    arr[j + 2] = temp;
                }
            }
        }
        System.out.println("Arrays could be sorted : " + Arrays.equals(arr, sorted));
    }
}
