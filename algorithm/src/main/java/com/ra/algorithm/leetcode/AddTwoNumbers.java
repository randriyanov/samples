package com.ra.algorithm.leetcode;

import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddTwoNumbers {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }

        @Override
        public String toString() {
            return "ListNode{" +
                    "val=" + val +
                    ", next=" + next +
                    '}';
        }
    }

    public static void main(String[] args) {
        ListNode listOne = fillList(new int[]{2, 4, 3});
        ListNode listTow = fillList(new int[]{5, 6, 4});

        ListNode result = sumRevertedValuesImproved(listOne, listTow);
        System.out.println(result);

    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        List<Integer> vals = new ArrayList<>();

        return null;
    }

    private static ListNode fillList(int[] values) {
        ListNode head = new ListNode(values[0]);
        ListNode next = head;
        for (int i = 1; i < values.length; i++) {
            next.next = new ListNode(values[i]);
            next = next.next;
        }
        return head;
    }

    private static ListNode sumRevertedValuesImproved(ListNode l1, ListNode l2) {
        Deque<Integer> queueL1Nums = new ArrayDeque<>();
        Deque<Integer> queueL2Nums = new ArrayDeque<>();
        for (ListNode i = l1, j = l2; i != null && j != null; i = i.next, j = j.next) {
            queueL1Nums.addFirst(i.val);
            queueL2Nums.addFirst(j.val);
        }
        BigInteger l1Num = new BigInteger(queueL1Nums.stream().map(Object::toString).collect(Collectors.joining()));
        BigInteger l2Num = new BigInteger(queueL2Nums.stream().map(Object::toString).collect(Collectors.joining()));

        int[] newNumber = Stream.of(l1Num.add(l2Num).toString().split("")).mapToInt(Integer::valueOf).toArray();
        int[] reverted = new int[newNumber.length];
        for (int i = 0; i < newNumber.length ; i++) {
            reverted[i] = newNumber[newNumber.length - i - 1];
        }
        return fillList(reverted);
    }

    private static ListNode sumRevertedValues(ListNode l1, ListNode l2) {
        String firstNum = "";
        String secondNum = "";
        for (ListNode i = l1, j = l2; i != null && j != null; i = i.next, j = j.next) {
            firstNum = firstNum.concat(String.valueOf(i.val));
            secondNum = secondNum.concat(String.valueOf(j.val));
        }
        BigInteger numbOne = new BigInteger(firstNum);
        BigInteger numbTwo = new BigInteger(secondNum);

        Long numberOne = Long.valueOf(firstNum);
        Long numberTwo = Long.valueOf(secondNum);
        Long reverseNumberOne = reverseNumber(numberOne);
        Long reverseNumberTwo = reverseNumber(numberTwo);
        long sum = reverseNumberOne + reverseNumberTwo;
        String[] sumString = String.valueOf(sum).split("");
        int[] values = new int[sumString.length];
        for (int i = 0; i < sumString.length; i++) {
            values[i] = Integer.valueOf(sumString[i]);
        }

        ListNode head = new ListNode(values[0]);
        ListNode next = head;
        for (int i = 1; i < values.length; i++) {
            next.next = new ListNode(values[i]);
            next = next.next;
        }
        return head;
    }

    private static Long reverseNumber(Long num) {
        long reverse = 0;
        while (num != 0) {
            reverse = reverse * 10;
            reverse = reverse + num % 10;
            num = num / 10;
        }
        return reverse;
    }
}
