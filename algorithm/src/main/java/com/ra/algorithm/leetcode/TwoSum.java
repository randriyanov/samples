package com.ra.algorithm.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {

    public static void main(String[] args) {
        int[] testArray = new int[] {2, 7, 11, 15};
        Arrays.stream(twoSum(testArray, 9)).forEach(System.out::println);
    }

    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> duplicates = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int diff = target - nums[i];
            if (!duplicates.containsKey(diff)) {
                duplicates.put(nums[i], i);
            } else {
                return new int[]{duplicates.get(diff), i};
            }
        }
        return new int[]{};
    }
}
