package com.ra.algorithm.leetcode.mock1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RomanNumbers {

    private static Map<String, Integer> numeralsMap = new HashMap<>();

    static {
        numeralsMap.put("I", 1);
        numeralsMap.put("V", 5);
        numeralsMap.put("X", 10);
        numeralsMap.put("L", 50);
        numeralsMap.put("C", 100);
        numeralsMap.put("D", 500);
        numeralsMap.put("M", 1000);
    }

    public static void main(String[] args) {
        System.out.println(romanToInt("LVIII"));
        System.out.println(romanToInt("MCMXCIV"));
        System.out.println(romanToInt("III"));
        System.out.println(romanToInt("IV"));
        System.out.println(romanToInt("IX"));
    }

    public static int romanToInt(String s) {
        int[] romans = Arrays.stream(s.split("")).map(numeralsMap::get).mapToInt(Integer::valueOf).toArray();
        int sum = 0;
        for (int i =  0; i < romans.length; i++) {
            if((i + 1) < romans.length && romans[i] < romans[i + 1]) {
                sum += (romans[i + 1] - romans[i]);
                i++;
            } else {
                sum += romans[i];
            }
        }
        return sum;
    }
}
