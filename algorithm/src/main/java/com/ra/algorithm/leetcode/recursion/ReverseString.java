package com.ra.algorithm.leetcode.recursion;

public class ReverseString {
    public static void main(String[] args) {
        char[] test = {'h', 'e', 'l', 'l', 'o'};
        changeInArray(test);
    }

    public static void reverseString(char[] s) {
        helpFunction(s, s.length - 1);
    }

    public static void changeInArray(char[] s) {
        secondHelpFunction(s, s.length);
    }

    public static void secondHelpFunction(char[] s, int counter) {
        if (counter == s.length / 2) {
            return;
        } else {
            char temp = s[counter];
            s[counter] = s[counter - s.length + 1];
            s[counter - s.length + 1] = temp;
        }
        secondHelpFunction(s, counter - 1);
    }


    public static void helpFunction(char[] s, int counter) {
        if (counter == 0) {
            System.out.print("\"" + s[counter] + "\"");
            return;
        } else {
            System.out.print("\"" + s[counter] + "\"" + ",");
        }
        helpFunction(s, counter - 1);
    }
}
