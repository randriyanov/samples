package com.ra.algorithm.linked;

public class FindMiddleElementLinkedList<E> {

    private Node<E> head;

    private Node<E> tail;

    public E findMiddleElement() {
        Node<E> current = head;
        int length = 0;
        Node<E> middle = head;

        while (current.next != null) {
            length++;
            if (length % 2 == 0) {
                middle = middle.next;
            }
            current = current.next;
        }

        if (length % 2 == 1) {
            middle = middle.next;
        }

        return middle.item;
    }

    public boolean add(E element) {
        Node<E> last = tail;
        Node<E> newNode = new Node<>(last, element, null);
        tail = newNode;
        if(head == null) {
            head = newNode;
        } else {
            last.next = newNode;
        }
        return true;
    }

    public static void main(String[] args) {
        FindMiddleElementLinkedList<String> middleElemnt = new FindMiddleElementLinkedList<>();
        middleElemnt.add("A");
        middleElemnt.add("B");
        middleElemnt.add("C");
        middleElemnt.add("D");
        middleElemnt.add("E");
        middleElemnt.add("F");
        middleElemnt.add("G");
        System.out.println(middleElemnt.findMiddleElement());
    }

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }
}
