package com.ra.algorithm.linked;

public class SandboxLinkedList<E> {

    private Node<E> head;

    private Node<E> tail;

    public boolean add(E element) {
        Node<E> last = tail;
        Node<E> newNode = new Node<>(last, element, null);
        tail = newNode;
        if (head == null) {
            head = newNode;
        } else {
            last.next = newNode;
        }
        return true;
    }
    // L1[A] -> L2[B] -> L3[C] -> L[D] -> L5[E]
    // next = null
    //current = L1[1]
    //prev = null

    //next = L1[A] <- L2[B] -> L3[C] -> L[D] -> L5[E]
    //current.next = L1[A] <- L2[B] <- L3[C] -> L[D] -> L5[E]
    //prev = L1[A]
    //current = L1[A] <- L2[B] -> L3[C] -> L[D] -> L5[E]

    //next = L1[A] <- L2[B] <- L3[C] -> L[D] -> L5[E]
    //current.next = L1[A] <- L2[B] <- L[C]
    //prev = L1[A] <- L2[B]
    //current = L3[C] -> L[D] -> L5[E]
    private SandboxLinkedList<E> reverse() {
        Node<E> current = head;
        Node<E> next = null;
        Node<E> prev = null;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        head = prev;
        return this;
    }

    public E getElement(E element) {
        for (Node<E> i = head; i != null; i = i.next) {
            if (i.item.equals(element)) {
                return element;
            }
        }
        return null;
    }

    public void printElements() {
        for (Node<E> i = head; i != null; i = i.next) {
            System.out.println(i.item);
        }
    }


    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    public static void main(String[] args) {
        SandboxLinkedList<String> sandboxLinkedList = new SandboxLinkedList<>();
        sandboxLinkedList.add("A");
        sandboxLinkedList.add("B");
        sandboxLinkedList.add("C");
        sandboxLinkedList.add("D");
        System.out.println(sandboxLinkedList.getElement("B"));
        sandboxLinkedList.reverse();
        //sandboxLinkedList.printElements();
    }
}
