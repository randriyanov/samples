package com.ra.algorithm.math;

public class FibonacciNumbers {
    public static void main(String[] args) {
        fiboImperative(10);
        System.out.println(fiboRecursive(10));
    }

    private static int fiboRecursive(int number) {
        if(number == 2) {
            return 2;
        }
        if(number == 1) {
            return 1;
        }
        return fiboRecursive(number - 1) + fiboRecursive(number - 2);
    }

    private static int fiboImperative(int number) {
        int prev = 0;
        int next = 1;
        int fibo = 0;
        for (int i = 0; i < number; i++) {
             fibo = prev + next;
             prev = next;
             next = fibo;
            System.out.println(fibo);
        }
        return fibo;
    }
}
