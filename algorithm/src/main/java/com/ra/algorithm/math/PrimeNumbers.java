package com.ra.algorithm.math;

public class PrimeNumbers {

    public static void main(String[] args) {
        System.out.println(isPrimeRecursive(7, 1));
        System.out.println(isPrime(713));
    }

    static boolean isPrime(int n) {
        for (int x = 2; x * x <= n; x++) {
            if (n % x == 0) {
                return false;
            }
        }
        return true;
    }


    static boolean isPrimeRecursive(int n, int i) {

        // Base cases
        if (n <= 2) {
            return n == 2;
        }
        if (n % i == 0) {
            return false;
        }
        if (i * i > n) {
            return true;
        }

        // Check for next divisor
        return isPrimeRecursive(n, i + 1);
    }


    boolean isPrimeOptimized(int n) {
        for (int x = 2; x <= Math.sqrt(n); x++) {
            if (n % x == 0) {
                return false;
            }
        }
        return true;
    }
}
