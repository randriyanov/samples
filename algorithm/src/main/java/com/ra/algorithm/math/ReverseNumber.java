package com.ra.algorithm.math;

public class ReverseNumber {
    public static void main(String[] args) {
        int num = 12321;
        int tempNum = num;
        int reverse = 0;
        while (num != 0) {
            reverse = reverse * 10;
            reverse = reverse + num % 10;
            num = num/10;
        }
        System.out.println(reverse == tempNum);
    }
}
