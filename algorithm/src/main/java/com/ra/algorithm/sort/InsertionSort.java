package com.ra.algorithm.sort;

//Stable Algorithm
//Start with position 0, assume that sorted portion
// we compare the value that was inserted with value in sorted portion
//we do not swap elements we shift elements
public class InsertionSort {

    public static void main(String[] args) {
        int[] intArray = {20, 35, -15, 7, 55, 1, -22};

        for (int i = 1; i < intArray.length; i++) {

            int j;
            int newElement = intArray[i];
            for (j = i; j > 0 && intArray[j - 1] > newElement; j--) {
                intArray[j] = intArray[j - 1];
            }
            intArray[j] = newElement;
        }

        for (int i = 0; i < intArray.length; i++) {
            System.out.println(intArray[i]);
        }
    }
}
