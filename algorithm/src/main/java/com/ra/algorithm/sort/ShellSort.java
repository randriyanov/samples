package com.ra.algorithm.sort;

//The goal to reduce shift amounts
//Does some preliminarily work
//The way you calculate a gap is influence on time complexity
//Unstable
public class ShellSort {
    //Knuth sequences
    public static void main(String[] args) {

        int[] intArray = {20, 35, -15, 7, 55, 1, -22};
        for (int gap = intArray.length / 2; gap > 0; gap /= 2) {


            for (int i = gap; i < intArray.length; i++) {
                int newElement = intArray[i];

                int j = i;

                while (j >= gap && intArray[j - gap] > newElement) {
                    intArray[j] = intArray[j - gap];
                    j -= gap;
                }

                intArray[j] = newElement;
            }
        }

        for (int i = 0; i < intArray.length; i++) {
            System.out.println(intArray[i]);
        }

    }

    private static int factorial(int num) {
        if(num == 0) {
            return 1;
        }
        return num * factorial(num - 1);
    }
}
