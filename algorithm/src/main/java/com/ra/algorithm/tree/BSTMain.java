package com.ra.algorithm.tree;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class BSTMain {
    public static void main(String[] args) {
        Tree<Integer> tree = new Tree<>(10);
        addRecursiveToTree(tree, 15);
        addRecursiveToTree(tree, 9);
        addRecursiveToTree(tree, 21);
        addRecursiveToTree(tree, 13);
        addRecursiveToTree(tree, 10);
        addRecursiveToTree(tree, 8);
        //System.out.println(findClosestValue(tree, 14));

        Node<Integer> searchNode = bfsSearch(tree, 13);
        //System.out.println(searchNode.data);
        System.out.println(size(tree));
    }

    public static int size(Tree tree) {
        Node current = tree.root;
        int size = 0;
        Set<String> set = new HashSet<>();
        Stack<Node> stack = new Stack<>();
        while (!stack.isEmpty() || current != null) {
            if (current != null) {
                stack.push(current);
                current = current.left;
            } else {
                size++;
                current = stack.pop();
                current = current.right;
            }
        }
        return size;
    }

    private static Node<Integer> addRecursive(Node<Integer> current, Integer value) {
        if (current == null) {
            return new Node<>(value);
        }
        if (value < current.data) {
            current.left = addRecursive(current.left, value);
        } else if (value > current.data) {
            current.right = addRecursive(current.right, value);
        }
        return current;
    }

    private static void addRecursiveToTree(Tree<Integer> tree, Integer value) {
        if (tree.root == null) {
            tree.root = new Node<>(value);
        }
        addRecursive(tree.root, value);
    }

    private static Integer findClosestValue(Tree<Integer> tree, Integer target) {
        Node<Integer> root = tree.root;
        if (root.data.equals(target)) {
            return root.data;
        }
        return findClosestValueHelper(root, target, Integer.MAX_VALUE);
    }

    private static Integer findClosestValueHelper(Node<Integer> root, Integer target, Integer closest) {
        if (closest.equals(target) || root == null) {
            return closest;
        }
        Integer closestDiff = Math.abs(target - closest);
        Integer targetDiff = Math.abs(target - root.data);
        if (target < root.data && closestDiff > targetDiff) {
            return findClosestValueHelper(root.left, target, root.data);
        } else if (target > root.data && closestDiff > targetDiff) {
            return findClosestValueHelper(root.right, target, root.data);
        }
        return closest;
    }


    public static <T extends Comparable<T>> Node<T> bfsSearch(Tree<T> tTree, Integer search) {
        Node<T> current = tTree.root;
        while (current != null) {
            if (current.data == search) {
                break;
            } else if (current.data.compareTo((T) search) > 0) {
                current = current.left;
            } else if (current.data.compareTo((T) search) < 0) {
                current = current.right;
            }
        }
        return current;
    }

    public <T extends Comparable<T>> Node<T> bfsSearchRecursive(Tree<T> tTree) {
        return null;
    }

}
