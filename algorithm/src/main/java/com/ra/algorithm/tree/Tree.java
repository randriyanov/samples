package com.ra.algorithm.tree;

public class Tree<T extends Comparable<T>> {
    Node<T> root;

    // Constructors
    public Tree(T key) {
        root = new Node<>(key);
    }

    public Tree() {
        root = null;
    }

}
