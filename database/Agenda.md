1. [Entities and Mappings] +++
2.   [Connecting Database Servers]+++
3. [Entity Relationships] +++
4. [Embeddable Classes] ++
5. [Persisting Collections of Basic and Embeddable Types] +
6. [Entity Inheritance] ++
7. [Composite Primary Key] +
8. [Primary Keys Corresponding to Derived Identities] +
9. [More Mappings](JPA - Cascading Entities) +++
10. [Entity Operations] +++
11. [Locking and Concurrency] +(advanced)
12. [Entity Listeners and Callback Methods] +
13. [Validation]+ hibernate-validation
14. [Entity Graphs] +
15. [Type Conversion of Entity Attributes]+
16. [Second-Level Cache] +(advanced)
17. [Java Persistence Query Language (JPQL)] +++
18. [Named Queries] +++
19. [Stored Procedures] +
20. [Criteria API] ++
 