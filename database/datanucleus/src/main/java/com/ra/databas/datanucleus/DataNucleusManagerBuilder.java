package com.ra.databas.datanucleus;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class DataNucleusManagerBuilder {

    public static EntityManager buildPostgresEntityManager(String persistanceUnit) {
        return Persistence.createEntityManagerFactory(persistanceUnit).createEntityManager();
    }

}
