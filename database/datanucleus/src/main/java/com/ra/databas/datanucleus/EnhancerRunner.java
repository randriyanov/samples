package com.ra.databas.datanucleus;

import org.datanucleus.enhancer.DataNucleusEnhancer;

public class EnhancerRunner {
    public static void main(String[] args) {
        DataNucleusEnhancer enhancer = new DataNucleusEnhancer("JPA", null);
        enhancer.setVerbose(true);
        enhancer.addPersistenceUnit("postgres-unit");
        enhancer.enhance();
    }
}
