package com.ra.databas.datanucleus;

import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class PersistenceServiceTest {

    @Test
    public void testUserSaveInMongoWithNucleus() {
        EntityManager postgresEntityManager = DataNucleusManagerBuilder.buildPostgresEntityManager("mongo-unit");

        EntityTransaction transaction = postgresEntityManager.getTransaction();
        transaction.begin();

        var user = getPersistUser();

        postgresEntityManager.persist(user);
        transaction.commit();
    }

    @Test
    public void testGetUserFromDataBase() {
        EntityManager postgresEntityManager = DataNucleusManagerBuilder.buildPostgresEntityManager("postgres-unit");
        EntityTransaction transaction = postgresEntityManager.getTransaction();
        transaction.begin();

        var user = getPersistUser();

        postgresEntityManager.persist(user);
        transaction.commit();

        user = postgresEntityManager.find(User.class, 1L);
        System.out.println(user);
    }

    @Test
    public void testUserSaveWithNucleus() {
        EntityManager postgresEntityManager = DataNucleusManagerBuilder.buildPostgresEntityManager("postgres-unit");

        EntityTransaction transaction = postgresEntityManager.getTransaction();
        transaction.begin();

        var user = getPersistUser();

        postgresEntityManager.persist(user);
        transaction.commit();
    }


    private User getPersistUser() {
        Item someItem = new Item();

        someItem.setName("Some Item");

        Item otherItem = new Item();
        otherItem.setName("Other Item");

        User someUser = new User();
        someUser.setUsername("johndoe");
        someUser.getBoughtItems().add(someItem); // Link
        someItem.setBuyer(someUser); // Link
        someUser.getBoughtItems().add(otherItem);
        otherItem.setBuyer(someUser);

        return someUser;
    }
}
