package com.db.hibernate.basic;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class MainAddress {
    public static void main(String[] args) {
        User someUser = new User("johndoe");
        Address someAddress = new Address("Some Street 123", "12345", "Some City");
        someAddress.setUser(someUser);
        someUser.setShippingAddress(someAddress);
        // Entity manager obtain
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("postgres-basic-unit");
        EntityManager em = entityManagerFactory.createEntityManager();

        EntityTransaction tx = em.getTransaction();

        tx.begin();
        em.persist(someUser); // Transitive persistence of shippingAddress
        tx.commit();

        Long USER_ID = someUser.getId();
        Long ADDRESS_ID = someAddress.getId();

        User user = em.find(User.class, USER_ID);
        System.out.println(user.getShippingAddress().getZipcode());
//        assertEquals(user.getShippingAddress().getZipcode(), "12345");
//
//        Address address = em.find(Address.class, ADDRESS_ID);
//        assertEquals(address.getZipcode(), "12345");
//
//
        em.close();

    }
}
