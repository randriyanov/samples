package com.db.hibernate.basic;

import javax.persistence.*;

// class is entity when it has two annotations @Entity & @Id
@Entity
@Table(name = "my_object")
public class MyObject {

    @Id
    @SequenceGenerator(name = "ob_sequence", sequenceName = "my_sequence", allocationSize = 1)
    @GeneratedValue(generator = "ob_sequence")
    private long objId;
    private String str;

    public long getObjId() {
        return objId;
    }

    public void setObjId(long objId) {
        this.objId = objId;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

}
