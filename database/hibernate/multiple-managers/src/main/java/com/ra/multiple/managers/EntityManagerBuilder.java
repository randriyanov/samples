package com.ra.multiple.managers;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public class EntityManagerBuilder {

    public static EntityManager buildPostgresEntityManager(String persistanceUnit) {
        return Persistence.createEntityManagerFactory(persistanceUnit).createEntityManager();
    }

    public static EntityManagerFactory buildPostgresEntityManagerFactory(String persistanceUnit) {
        return Persistence.createEntityManagerFactory(persistanceUnit);
    }

    public static void createSchema(String persistanceUnit) {
        generateSchema("create", persistanceUnit);
    }

    public static void dropSchema(String persistanceUnit) {
        generateSchema("drop", persistanceUnit);
    }

    private static void generateSchema(String action, String persistanceUnit) {
        // Take exiting EMF properties, override the schema generation setting on a copy
        Map<String, String> createSchemaProperties = new HashMap<>();
        createSchemaProperties.put(
                "javax.persistence.schema-generation.database.action",
                action
        );
        Persistence.generateSchema(persistanceUnit, createSchemaProperties);
    }
}
