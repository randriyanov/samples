package com.ra.multiple.managers;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class MultipleManagersDao {

    @Test
    public void getUser() {
        EntityManager postgresEntityManager = EntityManagerBuilder.buildPostgresEntityManager("postgres-unit");
        User user = postgresEntityManager.find(User.class, 1L);
        System.out.println(user);
    }

    @Test
    public void saveUserAndItemInPostgreSQL() {
        User user = persistUser("postgres-unit");
        Assertions.assertThat(user.getId()).isEqualTo(1L);
    }

    @Test
    public void saveUserAndItemInMySQL() {
        User user = persistUser("mysql-unit");
        Assertions.assertThat(user.getId()).isEqualTo(1L);
    }

    @Test
    public void saveUserAndItemInMariaDB() {
        User user = persistUser("mariadb-unit");
        Assertions.assertThat(user.getId()).isEqualTo(1L);
    }

    @Test
    public void saveUserAndItemInMSSQL() {
        User user = persistUser("mssql-unit");
        Assertions.assertThat(user.getId()).isEqualTo(1L);
    }


    private User persistUser(String persistanceUnit) {
        Item someItem = new Item();

        someItem.setName("Some Item");

        Item otherItem = new Item();
        otherItem.setName("Other Item");

        User someUser = new User();
        someUser.setUsername("johndoe");
        someUser.getBoughtItems().add(someItem); // Link
        someItem.setBuyer(someUser); // Link
        someUser.getBoughtItems().add(otherItem);
        otherItem.setBuyer(someUser);

        EntityManager postgresEntityManager = EntityManagerBuilder.buildPostgresEntityManager(persistanceUnit);

        EntityTransaction transaction = postgresEntityManager.getTransaction();
        transaction.begin();

        postgresEntityManager.persist(someUser);
        transaction.commit();

        return someUser;
    }
}
