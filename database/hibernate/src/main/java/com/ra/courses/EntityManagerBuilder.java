package com.ra.courses;


import org.hibernate.annotations.common.util.StringHelper;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public class EntityManagerBuilder {
    private static Map<String, String> properties = new HashMap<>();
    private static Map<String, String> replicatedProperties = new HashMap<>();

    static {
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgresPlusDialect");
        properties.put("hibernate.connection.driver_class", "org.postgresql.Driver");
        properties.put("hibernate.connection.url", "jdbc:postgresql://localhost:5430/postgres");
        properties.put("hibernate.connection.password", "postgres");
        properties.put("hibernate.connection.username", "postgres");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.format_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.archive.autodetection", "none");

        replicatedProperties.put("hibernate.dialect", "org.hibernate.dialect.PostgresPlusDialect");
        replicatedProperties.put("hibernate.connection.driver_class", "org.postgresql.Driver");
        replicatedProperties.put("hibernate.connection.url", "jdbc:postgresql://localhost:5433/postgres");
        replicatedProperties.put("hibernate.connection.password", "postgres");
        replicatedProperties.put("hibernate.connection.username", "postgres");
        replicatedProperties.put("hibernate.show_sql", "true");
        replicatedProperties.put("hibernate.format_sql", "true");
        replicatedProperties.put("hibernate.hbm2ddl.auto", "update");
    }

    public static EntityManager buildPostgresEntityManager(String persistanceUnit, String... hbmResources) {
        properties.put(
                "hibernate.hbmxml.files",
                StringHelper.join(",", hbmResources != null ? hbmResources : new String[0])
        );
        return Persistence.createEntityManagerFactory(persistanceUnit, properties).createEntityManager();
    }

    public static EntityManagerFactory buildPostgresEntityManagerFactory(String persistanceUnit, String... hbmResources) {
        properties.put(
                "hibernate.hbmxml.files",
                StringHelper.join(",", hbmResources != null ? hbmResources : new String[0])
        );
        return Persistence.createEntityManagerFactory(persistanceUnit, properties);
    }

    public static EntityManager buildReplicatedPostgresEntityManager(String persistanceUnit, String... hbmResources) {
        replicatedProperties.put(
                "hibernate.hbmxml.files",
                StringHelper.join(",", hbmResources != null ? hbmResources : new String[0])
        );
        return Persistence.createEntityManagerFactory(persistanceUnit, replicatedProperties).createEntityManager();
    }



    public static void createSchema(String persistanceUnit) {
        generateSchema("create", persistanceUnit);
    }

    public static void dropSchema(String persistanceUnit) {
        generateSchema("drop", persistanceUnit);
    }

    private static void generateSchema(String action, String persistanceUnit) {
        // Take exiting EMF properties, override the schema generation setting on a copy
        Map<String, String> createSchemaProperties = new HashMap<>(properties);
        createSchemaProperties.put(
                "javax.persistence.schema-generation.database.action",
                action
        );
        Persistence.generateSchema(persistanceUnit, createSchemaProperties);
    }
}
