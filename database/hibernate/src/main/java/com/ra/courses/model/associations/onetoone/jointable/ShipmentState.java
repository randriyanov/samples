package com.ra.courses.model.associations.onetoone.jointable;

public enum ShipmentState {

    TRANSIT,
    CONFIRMED

}
