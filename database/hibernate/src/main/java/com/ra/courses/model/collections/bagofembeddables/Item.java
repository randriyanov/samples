package com.ra.courses.model.collections.bagofembeddables;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.util.ArrayList;
import java.util.Collection;

@Entity

public class Item {

    @Id
    @GeneratedValue
    protected Long id;

    @ElementCollection

    @CollectionTable(name = "IMAGE")
    @org.hibernate.annotations.CollectionId(
            columns = @Column(name = "IMAGE_ID"),
            type = @org.hibernate.annotations.Type(type = "long"),
            generator = "IMAGE_GENERATOR")
    @SequenceGenerator(name = "IMAGE_GENERATOR", sequenceName = "image_sequence")
    protected Collection<Image> images = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public Collection<Image> getImages() {
        return images;
    }

    public void setImages(Collection<Image> images) {
        this.images = images;
    }

    // ...
}
