package com.ra.courses.model.complexschemas.naturalforeignkey;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

// TODO User class must be serializable, Hibernate bug HHH-7668
@Entity
@Table(name = "USERS")
public class User implements Serializable {

    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(unique = true)
    protected String customerNr;

    protected User() {
    }

    public User(String customerNr) {
        this.customerNr = customerNr;
    }

    public Long getId() {
        return id;
    }

    public String getCustomerNr() {
        return customerNr;
    }

    public void setCustomerNr(String customerNr) {
        this.customerNr = customerNr;
    }

    // ...
}