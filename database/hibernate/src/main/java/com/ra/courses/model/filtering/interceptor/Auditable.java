package com.ra.courses.model.filtering.interceptor;

public interface Auditable {
    public Long getId();
}
