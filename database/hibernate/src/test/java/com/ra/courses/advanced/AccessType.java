package com.ra.courses.advanced;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.advanced.Item;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.junit.jupiter.api.Assertions.assertEquals;



public class AccessType {


    @Test
    public void storeLoadAccessType() throws Exception {
        TransactionManagerSetup transactionManager = new TransactionManagerSetup();
        UserTransaction tx = transactionManager.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("AdvancedPU");
            Item someItem = new Item();
            someItem.setName("Some item");
            someItem.setDescription("This is some description.");
            em.persist(someItem);
            tx.commit();
            em.close();

            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("AdvancedPU");

            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getName(), "AUCTION: Some item");
            tx.commit();
            em.close();
        } finally {
            tx.rollback();
        }
    }

}
