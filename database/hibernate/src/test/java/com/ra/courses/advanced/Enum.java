package com.ra.courses.advanced;

import com.ra.courses.model.advanced.AuctionType;
import com.ra.courses.model.advanced.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Enum extends JPATest {

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("AdvancedPU");
    }

    @Test
    public void storeLoadEnum() throws Exception {
        UserTransaction tx = TM.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = JPA.createEntityManager();
            Item someItem = new Item();
            someItem.setName("Some item");
            someItem.setDescription("This is some description.");
            someItem.setAuctionType(AuctionType.LOWEST_BID);
            em.persist(someItem);
            tx.commit();
            em.close();
            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = JPA.createEntityManager();
            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getAuctionType(), AuctionType.LOWEST_BID);
            tx.commit();
            em.close();
        } finally {
            TM.rollback();
        }
    }

}
