package com.ra.courses.associations;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.maps.mapkey.Bid;
import com.ra.courses.model.associations.maps.mapkey.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.Map;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MapsMapKey  {

    @Test
    public void storeAndLoadItemBids() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("MapsMapKeyPU");

            Item someItem = new Item("Some Item");
            em.persist(someItem);

            Bid someBid = new Bid(new BigDecimal("123.00"), someItem);
            em.persist(someBid);
            someItem.getBids().put(someBid.getId(), someBid); // Optional...

            Bid secondBid = new Bid(new BigDecimal("456.00"), someItem);
            em.persist(secondBid);
            someItem.getBids().put(secondBid.getId(), secondBid); // Optional...

            tx.commit();
            em.close();

            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("MapsMapKeyPU");

            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getBids().size(), 2);

            for (Map.Entry<Long, Bid> entry : item.getBids().entrySet()) {
                // The key is the identifier of each Bid
                assertEquals(entry.getKey(), entry.getValue().getId());
            }

            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("MapsMapKeyPU");
            transactionManagerSetup.stop();
        }
    }

}