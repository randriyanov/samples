package com.ra.courses.associations;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetomany.bag.Bid;
import com.ra.courses.model.associations.onetomany.bag.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OneToManyBag  {


    @Test
    public void storeAndLoadItemBids() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyBagPU");

            Item someItem = new Item("Some Item");
            em.persist(someItem);

            Bid someBid = new Bid(new BigDecimal("123.00"), someItem);
            someItem.getBids().add(someBid);
            someItem.getBids().add(someBid); // No persistent effect!
            em.persist(someBid);

            assertEquals(someItem.getBids().size(), 2);

            tx.commit();
            em.close();

            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyBagPU");
            {
                Item item = em.find(Item.class, ITEM_ID);
                assertEquals(item.getBids().size(), 1);
            }
            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyBagPU");
            {
                Item item = em.find(Item.class, ITEM_ID);

                Bid bid = new Bid(new BigDecimal("456.00"), item);
                item.getBids().add(bid); // No SELECT!
                em.persist(bid);
            }
            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToManyBagPU");
            transactionManagerSetup.stop();
        }
    }

}