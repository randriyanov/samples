package com.ra.courses.associations;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetomany.bidirectional.Bid;
import com.ra.courses.model.associations.onetomany.bidirectional.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.Collection;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OneToManyBidirectional  {


    @Test
    public void storeAndLoadItemBids() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyBidirectionalPU");

            Item someItem = new Item("Some Item");
            em.persist(someItem);

            Bid someBid = new Bid(new BigDecimal("123.00"), someItem);
            someItem.getBids().add(someBid); // Don't forget!
            em.persist(someBid);

            Bid secondBid = new Bid(new BigDecimal("456.00"), someItem);
            someItem.getBids().add(secondBid);
            em.persist(secondBid);

            tx.commit(); // Dirty checking, SQL execution
            em.close();

            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyBidirectionalPU");

            Item item = em.find(Item.class, ITEM_ID); // First SELECT to load ITEM row
            assertEquals(item.getBids().size(), 2); // The size() method triggers second SELECT

            tx.commit();
            em.close();

            // Repeat the same with an explicit query, no collection mapping needed!
            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyBidirectionalPU");

            Collection<Bid> bids =
                    em.createQuery("select b from Bid b where b.item.id = :itemId")
                    .setParameter("itemId", ITEM_ID)
                    .getResultList();
            assertEquals(bids.size(), 2);

            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToManyBidirectionalPU");
            transactionManagerSetup.stop();
        }
    }

}