package com.ra.courses.associations;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetomany.embeddablejointable.Address;
import com.ra.courses.model.associations.onetomany.embeddablejointable.Shipment;
import com.ra.courses.model.associations.onetomany.embeddablejointable.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OneToManyEmbeddableJoinTable {

    @Test
    public void storeAndLoadUsersShipments() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyEmbeddableJoinTablePU");

            User user = new User("johndoe");
            Address deliveryAddress = new Address("Some Street", "12345", "Some City");
            user.setShippingAddress(deliveryAddress);
            em.persist(user);

            Shipment firstShipment = new Shipment();
            deliveryAddress.getDeliveries().add(firstShipment);
            em.persist(firstShipment);

            Shipment secondShipment = new Shipment();
            deliveryAddress.getDeliveries().add(secondShipment);
            em.persist(secondShipment);

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyEmbeddableJoinTablePU");

            Long USER_ID = user.getId();

            User johndoe = em.find(User.class, USER_ID);
            assertEquals(johndoe.getShippingAddress().getDeliveries().size(), 2);

            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToManyEmbeddableJoinTablePU");
            transactionManagerSetup.stop();
        }
    }

}