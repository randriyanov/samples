package com.ra.courses.associations;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetomany.jointable.Item;
import com.ra.courses.model.associations.onetomany.jointable.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OneToManyJoinTable {

    @Test
    public void storeAndLoadItemUsers() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyJoinTablePU");

            Item someItem = new Item("Some Item");
            em.persist(someItem);
            Item otherItem = new Item("Other Item");
            em.persist(otherItem);

            User someUser = new User("johndoe");
            someUser.getBoughtItems().add(someItem); // Link
            someItem.setBuyer(someUser); // Link
            someUser.getBoughtItems().add(otherItem);
            otherItem.setBuyer(someUser);
            em.persist(someUser);

            Item unsoldItem = new Item("Unsold Item");
            em.persist(unsoldItem);

            tx.commit();
            em.close();

            Long ITEM_ID = someItem.getId();
            Long UNSOLD_ITEM_ID = unsoldItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyJoinTablePU");

            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getBuyer().getUsername(), "johndoe");
            assertTrue(item.getBuyer().getBoughtItems().contains(item));

            Item item2 = em.find(Item.class, UNSOLD_ITEM_ID);
            assertNull(item2.getBuyer());

            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToManyJoinTablePU");
            transactionManagerSetup.stop();
        }
    }

}