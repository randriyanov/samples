package com.ra.courses.associations;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetomany.list.Bid;
import com.ra.courses.model.associations.onetomany.list.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.List;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OneToManyList{


    @Test
    public void storeAndLoadItemBids() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyListPU");

            Item someItem = new Item("Some Item");
            em.persist(someItem);

            Bid someBid = new Bid(new BigDecimal("123.00"), someItem);
            someItem.getBids().add(someBid);
            //someItem.getBids().add(someBid); // No persistent effect!
            em.persist(someBid);

            Bid secondBid = new Bid(new BigDecimal("456.00"), someItem);
            someItem.getBids().add(secondBid);
            em.persist(secondBid);

            assertEquals(someItem.getBids().size(), 2);

            tx.commit();
            em.close();

            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyListPU");

            Item item = em.find(Item.class, ITEM_ID);
            List<Bid> bids =  item.getBids();
            assertEquals(bids.size(), 2);
            assertEquals(bids.get(0).getAmount().compareTo(new BigDecimal("123")), 0);
            assertEquals(bids.get(1).getAmount().compareTo(new BigDecimal("456")), 0);

            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToManyListPU");
            transactionManagerSetup.stop();
        }
    }

}