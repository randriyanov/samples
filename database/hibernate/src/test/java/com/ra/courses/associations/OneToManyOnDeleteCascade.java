package com.ra.courses.associations;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetomany.ondeletecascade.Bid;
import com.ra.courses.model.associations.onetomany.ondeletecascade.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.Collection;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OneToManyOnDeleteCascade  {


    @Test
    public void storeAndLoadItemBids() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyOnDeleteCascadePU");

            Item someItem = new Item("Some Item");
            em.persist(someItem); // Saves the bids automatically (later, at flush time)

            Bid someBid = new Bid(new BigDecimal("123.00"), someItem);
            em.persist(someBid);
            someItem.getBids().add(someBid);

            Bid secondBid = new Bid(new BigDecimal("456.00"), someItem);
            em.persist(secondBid);
            someItem.getBids().add(secondBid);

            tx.commit(); // Dirty checking, SQL execution
            em.close();

            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyOnDeleteCascadePU");
            Collection<Bid> bids =
               em.createQuery("select b from Bid b where b.item.id = :itemId")
                  .setParameter("itemId", ITEM_ID)
                  .getResultList();
            assertEquals(bids.size(), 2);
            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyOnDeleteCascadePU");

            Item item = em.find(Item.class, ITEM_ID);
            em.remove(item); // The bids are deleted in the database, any Bid in memory might be outdated

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyOnDeleteCascadePU");
            bids = em.createQuery("select b from Bid b where b.item.id = :itemId")
               .setParameter("itemId", ITEM_ID)
               .getResultList();
            assertEquals(bids.size(), 0); // Bids are gone
            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToManyOnDeleteCascadePU");
            transactionManagerSetup.stop();
        }
    }

}