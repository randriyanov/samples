package com.ra.courses.associations;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetomany.orphanremoval.Bid;
import com.ra.courses.model.associations.onetomany.orphanremoval.Item;
import com.ra.courses.model.associations.onetomany.orphanremoval.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OneToManyOrphanRemoval {


    @Test
    public void storeAndLoadItemBids() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyOrphanRemovalPU");

            User johndoe = new User();
            em.persist(johndoe);

            Item anItem = new Item("Some Item");
            Bid bidA = new Bid(new BigDecimal("123.00"), anItem);
            bidA.setBidder(johndoe);
            anItem.getBids().add(bidA);

            Bid bidB = new Bid(new BigDecimal("456.00"), anItem);
            anItem.getBids().add(bidB);
            bidB.setBidder(johndoe);

            em.persist(anItem);

            tx.commit();
            em.close();
            Long ITEM_ID = anItem.getId();
            Long USER_ID = johndoe.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyOrphanRemovalPU");

            User user = em.find(User.class, USER_ID);
            assertEquals(user.getBids().size(), 2); // User made two bids...

            Item item = em.find(Item.class, ITEM_ID);
            Bid firstBid = item.getBids().iterator().next();
            item.getBids().remove(firstBid); // One bid is removed

            // FAILURE!
            // assertEquals(user.getBids().size(), 1);
            assertEquals(user.getBids().size(), 2); // Still two!

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToManyOrphanRemovalPU");

            item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getBids().size(), 1); // One of the bids is gone
            em.remove(item);
            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToManyOrphanRemovalPU");
            transactionManagerSetup.stop();
        }
    }

}