package com.ra.courses.associations;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetoone.foreigngenerator.Address;
import com.ra.courses.model.associations.onetoone.foreigngenerator.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OneToOneForeignGenerator {

    @Test
    public void storeAndLoadUserAddress() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToOneForeignGeneratorPU");

            User someUser = new User("johndoe");

            Address someAddress = new Address(someUser, // Link
                    "Some Street 123", "12345", "Some City"
                );

            someUser.setShippingAddress(someAddress); // Link

            em.persist(someUser); // Transitive persistence of shippingAddress

            tx.commit();
            em.close();

            Long USER_ID = someUser.getId();
            Long ADDRESS_ID = someAddress.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToOneForeignGeneratorPU");

            User user = em.find(User.class, USER_ID);
            assertEquals(user.getShippingAddress().getZipcode(), "12345");

            Address address = em.find(Address.class, ADDRESS_ID);
            assertEquals(address.getZipcode(), "12345");

            assertEquals(user.getId(), address.getId());

            //assertEquals(address.getUser(), user);

            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToOneForeignGeneratorPU");
            transactionManagerSetup.stop();
        }
    }

}