package com.ra.courses.associations;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetoone.foreignkey.Address;
import com.ra.courses.model.associations.onetoone.foreignkey.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static com.ra.courses.JPAUtility.unwrapCauseOfType;
import static org.junit.jupiter.api.Assertions.*;

public class OneToOneForeignKey  {


    @Test
    public void storeAndLoadUserAddress() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToOneForeignKeyPU");

            User someUser =
                new User("johndoe");

            Address someAddress =
                new Address("Some Street 123", "12345", "Some City");

            someUser.setShippingAddress(someAddress); // Link

            em.persist(someUser); // Transitive persistence of shippingAddress

            tx.commit();
            em.close();
            Long USER_ID = someUser.getId();
            Long ADDRESS_ID = someAddress.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToOneForeignKeyPU");

            User user = em.find(User.class, USER_ID);
            assertEquals(user.getShippingAddress().getZipcode(), "12345");

            Address address = em.find(Address.class, ADDRESS_ID);
            assertEquals(address.getZipcode(), "12345");

            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToOneForeignKeyPU");
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void storeNonUniqueRelationship() throws Throwable {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToOneForeignKeyPU");;

            Address someAddress = new Address("Some Street 123", "12345", "Some City");

            User userOne = new User("johndoe");
            userOne.setShippingAddress(someAddress);
            em.persist(userOne); // OK

            User userTwo = new User("janeroe");
            userTwo.setShippingAddress(someAddress);
            em.persist(userTwo); // Fails, true unique @OneToOne!

            try {
                // Hibernate tries the INSERT but fails
                em.flush();
            } catch (Exception ex) {
                throw unwrapCauseOfType(ex, org.hibernate.exception.ConstraintViolationException.class);
            }
        } finally {
//            EntityManagerBuilder.dropSchema("OneToOneForeignKeyPU");
//            transactionManagerSetup.stop();
        }
    }

}