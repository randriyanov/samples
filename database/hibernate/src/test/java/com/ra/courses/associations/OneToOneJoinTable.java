package com.ra.courses.associations;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetoone.jointable.Item;
import com.ra.courses.model.associations.onetoone.jointable.Shipment;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static com.ra.courses.JPAUtility.unwrapCauseOfType;
import static org.junit.jupiter.api.Assertions.*;

public class OneToOneJoinTable {


    @Test
    public void storeAndLoadUserAddress() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToOneJoinTablePU");

            Shipment someShipment = new Shipment();
            em.persist(someShipment);

            Item someItem = new Item("Some Item");
            em.persist(someItem);

            Shipment auctionShipment = new Shipment(someItem);
            em.persist(auctionShipment);

            tx.commit();
            em.close();

            Long ITEM_ID = someItem.getId();
            Long SHIPMENT_ID = someShipment.getId();
            Long AUCTION_SHIPMENT_ID = auctionShipment.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToOneJoinTablePU");
            ;

            Item item = em.find(Item.class, ITEM_ID);
            Shipment shipment1 = em.find(Shipment.class, SHIPMENT_ID);
            Shipment shipment2 = em.find(Shipment.class, AUCTION_SHIPMENT_ID);

            assertNull(shipment1.getAuction());
            assertEquals(shipment2.getAuction(), item);

            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToOneJoinTablePU");
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void storeNonUniqueRelationship() throws Throwable {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToOneJoinTablePU");

            Item someItem = new Item("Some Item");
            em.persist(someItem);

            Shipment shipment1 = new Shipment(someItem);
            em.persist(shipment1);

            Shipment shipment2 = new Shipment(someItem);
            em.persist(shipment2);

            try {
                // Hibernate tries the INSERT but fails
                em.flush();
            } catch (Exception ex) {
                throw unwrapCauseOfType(ex, org.hibernate.exception.ConstraintViolationException.class);
            }
        } finally {
            EntityManagerBuilder.dropSchema("OneToOneJoinTablePU");
            transactionManagerSetup.stop();
        }

    }
}