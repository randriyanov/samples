package com.ra.courses.associations;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.associations.onetoone.sharedprimarykey.Address;
import com.ra.courses.model.associations.onetoone.sharedprimarykey.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OneToOneSharedPrimaryKey {

    @Test
    public void storeAndLoadUserAddress() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("OneToOneSharedPrimaryKeyPU");;

            Address someAddress =
                new Address("Some Street 123", "12345", "Some City");

            em.persist(someAddress); // Generate identifier value

            User someUser =
                new User(
                    someAddress.getId(), // Assign same identifier value
                    "johndoe"
                );

            em.persist(someUser);

            someUser.setShippingAddress(someAddress); // Optional...

            tx.commit();
            em.close();

            Long USER_ID = someUser.getId();
            Long ADDRESS_ID = someAddress.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("OneToOneSharedPrimaryKeyPU");;

            User user = em.find(User.class, USER_ID);
            assertEquals(user.getShippingAddress().getZipcode(), "12345");

            Address address = em.find(Address.class, ADDRESS_ID);
            assertEquals(address.getZipcode(), "12345");

            assertEquals(user.getId(), address.getId());

            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("OneToOneSharedPrimaryKeyPU");
            transactionManagerSetup.stop();
        }
    }

}