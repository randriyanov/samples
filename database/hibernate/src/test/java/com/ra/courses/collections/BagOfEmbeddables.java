package com.ra.courses.collections;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.collections.bagofembeddables.Image;
import com.ra.courses.model.collections.bagofembeddables.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class BagOfEmbeddables {


    @Test
    public void storeLoadCollection() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("BagOfEmbeddablesPU");

            Item someItem = new Item();

            someItem.getImages().add(new Image(
                    "Foo", "foo.jpg", 640, 480
            ));
            someItem.getImages().add(new Image(
                    null, "bar.jpg", 800, 600 // Columns can be NULL now!
            ));
            someItem.getImages().add(new Image(
                    "Baz", "baz.jpg", 1024, 768
            ));
            someItem.getImages().add(new Image(
                    "Baz", "baz.jpg", 1024, 768
            )); // Duplicate allowed!
            someItem.getImages().add(new Image(
                    "Giz", "giz.jpg", 788, 640
            ));

            em.persist(someItem);
            tx.commit();
            em.close();
            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("BagOfEmbeddablesPU");
            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getImages().size(), 5);
            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("BagOfEmbeddablesPU");
            transactionManagerSetup.stop();
        }
    }

}
