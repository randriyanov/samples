package com.ra.courses.collections;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.collections.bagofstringsorderby.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BagOfStringsOrderBy  {


    @Test
    public void storeLoadCollection() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("BagOfStringsOrderByPU");
            Item someItem = new Item();

            someItem.getImages().add("foo.jpg");
            someItem.getImages().add("bar.jpg");
            someItem.getImages().add("baz.jpg");
            someItem.getImages().add("baz.jpg");

            em.persist(someItem);
            tx.commit();
            em.close();
            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("BagOfStringsOrderByPU");
            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getImages().size(), 4);

            // Iteration order as retrieved from database with ORDER BY clause
            Iterator<String> it = item.getImages().iterator();
            String image;
            image = it.next();
            assertEquals(image, "foo.jpg");
            image = it.next();
            assertEquals(image, "baz.jpg");
            image = it.next();
            assertEquals(image, "baz.jpg");
            image = it.next();
            assertEquals(image, "bar.jpg");

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("BagOfStringsOrderByPU");
            transactionManagerSetup.stop();
        }
    }

}
