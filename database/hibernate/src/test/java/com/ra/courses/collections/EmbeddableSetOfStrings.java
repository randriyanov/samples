package com.ra.courses.collections;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.collections.embeddablesetofstrings.Address;
import com.ra.courses.model.collections.embeddablesetofstrings.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EmbeddableSetOfStrings {

    @Test
    public void storeLoadCollection() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("EmbeddableSetOfStringsPU");

            User user = new User("johndoe");
            Address address = new Address("Some Street", "1234", "Some City");
            user.setAddress(address);

            address.getContacts().add("Foo");
            address.getContacts().add("Bar");
            address.getContacts().add("Baz");

            em.persist(user);
            tx.commit();
            em.close();

            Long USER_ID = user.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("EmbeddableSetOfStringsPU");

            User johndoe = em.find(User.class, USER_ID);
            assertEquals(johndoe.getAddress().getContacts().size(), 3);

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("EmbeddableSetOfStringsPU");
            transactionManagerSetup.stop();
        }
    }

}
