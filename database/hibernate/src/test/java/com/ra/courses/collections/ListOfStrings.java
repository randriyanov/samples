package com.ra.courses.collections;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.collections.listofstrings.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ListOfStrings  {

    @Test
    public void storeLoadCollection() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("ListOfStringsPU");
            Item someItem = new Item();

            someItem.getImages().add("foo.jpg");
            someItem.getImages().add("bar.jpg");
            someItem.getImages().add("baz.jpg");
            someItem.getImages().add("baz.jpg");

            em.persist(someItem);
            tx.commit();
            em.close();
            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("ListOfStringsPU");
            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getImages().size(), 4);
            assertEquals(item.getImages().get(0), "foo.jpg");
            assertEquals(item.getImages().get(1), "bar.jpg");
            assertEquals(item.getImages().get(2), "baz.jpg");
            assertEquals(item.getImages().get(3), "baz.jpg");
            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("ListOfStringsPU");
            transactionManagerSetup.stop();
        }
    }

}
