package com.ra.courses.collections;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.collections.mapofembeddables.Filename;
import com.ra.courses.model.collections.mapofembeddables.Image;
import com.ra.courses.model.collections.mapofembeddables.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MapOfEmbeddables {


    @Test
    public void storeLoadCollection() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("MapOfEmbeddablesPU");
            Item someItem = new Item();

            someItem.getImages().put(
                new Filename("foo", "jpg"),
                new Image("Foo", 640, 480));
            someItem.getImages().put(
                new Filename("bar", "jpg"),
                new Image(null, 800, 600));
            someItem.getImages().put(
                new Filename("baz", "jpg"),
                new Image("Baz", 1024, 768));
            someItem.getImages().put(
                new Filename("baz", "jpg"),
                new Image("Baz", 1024, 768)); // Duplicate key filtered!

            em.persist(someItem);
            tx.commit();
            em.close();
            Long ITEM_ID = someItem.getId();

            tx.begin();
            em =  EntityManagerBuilder.buildPostgresEntityManager("MapOfEmbeddablesPU");
            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getImages().size(), 3);
            assertEquals(item.getImages().get(new Filename("foo","jpg")).getTitle(), "Foo");
            assertEquals(item.getImages().get(new Filename("bar", "jpg")).getTitle(), null);
            assertEquals(item.getImages().get(new Filename("baz", "jpg")), new Image("Baz", 1024, 768));

            // Remove one
            item.getImages().remove(new Filename("foo","jpg"));
            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("MapOfEmbeddablesPU");
            item = em.find(Item.class, ITEM_ID);
            // Should be one less
            assertEquals(item.getImages().size(), 2);
            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("MapOfEmbeddablesPU");
            transactionManagerSetup.stop();
        }
    }

}
