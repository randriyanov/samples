package com.ra.courses.collections;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.collections.mapofstringsembeddables.Image;
import com.ra.courses.model.collections.mapofstringsembeddables.Item;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.util.List;
import java.util.Map;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MapOfStringsEmbeddables {

    @Test
    public void storeLoadCollection() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("MapOfStringsEmbeddablesPU");
            Item someItem = new Item();

            someItem.getImages().put("foo.jpg",
                    new Image("Foo", 640, 480));
            someItem.getImages().put("bar.jpg",
                    new Image(null, 800, 600));
            someItem.getImages().put("baz.jpg",
                    new Image("Baz", 1024, 768));
            someItem.getImages().put("baz.jpg",
                    new Image("Baz", 1024, 768)); // Duplicate key filtered!

            em.persist(someItem);
            tx.commit();
            em.close();
            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("MapOfStringsEmbeddablesPU");
            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getImages().size(), 3);
            assertEquals(item.getImages().get("foo.jpg").getTitle(), "Foo");
            assertEquals(item.getImages().get("bar.jpg").getTitle(), null);
            assertEquals(item.getImages().get("baz.jpg"), new Image("Baz", 1024, 768));
            tx.commit();
            em.close();

            {
                tx.begin();
                em = EntityManagerBuilder.buildPostgresEntityManager("MapOfStringsEmbeddablesPU");
                Query q = em.createQuery(
                    "select value(img)\n" +
                        "    from Item i join i.images img\n" +
                        "    where key(img) like '%.jpg'"
                );
                List<Image> result = q.getResultList();
                assertEquals(result.size(), 3);
                assertTrue(result.get(0) instanceof Image);
                tx.commit();
                em.close();
            }
            {
                tx.begin();
                em = EntityManagerBuilder.buildPostgresEntityManager("MapOfStringsEmbeddablesPU");
                Query q = em.createQuery(
                    "select entry(img)\n" +
                        "    from Item i join i.images img\n" +
                        "    where key(img) like '%.jpg'"
                );
                List<Map.Entry<String, Image>> result = q.getResultList();
                assertEquals(result.size(), 3);
                assertTrue(result.get(0) instanceof Map.Entry);
                assertTrue(result.get(0).getKey().endsWith(".jpg"));
                tx.commit();
                em.close();
            }

        } finally {
            EntityManagerBuilder.dropSchema("MapOfStringsEmbeddablesPU");
            transactionManagerSetup.stop();
        }
    }

}
