package com.ra.courses.collections;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.collections.setofembeddables.Image;
import com.ra.courses.model.collections.setofembeddables.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SetOfEmbeddables {

    @Test
    public void storeLoadCollection() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("SetOfEmbeddablesPU");
            Item someItem = new Item();

            someItem.getImages().add(new Image(
                    "Foo", "foo.jpg", 640, 480
            ));
            someItem.getImages().add(new Image(
                    "Bar", "bar.jpg", 800, 600
            ));
            someItem.getImages().add(new Image(
                    "Baz", "baz.jpg", 1024, 768
            ));
            someItem.getImages().add(new Image(
                    "Baz", "baz.jpg", 1024, 768
            ));
            assertEquals(someItem.getImages().size(), 3);

            em.persist(someItem);
            tx.commit();
            em.close();
            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("SetOfEmbeddablesPU");
            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getImages().size(), 3);
            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("SetOfEmbeddablesPU");
            transactionManagerSetup.stop();
        }
    }

}
