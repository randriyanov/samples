package com.ra.courses.collections;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.collections.setofembeddablesorderby.Image;
import com.ra.courses.model.collections.setofembeddablesorderby.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.Iterator;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SetOfEmbeddablesOrderBy {

    @Test
    public void storeLoadCollection() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("SetOfEmbeddablesOrderByPU");
            Item someItem = new Item();

            someItem.getImages().add(new Image(
                    "Foo", "foo.jpg", 640, 480
            ));
            someItem.getImages().add(new Image(
                    "Bar", "bar.jpg", 800, 600
            ));
            someItem.getImages().add(new Image(
                    "Baz", "baz.jpg", 1024, 768
            ));
            someItem.getImages().add(new Image(
                    "Baz", "baz.jpg", 1024, 768
            ));
            assertEquals(someItem.getImages().size(), 3);

            em.persist(someItem);
            tx.commit();
            em.close();
            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("SetOfEmbeddablesOrderByPU");
            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getImages().size(), 3);

            Iterator<Image> it = item.getImages().iterator();
            assertEquals(it.next().getFilename(), "bar.jpg");
            assertEquals(it.next().getFilename(), "baz.jpg");
            assertEquals(it.next().getFilename(), "foo.jpg");
            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("SetOfEmbeddablesOrderByPU");
            transactionManagerSetup.stop();
        }
    }

}
