package com.ra.courses.collections;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.collections.sortedmapofstrings.Item;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.Iterator;
import java.util.Map;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SortedMapOfStrings {


    @Test
    public void storeLoadCollection() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("SortedMapOfStringsPU");
            Item someItem = new Item();

            someItem.getImages().put("foo.jpg", "Foo");
            someItem.getImages().put("bar.jpg", "Bar");
            someItem.getImages().put("baz.jpg", "WRONG!");
            someItem.getImages().put("baz.jpg", "Baz");

            em.persist(someItem);
            tx.commit();
            em.close();
            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("SortedMapOfStringsPU");
            Item item = em.find(Item.class, ITEM_ID);
            assertEquals(item.getImages().size(), 3);

            // Sorted in-memory with TreeMap
            Iterator<Map.Entry<String,String>> it = item.getImages().entrySet().iterator();
            Map.Entry<String,String> entry;
            entry = it.next();
            assertEquals(entry.getKey(), "foo.jpg");
            assertEquals(entry.getValue(), "Foo");
            entry = it.next();
            assertEquals(entry.getKey(), "baz.jpg");
            assertEquals(entry.getValue(), "Baz");
            entry = it.next();
            assertEquals(entry.getKey(), "bar.jpg");
            assertEquals(entry.getValue(), "Bar");

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("SortedMapOfStringsPU");
            transactionManagerSetup.stop();
        }
    }

}
