package com.ra.courses.complexschemas;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.complexschemas.compositekey.embedded.User;
import com.ra.courses.model.complexschemas.compositekey.embedded.UserId;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompositeKeyEmbeddedId {


    @Test
    public void storeLoad() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("CompositeKeyEmbeddedIdPU");
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();


            {
                UserId id = new UserId("johndoe", "123");
                User user = new User(id);
                em.persist(user);
            }

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("CompositeKeyEmbeddedIdPU");

            {
                UserId id = new UserId("johndoe", "123");
                User user = em.find(User.class, id);
                assertEquals(user.getId().getDepartmentNr(), "123");
            }

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("CompositeKeyEmbeddedIdPU");
            transactionManagerSetup.stop();
        }
    }

}
