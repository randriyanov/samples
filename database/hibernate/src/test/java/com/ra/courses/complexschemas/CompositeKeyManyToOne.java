package com.ra.courses.complexschemas;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.complexschemas.compositekey.manytoone.Item;
import com.ra.courses.model.complexschemas.compositekey.manytoone.User;
import com.ra.courses.model.complexschemas.compositekey.manytoone.UserId;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CompositeKeyManyToOne  {

    @Test
    public void storeLoad() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("CompositeKeyManyToOnePU");

            {
                UserId id = new UserId("johndoe", "123");
                User user = new User(id);
                em.persist(user);

                Item item = new Item("Some Item");
                item.setSeller(user);
                em.persist(item);
            }

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("CompositeKeyManyToOnePU");

            {
                UserId id = new UserId("johndoe", "123");
                User user = em.find(User.class, id);
                assertEquals(user.getId().getDepartmentNr(), "123");

                Item item = (Item)em.createQuery(
                    "select i from Item i where i.seller = :u"
                ).setParameter("u", user).getSingleResult();

                assertEquals(item.getName(), "Some Item");
            }

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("CompositeKeyManyToOnePU");
            transactionManagerSetup.stop();
        }
    }

}
