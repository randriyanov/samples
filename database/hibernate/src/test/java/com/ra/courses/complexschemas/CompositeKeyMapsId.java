package com.ra.courses.complexschemas;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.complexschemas.compositekey.mapsid.Department;
import com.ra.courses.model.complexschemas.compositekey.mapsid.User;
import com.ra.courses.model.complexschemas.compositekey.mapsid.UserId;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CompositeKeyMapsId  {


    @Test
    public void storeLoad() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("CompositeKeyMapsIdPU");

            Long DEPARTMENT_ID;
            {
                Department department = new Department("Sales");
                em.persist(department);

                UserId id = new UserId("johndoe", null); // Null?
                User user = new User(id);
                user.setDepartment(department); // Required!
                em.persist(user);

                DEPARTMENT_ID = department.getId();
            }

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("CompositeKeyMapsIdPU");

            {
                UserId id = new UserId("johndoe", DEPARTMENT_ID);
                User user = em.find(User.class, id);
                assertEquals(user.getDepartment().getName(), "Sales");
            }

            tx.commit();
            em.close();
        } finally {
//            EntityManagerBuilder.dropSchema("CompositeKeyMapsIdPU");
//            transactionManagerSetup.stop();
        }
    }

}
