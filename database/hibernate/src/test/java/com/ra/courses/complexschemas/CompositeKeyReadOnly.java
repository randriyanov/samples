package com.ra.courses.complexschemas;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.complexschemas.compositekey.readonly.Department;
import com.ra.courses.model.complexschemas.compositekey.readonly.User;
import com.ra.courses.model.complexschemas.compositekey.readonly.UserId;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CompositeKeyReadOnly {

    @Test
    public void storeLoad() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("CompositeKeyReadOnlyPU");

            Long DEPARTMENT_ID;
            {
                Department department = new Department("Sales");
                em.persist(department); // Assign PK value

                UserId id = new UserId("johndoe", department.getId()); // Required!
                User user = new User(id);
                em.persist(user);

                assertNull(user.getDepartment()); // Careful!

                DEPARTMENT_ID = department.getId();
            }

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("CompositeKeyReadOnlyPU");

            {
                UserId id = new UserId("johndoe", DEPARTMENT_ID);
                User user = em.find(User.class, id);
                assertEquals(user.getDepartment().getName(), "Sales");
            }

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("CompositeKeyReadOnlyPU");
            transactionManagerSetup.stop();
        }
    }

}
