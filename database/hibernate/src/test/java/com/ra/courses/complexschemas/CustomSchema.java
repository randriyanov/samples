package com.ra.courses.complexschemas;

import com.ra.courses.CalendarUtil;
import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.complexschemas.custom.Bid;
import com.ra.courses.model.complexschemas.custom.Item;
import com.ra.courses.model.complexschemas.custom.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import static com.ra.courses.JPAUtility.unwrapCauseOfType;
import static org.junit.jupiter.api.Assertions.*;

public class CustomSchema {


    // All these tests are testing for failure

    @Test
    public void storeLoadDomainInvalid() throws Throwable {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("CustomSchemaPU");

            // This will fail and therefore validate that we actually
            // have a custom SQL datatype in the DDL
            User user = new User();
            user.setEmail("@invalid.address");
            user.setUsername("someuser");
            em.persist(user);

            try {
                em.flush();
            } catch (Exception ex) {
                throw unwrapCauseOfType(ex, org.hibernate.exception.ConstraintViolationException.class);
            }
        } finally {
            EntityManagerBuilder.dropSchema("CustomSchemaPU");
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void storeLoadCheckColumnInvalid() throws Throwable {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("CustomSchemaPU");

            User user = new User();
            user.setEmail("valid@test.com");
            user.setUsername("adminPretender");
            em.persist(user);

            try {
                em.flush();
            } catch (Exception ex) {
                throw unwrapCauseOfType(ex, org.hibernate.exception.ConstraintViolationException.class);
            }
        } finally {
            EntityManagerBuilder.dropSchema("CustomSchemaPU");
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void storeLoadCheckSingleRowInvalid() throws Throwable {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("CustomSchemaPU");

            // Wrong start/end time
            Item item = new Item("Some Item", CalendarUtil.TOMORROW.getTime(), CalendarUtil.TODAY.getTime());
            em.persist(item);

            try {
                em.flush();
            } catch (Exception ex) {
                throw unwrapCauseOfType(ex, org.hibernate.exception.ConstraintViolationException.class);
            }
        } finally {
            EntityManagerBuilder.dropSchema("CustomSchemaPU");
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void storeLoadUniqueMultiColumnValid() throws Throwable {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("CustomSchemaPU");

            User user = new User();
            user.setEmail("valid@test.com");
            user.setUsername("someuser");
            em.persist(user);

            user = new User();
            user.setEmail("valid@test.com");
            user.setUsername("someuser");
            em.persist(user);

            try {
                em.flush();
            } catch (Exception ex) {
                throw unwrapCauseOfType(ex, org.hibernate.exception.ConstraintViolationException.class);
            }
        } finally {
            EntityManagerBuilder.dropSchema("CustomSchemaPU");
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void storeLoadCheckSubselectValid() throws Throwable {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {

            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("CustomSchemaPU");

            Item item = new Item("Some Item", CalendarUtil.TODAY.getTime(), CalendarUtil.TOMORROW.getTime());
            Bid bid = new Bid(new BigDecimal(1), item);
            bid.setCreatedOn(CalendarUtil.AFTER_TOMORROW.getTime()); // Out of date range of auction
            item.getBids().add(bid);

            em.persist(item);
            em.persist(bid);

            try {
                em.flush();
            } catch (Exception ex) {
                throw unwrapCauseOfType(ex, org.hibernate.exception.ConstraintViolationException.class);
            }
        } finally {
            EntityManagerBuilder.dropSchema("CustomSchemaPU");
            transactionManagerSetup.stop();
        }
    }


    @Test // The control
    public void storeLoadValid() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("CustomSchemaPU");

            User user = new User();
            user.setEmail("valid@test.com");
            user.setUsername("someuser");
            em.persist(user);

            user = new User();
            user.setEmail("valid2@test.com");
            user.setUsername("otheruser");
            em.persist(user);

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("CustomSchemaPU");
            user = em.find(User.class, user.getId());
            assertEquals(user.getUsername(), "otheruser");
            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("CustomSchemaPU");
            transactionManagerSetup.stop();
        }
    }
}
