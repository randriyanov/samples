package com.ra.courses.complexschemas;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.complexschemas.naturalforeignkey.Item;
import com.ra.courses.model.complexschemas.naturalforeignkey.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NaturalForeignKey  {



    @Test
    public void storeLoad() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("NaturalForeignKeyPU");

            Long USER_ID;
            {
                User user = new User("1234");
                em.persist(user);
                
                Item item = new Item("Some Item");
                item.setSeller(user);
                em.persist(item);
                USER_ID = user.getId();
            }

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("NaturalForeignKeyPU");

            {
                User user = em.find(User.class, USER_ID);
                
                Item item = (Item)em.createQuery(
                    "select i from Item i where i.seller = :u"
                ).setParameter("u", user).getSingleResult();
                
                assertEquals(item.getName(), "Some Item");
            }

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("BagOfEmbeddablesPU");
            transactionManagerSetup.stop();
        }
    }

}
