package com.ra.courses.complexschemas;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.complexschemas.naturalprimarykey.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NaturalPrimaryKey{


    @Test
    public void storeLoad() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("NaturalPrimaryKeyPU");
            {
                User user = new User("johndoe");
                em.persist(user);
            }
            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("NaturalPrimaryKeyPU");
            {
                User user = em.find(User.class, "johndoe");
                assertNotNull(user);
            }
            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("NaturalPrimaryKeyPU");
            transactionManagerSetup.stop();
        }
    }

}
