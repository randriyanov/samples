package com.ra.courses.complexschemas;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.complexschemas.secondarytable.Address;
import com.ra.courses.model.complexschemas.secondarytable.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SecondaryTable {



    @Test
    public void storeAndLoadUsers() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("SecondaryTablePU");

            User user = new User();
            user.setUsername("johndoe");
            Address homeAddress = new Address("Some Street 123", "12345", "Some City");
            user.setHomeAddress(homeAddress);
            em.persist(user);

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("SecondaryTablePU");

            User u = em.find(User.class, user.getId());

            assertEquals(u.getUsername(), "johndoe");
            assertEquals(u.getHomeAddress().getStreet(), "Some Street 123");

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("SecondaryTablePU");
            transactionManagerSetup.stop();
        }
    }

}
