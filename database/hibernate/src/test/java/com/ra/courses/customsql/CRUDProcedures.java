package com.ra.courses.customsql;

import com.ra.courses.FetchTestLoadEventListener;
import com.ra.courses.TestData;
import com.ra.courses.model.customsql.procedures.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.ra.courses.JPATest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CRUDProcedures extends JPATest {

    FetchTestLoadEventListener loadEventListener;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("CRUDProceduresPU", "customsql/CRUDProcedures.hbm.xml");
    }

    @Override
    public void afterJPABootstrap() throws Exception {
        loadEventListener = new FetchTestLoadEventListener(JPA.getEntityManagerFactory());
    }

    class CustomSQLTestData {
        TestData users;
    }

    public CustomSQLTestData create() throws Exception {
        UserTransaction tx = TM.getUserTransaction();
        tx.begin();
        EntityManager em = JPA.createEntityManager();

        CustomSQLTestData testData = new CustomSQLTestData();
        testData.users = new TestData(new Long[2]);

        User johndoe = new User("johndoe");
        em.persist(johndoe);
        testData.users.identifiers[0] = johndoe.getId();

        tx.commit();
        em.close();

        return testData;
    }

    @Test
    public void read() throws Exception {
        CustomSQLTestData testData = create();
        Long USER_ID = testData.users.getFirstId();

        UserTransaction tx = TM.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = JPA.createEntityManager();

            {
                User user = em.find(User.class, USER_ID);
                assertEquals(loadEventListener.getLoadCount(User.class), 1);
                assertEquals(user.getId(), USER_ID);
            }
            em.clear();
            loadEventListener.reset();

            tx.commit();
            em.close();
        } finally {
            TM.rollback();
        }
    }

    @Test
    public void update() throws Exception {
        CustomSQLTestData testData = create();
        Long USER_ID = testData.users.getFirstId();

        UserTransaction tx = TM.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = JPA.createEntityManager();

            {
                User user = em.find(User.class, USER_ID);
                user.setUsername("jdoe");
                em.flush();

            }
            em.clear();
            loadEventListener.reset();

            tx.commit();
            em.close();
        } finally {
            TM.rollback();
        }
    }

    @Test
    public void delete() throws Exception {
        CustomSQLTestData testData = create();
        Long USER_ID = testData.users.getFirstId();

        UserTransaction tx = TM.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = JPA.createEntityManager();

            {
                User user = em.find(User.class, USER_ID);
                em.remove(user);
                em.flush();
                em.clear();

                assertNull(em.find(User.class, USER_ID));
            }
            em.clear();
            loadEventListener.reset();

            tx.commit();
            em.close();
        } finally {
            TM.rollback();
        }
    }
}
