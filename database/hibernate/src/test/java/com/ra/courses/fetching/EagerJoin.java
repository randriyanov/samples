package com.ra.courses.fetching;

import com.ra.courses.*;
import com.ra.courses.model.fetching.eagerjoin.Bid;
import com.ra.courses.model.fetching.eagerjoin.Item;
import com.ra.courses.model.fetching.eagerjoin.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EagerJoin  {

    public FetchTestData storeTestData() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingEagerJoinPU");

            Long[] itemIds = new Long[3];
            Long[] userIds = new Long[3];

            User johndoe = new User("johndoe");
            em.persist(johndoe);
            userIds[0] = johndoe.getId();

            User janeroe = new User("janeroe");
            em.persist(janeroe);
            userIds[1] = janeroe.getId();

            User robertdoe = new User("robertdoe");
            em.persist(robertdoe);
            userIds[2] = robertdoe.getId();

            Item item = new Item("Item One", CalendarUtil.TOMORROW.getTime(), johndoe);
            em.persist(item);
            itemIds[0] = item.getId();
            for (int i = 1; i <= 3; i++) {
                Bid bid = new Bid(item, robertdoe, new BigDecimal(9 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            item = new Item("Item Two", CalendarUtil.TOMORROW.getTime(), johndoe);
            em.persist(item);
            itemIds[1] = item.getId();
            for (int i = 1; i <= 1; i++) {
                Bid bid = new Bid(item, janeroe, new BigDecimal(2 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            item = new Item("Item Three", CalendarUtil.AFTER_TOMORROW.getTime(), janeroe);
            em.persist(item);
            itemIds[2] = item.getId();

            tx.commit();
            em.close();

            FetchTestData testData = new FetchTestData();
            testData.items = new TestData(itemIds);
            testData.users = new TestData(userIds);
            return testData;
        }finally {
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void fetchEagerJoin() throws Exception {
        FetchTestData testData = storeTestData();

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingEagerJoinPU");

            Long ITEM_ID = testData.items.getFirstId();

            Item item = em.find(Item.class, ITEM_ID);
            // select i.*, u.*, b.*
            //  from ITEM i
            //   left outer join USERS u on u.ID = i.SELLER_ID
            //   left outer join BID b on b.ITEM_ID = i.ID
            //  where i.ID = ?

            em.detach(item); // Done fetching, no more lazy loading

            // In detached state, bids are available...
            assertEquals(item.getBids().size(), 3);
            assertNotNull(item.getBids().iterator().next().getAmount());

            // .. and the seller
            assertEquals(item.getSeller().getUsername(), "johndoe");

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("FetchingEagerJoinPU");
            transactionManagerSetup.stop();
        }
    }
}
