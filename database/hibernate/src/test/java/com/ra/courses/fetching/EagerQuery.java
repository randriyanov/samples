package com.ra.courses.fetching;

import com.ra.courses.*;
import com.ra.courses.model.fetching.nplusoneselects.Bid;
import com.ra.courses.model.fetching.nplusoneselects.Item;
import com.ra.courses.model.fetching.nplusoneselects.User;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EagerQuery {

    public FetchTestData storeTestData() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingNPlusOneSelectsPU");

            Long[] itemIds = new Long[3];
            Long[] userIds = new Long[3];

            User johndoe = new User("johndoe");
            em.persist(johndoe);
            userIds[0] = johndoe.getId();

            User janeroe = new User("janeroe");
            em.persist(janeroe);
            userIds[1] = janeroe.getId();

            User robertdoe = new User("robertdoe");
            em.persist(robertdoe);
            userIds[2] = robertdoe.getId();

            Item item = new Item("Item One", CalendarUtil.TOMORROW.getTime(), johndoe);
            em.persist(item);
            itemIds[0] = item.getId();
            for (int i = 1; i <= 3; i++) {
                Bid bid = new Bid(item, robertdoe, new BigDecimal(9 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            item = new Item("Item Two", CalendarUtil.TOMORROW.getTime(), johndoe);
            em.persist(item);
            itemIds[1] = item.getId();
            for (int i = 1; i <= 1; i++) {
                Bid bid = new Bid(item, janeroe, new BigDecimal(2 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            item = new Item("Item Three", CalendarUtil.AFTER_TOMORROW.getTime(), janeroe);
            em.persist(item);
            itemIds[2] = item.getId();
            for (int i = 1; i <= 1; i++) {
                Bid bid = new Bid(item, johndoe, new BigDecimal(3 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            tx.commit();
            em.close();

            FetchTestData testData = new FetchTestData();
            testData.items = new TestData(itemIds);
            testData.users = new TestData(userIds);
            return testData;
        } finally {
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void fetchUsers() throws Exception {
        storeTestData();

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            {
                EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingNPlusOneSelectsPU");
                List<Item> items =
                        em.createQuery("select i from Item i join fetch i.seller")
                                .getResultList();
                // select i.*, u.*
                //  from ITEM i
                //   inner join USERS u on u.ID = i.SELLER_ID
                //  where i.ID = ?

                em.close(); // Detach all

                for (Item item : items) {
                    assertNotNull(item.getSeller().getUsername());
                }
            }
            {
                EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingNPlusOneSelectsPU");
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery criteria = cb.createQuery();

                Root<Item> i = criteria.from(Item.class);
                i.fetch("seller");
                criteria.select(i);

                List<Item> items = em.createQuery(criteria).getResultList();

                em.close(); // Detach all

                for (Item item : items) {
                    assertNotNull(item.getSeller().getUsername());
                }
            }
        } finally {
            EntityManagerBuilder.dropSchema("FetchingNPlusOneSelectsPU");
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void fetchBids() throws Exception {
        storeTestData();

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();

            {
                EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingNPlusOneSelectsPU");
                List<Item> items =
                        em.createQuery("select i from Item i left join fetch i.bids")
                                .getResultList();
                // select i.*, b.*
                //  from ITEM i
                //   left outer join BID b on b.ITEM_ID = i.ID
                //  where i.ID = ?

                em.close(); // Detach all

                for (Item item : items) {
                    assertTrue(item.getBids().size() > 0);
                }
            }
            {
                EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingNPlusOneSelectsPU");
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery criteria = cb.createQuery();

                Root<Item> i = criteria.from(Item.class);
                i.fetch("bids", JoinType.LEFT);
                criteria.select(i);

                List<Item> items = em.createQuery(criteria).getResultList();

                em.close(); // Detach all

                for (Item item : items) {
                    assertTrue(item.getBids().size() > 0);
                }
            }
        } finally {
            EntityManagerBuilder.dropSchema("FetchingNPlusOneSelectsPU");
            transactionManagerSetup.stop();
        }
    }

}
