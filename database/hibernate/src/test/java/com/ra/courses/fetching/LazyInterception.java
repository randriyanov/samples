package com.ra.courses.fetching;

import com.ra.courses.*;
import org.hibernate.Hibernate;
import com.ra.courses.model.fetching.interception.Item;
import com.ra.courses.model.fetching.interception.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// TODO: , the new bytecode enhancer is broken, you will not see the lazy loading SQL as outlined here!
public class LazyInterception {

    public FetchTestData storeTestData() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingInterceptionPU");

            Long[] itemIds = new Long[3];
            Long[] userIds = new Long[3];

            User johndoe = new User("johndoe");
            em.persist(johndoe);
            userIds[0] = johndoe.getId();

            User janeroe = new User("janeroe");
            em.persist(janeroe);
            userIds[1] = janeroe.getId();

            User robertdoe = new User("robertdoe");
            em.persist(robertdoe);
            userIds[2] = robertdoe.getId();

            Item item = new Item("Item One", CalendarUtil.TOMORROW.getTime(), johndoe, "Some description.");
            em.persist(item);
            itemIds[0] = item.getId();

            item = new Item("Item Two", CalendarUtil.TOMORROW.getTime(), johndoe, "Some description.");
            em.persist(item);
            itemIds[1] = item.getId();

            item = new Item("Item Three", CalendarUtil.AFTER_TOMORROW.getTime(), janeroe, "Some description.");
            em.persist(item);
            itemIds[2] = item.getId();

            tx.commit();
            em.close();

            FetchTestData testData = new FetchTestData();
            testData.items = new TestData(itemIds);
            testData.users = new TestData(userIds);
            return testData;
        } finally {
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void noUserProxy() throws Exception {
        FetchTestData testData = storeTestData();

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingInterceptionPU");

            Long ITEM_ID = testData.items.getFirstId();
            Long USER_ID = testData.users.getFirstId();

            {
                // Proxies are disabled, getReference() will return an initialized instance
                User user = em.getReference(User.class, USER_ID);
                // select * from USERS where ID = ?

                assertTrue(Hibernate.isInitialized(user));
            }
            em.clear();

            {
                Item item = em.find(Item.class, ITEM_ID);
                // select * from ITEM where ID = ?

                assertEquals(item.getSeller().getId(), USER_ID);
                // select * from USERS where ID = ?
                // Even item.getSeller() would trigger the SELECT!
            }

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("FetchingInterceptionPU");
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void lazyBasic() throws Exception {
        FetchTestData testData = storeTestData();

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingInterceptionPU");

            Long ITEM_ID = testData.items.getFirstId();

            Item item = em.find(Item.class, ITEM_ID);
            // select NAME, AUCTIONEND, ... from ITEM where ID = ?

            // Accessing one loads _all_ lazy properties (description, seller, ...)
            assertTrue(item.getDescription().length() > 0);
            // select DESCRIPTION from ITEM where ID = ?
            // select * from USERS where ID = ?

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("FetchingInterceptionPU");
            transactionManagerSetup.stop();
        }
    }


}
