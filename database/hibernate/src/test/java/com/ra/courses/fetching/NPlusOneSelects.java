package com.ra.courses.fetching;

import com.ra.courses.*;
import com.ra.courses.model.fetching.nplusoneselects.Bid;
import com.ra.courses.model.fetching.nplusoneselects.Item;
import com.ra.courses.model.fetching.nplusoneselects.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NPlusOneSelects {

    public FetchTestData storeTestData() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingNPlusOneSelectsPU");

            Long[] itemIds = new Long[3];
            Long[] userIds = new Long[3];

            User johndoe = new User("johndoe");
            em.persist(johndoe);
            userIds[0] = johndoe.getId();

            User janeroe = new User("janeroe");
            em.persist(janeroe);
            userIds[1] = janeroe.getId();

            User robertdoe = new User("robertdoe");
            em.persist(robertdoe);
            userIds[2] = robertdoe.getId();

            Item item = new Item("Item One", CalendarUtil.TOMORROW.getTime(), johndoe);
            em.persist(item);
            itemIds[0] = item.getId();
            for (int i = 1; i <= 3; i++) {
                Bid bid = new Bid(item, robertdoe, new BigDecimal(9 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            item = new Item("Item Two", CalendarUtil.TOMORROW.getTime(), johndoe);
            em.persist(item);
            itemIds[1] = item.getId();
            for (int i = 1; i <= 1; i++) {
                Bid bid = new Bid(item, janeroe, new BigDecimal(2 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            item = new Item("Item Three", CalendarUtil.AFTER_TOMORROW.getTime(), janeroe);
            em.persist(item);
            itemIds[2] = item.getId();
            for (int i = 1; i <= 1; i++) {
                Bid bid = new Bid(item, johndoe, new BigDecimal(3 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            tx.commit();
            em.close();

            FetchTestData testData = new FetchTestData();
            testData.items = new TestData(itemIds);
            testData.users = new TestData(userIds);
            return testData;
        } finally {
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void fetchUsers() throws Exception {
        storeTestData();
        FetchTestLoadEventListener loadEventListener = new FetchTestLoadEventListener(EntityManagerBuilder.buildPostgresEntityManager("FetchingNPlusOneSelectsPU").getEntityManagerFactory());

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingNPlusOneSelectsPU");

            List<Item> items = em.createQuery("select i from Item i").getResultList();
            // select * from ITEM
            assertEquals(loadEventListener.getLoadCount(Item.class), 3);
            assertEquals(loadEventListener.getLoadCount(User.class), 0);

            for (Item item : items) {
                // Each seller has to be loaded with an additional SELECT
                assertNotNull(item.getSeller().getUsername());
                // select * from USERS where ID = ?
            }
            assertEquals(loadEventListener.getLoadCount(User.class), 2);

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("FetchingNPlusOneSelectsPU");
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void fetchBids() throws Exception {
        storeTestData();
        FetchTestLoadEventListener loadEventListener = new FetchTestLoadEventListener(EntityManagerBuilder.buildPostgresEntityManager("FetchingNPlusOneSelectsPU").getEntityManagerFactory());

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingNPlusOneSelectsPU");

            List<Item> items = em.createQuery("select i from Item i").getResultList();
            // select * from ITEM
            assertEquals(loadEventListener.getLoadCount(Item.class), 3);
            assertEquals(loadEventListener.getLoadCount(Bid.class), 0);

            for (Item item : items) {
                // Each bids collection has to be loaded with an additional SELECT
                assertTrue(item.getBids().size() > 0);
                // select * from BID where ITEM_ID = ?
            }
            assertEquals(loadEventListener.getLoadCount(Bid.class), 5);

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("FetchingNPlusOneSelectsPU");
            transactionManagerSetup.stop();
        }
    }

}
