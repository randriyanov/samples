package com.ra.courses.fetching;

import com.ra.courses.*;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import com.ra.courses.model.fetching.profile.Bid;
import com.ra.courses.model.fetching.profile.Item;
import com.ra.courses.model.fetching.profile.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Profile  {

    public FetchTestData storeTestData() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingProfilePU");

            Long[] itemIds = new Long[3];
            Long[] userIds = new Long[3];

            User johndoe = new User("johndoe");
            em.persist(johndoe);
            userIds[0] = johndoe.getId();

            User janeroe = new User("janeroe");
            em.persist(janeroe);
            userIds[1] = janeroe.getId();

            User robertdoe = new User("robertdoe");
            em.persist(robertdoe);
            userIds[2] = robertdoe.getId();

            Item item = new Item("Item One", CalendarUtil.TOMORROW.getTime(), johndoe);
            em.persist(item);
            itemIds[0] = item.getId();
            for (int i = 1; i <= 3; i++) {
                Bid bid = new Bid(item, robertdoe, new BigDecimal(9 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            item = new Item("Item Two", CalendarUtil.TOMORROW.getTime(), johndoe);
            em.persist(item);
            itemIds[1] = item.getId();
            for (int i = 1; i <= 1; i++) {
                Bid bid = new Bid(item, janeroe, new BigDecimal(2 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            item = new Item("Item Three", CalendarUtil.AFTER_TOMORROW.getTime(), janeroe);
            em.persist(item);
            itemIds[2] = item.getId();
            for (int i = 1; i <= 1; i++) {
                Bid bid = new Bid(item, johndoe, new BigDecimal(3 + i));
                item.getBids().add(bid);
                em.persist(bid);
            }

            tx.commit();
            em.close();

            FetchTestData testData = new FetchTestData();
            testData.items = new TestData(itemIds);
            testData.users = new TestData(userIds);
            return testData;
        } finally {
            transactionManagerSetup.stop();
        }
    }

    @Test
    public void fetchWithProfile() throws Exception {
        FetchTestData testData = storeTestData();

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("FetchingProfilePU");

            Long ITEM_ID = testData.items.getFirstId();

            /* 
                The <code>Item#seller</code> is mapped lazy, so the default fetch plan
                will only retrieve the <code>Item</code> instance.
             */
            Item item = em.find(Item.class, ITEM_ID);

            assertFalse(Hibernate.isInitialized(item.getSeller()));

            em.clear();
            /* 
                You need the Hibernate API to enable a profile, it is then active for any operation in that
                unit of work. Now the <code>Item#seller</code> will be fetched with a join in the same SQL
                statement whenever an <code>Item</code> is loaded with this <code>EntityManager</code>.
             */
            em.unwrap(Session.class).enableFetchProfile(Item.PROFILE_JOIN_SELLER);
            item = em.find(Item.class, ITEM_ID);

            em.clear();
            assertNotNull(item.getSeller().getUsername());

            em.clear();
            /* 
                You can overlay another profile on the same unit of work, now the <code>Item#seller</code>
                and the <code>Item#bids</code> collection will be fetched with a join in the same SQL
                statement whenever an <code>Item</code> is loaded.
             */
            em.unwrap(Session.class).enableFetchProfile(Item.PROFILE_JOIN_BIDS);
            item = em.find(Item.class, ITEM_ID);

            em.clear();
            assertNotNull(item.getSeller().getUsername());
            assertTrue(item.getBids().size() > 0);

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("FetchingProfilePU");
            transactionManagerSetup.stop();
        }
    }


}
