package com.ra.courses.filtering;

import com.ra.courses.JPATest;
import com.ra.courses.model.filtering.callback.CurrentUser;
import com.ra.courses.model.filtering.callback.Item;
import com.ra.courses.model.filtering.callback.Mail;
import com.ra.courses.model.filtering.callback.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Callback extends JPATest {

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("FilteringCallbackPU");
    }

    @Test
    public void notifyPostPersist() throws Throwable {
        UserTransaction tx = TM.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = JPA.createEntityManager();

            {
                User user = new User("johndoe");
                CurrentUser.INSTANCE.set(user); // Thread-local

                em.persist(user);
                assertEquals(Mail.INSTANCE.size(), 0);
                em.flush();
                assertEquals(Mail.INSTANCE.size(), 1);
                assertTrue(Mail.INSTANCE.get(0).contains("johndoe"));
                Mail.INSTANCE.clear();


                Item item = new Item("Foo", user);
                em.persist(item);
                assertEquals(Mail.INSTANCE.size(), 0);
                em.flush();
                assertEquals(Mail.INSTANCE.size(), 1);
                assertTrue(Mail.INSTANCE.get(0).contains("johndoe"));
                Mail.INSTANCE.clear();

                CurrentUser.INSTANCE.set(null);
            }
            em.clear();

        } finally {
            TM.rollback();
        }
    }

}
