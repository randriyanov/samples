package com.ra.courses.inheritance;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.inheritance.joined.BankAccount;
import com.ra.courses.model.inheritance.joined.CreditCard;
import org.junit.jupiter.api.Test;

import javax.transaction.UserTransaction;


public class Joined extends InheritanceCRUD {


    @Override
    protected Object createBankAccount() {
        return new BankAccount(
                "Jane Roe", "445566", "One Percent Bank Inc.", "999"
        );
    }

    @Override
    protected Object createCreditCard() {
        return new CreditCard(
                "John Doe", "1234123412341234", "06", "2015"
        );
    }

    @Test
    public void jdbcBillingDetailsSqlQuery() throws Exception {
        TransactionManagerSetup managerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = managerSetup.getUserTransaction();
        try {
            storeLoadBillingDetails("JoinedPU", tx);
            doJdbcSqlQuery("inheritance/joined/AllQuery.sql.txt", false, "JoinedPU", new String[][]{
                    {"\\d*", "Jane Roe", null, null, null, "445566", "One Percent Bank Inc.", "999", "2"},
                    {"\\d*", "John Doe", "06", "2015", "1234123412341234", null, null, null, "1"}});
        } finally {
//            EntityManagerBuilder.dropSchema("JoinedPU");
//            managerSetup.stop();
        }
    }

    @Test
    public void jdbcCreditCardSqlQuery() throws Exception {
        TransactionManagerSetup managerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = managerSetup.getUserTransaction();
        try {
            storeLoadBillingDetails("JoinedPU", tx);
            doJdbcSqlQuery("inheritance/joined/CreditCard.sql.txt", false, "JoinedPU", new String[][]{
                    {"\\d*", "John Doe", "06", "2015", "1234123412341234"}});
        } finally {
            EntityManagerBuilder.dropSchema("JoinedPU");
            managerSetup.stop();
        }
    }
}
