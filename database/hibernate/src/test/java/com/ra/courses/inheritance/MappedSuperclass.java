package com.ra.courses.inheritance;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.inheritance.mappedsuperclass.BankAccount;
import com.ra.courses.model.inheritance.mappedsuperclass.BillingDetails;
import com.ra.courses.model.inheritance.mappedsuperclass.CreditCard;

import org.junit.jupiter.api.Test;

import javax.transaction.UserTransaction;


public class MappedSuperclass extends InheritanceCRUD {


    @Override
    protected Object createBankAccount() {
        return new BankAccount(
                "Jane Roe", "445566", "One Percent Bank Inc.", "999"
        );
    }

    @Override
    protected Object createCreditCard() {
        return new CreditCard(
                "John Doe", "1234123412341234", "06", "2015"
        );
    }

    @Override
    protected String getBillingDetailsQuery() {
        return "select bd from " + BillingDetails.class.getName() + " bd";
    }

    @Test
    public void jdbcSqlQueryBankAccount() throws Exception {
        TransactionManagerSetup managerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = managerSetup.getUserTransaction();
        try {
            storeLoadBillingDetails("MappedSuperclassPU", tx);
            doJdbcSqlQuery("inheritance/mappedsuperclass/BankAccount.sql.txt",
                    true, "MappedSuperclassPU", new String[]{"\\d*", "Jane Roe", "445566", "One Percent Bank Inc.", "999"});

        } finally {
            EntityManagerBuilder.dropSchema("MappedSuperclassPU");
            managerSetup.stop();
        }

    }

    @Test
    public void jdbcSqlQueryCreditCard() throws Exception {
        TransactionManagerSetup managerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = managerSetup.getUserTransaction();
        try {
            storeLoadBillingDetails("MappedSuperclassPU", tx);
            doJdbcSqlQuery("inheritance/mappedsuperclass/CreditCard.sql.txt",
                    true, "MappedSuperclassPU", new String[]{"\\d*", "John Doe", "1234123412341234", "06", "2015"});
        } finally {
            EntityManagerBuilder.dropSchema("MappedSuperclassPU");
            managerSetup.stop();
        }
    }

}
