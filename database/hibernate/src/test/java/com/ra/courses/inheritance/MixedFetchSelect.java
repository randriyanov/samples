package com.ra.courses.inheritance;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.inheritance.mixed.BankAccount;
import com.ra.courses.model.inheritance.mixed.CreditCard;

import org.junit.jupiter.api.Test;

import javax.transaction.UserTransaction;

public class MixedFetchSelect extends InheritanceCRUD {

    @Override
    protected Object createBankAccount() {
        return new BankAccount(
                "Jane Roe", "445566", "One Percent Bank Inc.", "999"
        );
    }

    @Override
    protected Object createCreditCard() {
        return new CreditCard(
                "John Doe", "1234123412341234", "06", "2015"
        );
    }

    @Test
    public void jdbcBillingDetailsSqlQuery() throws Exception {
        TransactionManagerSetup managerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = managerSetup.getUserTransaction();
        try {
            storeLoadBillingDetails("MixedFetchSelectPU", tx);
            doJdbcSqlQuery("inheritance/mixed/AllQuery.sql.txt", false, "MixedFetchSelectPU", new String[][]{
                    {"\\d*", "Jane Roe", "445566", "One Percent Bank Inc.", "999", null, null, null, "BA"},
                    {"\\d*", "John Doe", null, null, null, "06", "2015", "1234123412341234", "CC"}});
        } finally {
            EntityManagerBuilder.dropSchema("MixedFetchSelectPU");
            managerSetup.stop();
        }
    }

}
