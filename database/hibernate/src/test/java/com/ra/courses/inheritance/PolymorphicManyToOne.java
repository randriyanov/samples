package com.ra.courses.inheritance;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.inheritance.manytoone.BillingDetails;
import com.ra.courses.model.inheritance.manytoone.CreditCard;
import com.ra.courses.model.inheritance.manytoone.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PolymorphicManyToOne {

    @Test
    public void storeAndLoadItemBids() throws Exception {
        TransactionManagerSetup managerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = managerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("PolymorphicManyToOnePU");

            CreditCard cc = new CreditCard(
                    "John Doe", "1234123412341234", "06", "2015"
            );
            User johndoe = new User("johndoe");
            johndoe.setDefaultBilling(cc);

            em.persist(cc);
            em.persist(johndoe);

            tx.commit();
            em.close();

            Long USER_ID = johndoe.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("PolymorphicManyToOnePU");
            {
                User user = em.find(User.class, USER_ID);

                // Invoke the pay() method on a concrete subclass of BillingDetails
                user.getDefaultBilling().pay(123);
                assertEquals(user.getDefaultBilling().getOwner(), "John Doe");
            }

            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("PolymorphicManyToOnePU");
            {
                User user = em.find(User.class, USER_ID);

                BillingDetails bd = user.getDefaultBilling();

                assertFalse(bd instanceof CreditCard);

                // Don't do this, ClassCastException!
                // CreditCard creditCard = (CreditCard) bd;
            }
            {
                User user = em.find(User.class, USER_ID);

                BillingDetails bd = user.getDefaultBilling();

                CreditCard creditCard =
                        em.getReference(CreditCard.class, bd.getId()); // No SELECT!

                assertTrue(bd != creditCard); // Careful!
            }
            tx.commit();
            em.close();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("PolymorphicManyToOnePU");
            {
                User user = (User) em.createQuery(
                        "select u from DemoUser u " +
                                "left join fetch u.defaultBilling " +
                                "where u.id = :id")
                        .setParameter("id", USER_ID)
                        .getSingleResult();

                // No proxy has been used, the BillingDetails instance has been fetched eagerly
                CreditCard creditCard = (CreditCard) user.getDefaultBilling();
            }
            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("PolymorphicManyToOnePU");
            managerSetup.stop();
        }
    }

}