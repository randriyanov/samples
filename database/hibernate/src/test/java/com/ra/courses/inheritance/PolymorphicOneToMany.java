package com.ra.courses.inheritance;


import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.inheritance.associations.onetomany.BankAccount;
import com.ra.courses.model.inheritance.associations.onetomany.BillingDetails;
import com.ra.courses.model.inheritance.associations.onetomany.CreditCard;
import com.ra.courses.model.inheritance.associations.onetomany.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PolymorphicOneToMany {


    @Test
    public void storeAndLoadItemBids() throws Exception {
        TransactionManagerSetup managerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = managerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("PolymorphicOneToManyPU");

            BankAccount ba = new BankAccount(
                "Jane Roe", "445566", "One Percent Bank Inc.", "999"
            );
            CreditCard cc = new CreditCard(
                "John Doe", "1234123412341234", "06", "2015"
            );
            User johndoe = new User("johndoe");

            johndoe.getBillingDetails().add(ba);
            ba.setUser(johndoe);

            johndoe.getBillingDetails().add(cc);
            cc.setUser(johndoe);

            em.persist(ba);
            em.persist(cc);
            em.persist(johndoe);

            tx.commit();
            em.close();

            Long USER_ID = johndoe.getId();

            tx.begin();
            em = EntityManagerBuilder.buildPostgresEntityManager("PolymorphicOneToManyPU");
            {
                User user = em.find(User.class, USER_ID);

                for (BillingDetails billingDetails : user.getBillingDetails()) {
                    billingDetails.pay(123);
                }
                assertEquals(user.getBillingDetails().size(), 2);
            }

            tx.commit();
            em.close();

        } finally {
            EntityManagerBuilder.dropSchema("PolymorphicManyToOnePU");
            managerSetup.stop();
        }
    }

}