package com.ra.courses.inheritance;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.inheritance.singletable.BankAccount;
import com.ra.courses.model.inheritance.singletable.CreditCard;

import org.junit.jupiter.api.Test;

import javax.transaction.UserTransaction;


public class SingleTable extends InheritanceCRUD {

    @Override
    protected Object createBankAccount() {
        return new BankAccount(
                "Jane Roe", "445566", "One Percent Bank Inc.", "999"
        );
    }

    @Override
    protected Object createCreditCard() {
        return new CreditCard(
                "John Doe", "1234123412341234", "06", "2015"
        );
    }

    @Test
    public void jdbcBillingDetailsSqlQuery() throws Exception {
        TransactionManagerSetup managerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = managerSetup.getUserTransaction();
        try {
            storeLoadBillingDetails("SingleTablePU", tx);
            doJdbcSqlQuery("inheritance/singletable/AllQuery.sql.txt", false, "SingleTablePU", new String[][]{
                    {"\\d*", "Jane Roe", null, null, null, "445566", "One Percent Bank Inc.", "999", "BA"},
                    {"\\d*", "John Doe", "06", "2015", "1234123412341234", null, null, null, "CC"}});

        } finally {
//            EntityManagerBuilder.dropSchema("SingleTablePU");
//            managerSetup.stop();
        }
    }

    @Test
    public void jdbcCreditCardSqlQuery() throws Exception {
        TransactionManagerSetup managerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = managerSetup.getUserTransaction();
        try {
            storeLoadBillingDetails("SingleTablePU", tx);
            doJdbcSqlQuery("inheritance/singletable/CreditCard.sql.txt", false, "SingleTablePU", new String[][]{
                    {"\\d*", "John Doe", "06", "2015", "1234123412341234"}});
        } finally {
            EntityManagerBuilder.dropSchema("SingleTablePU");
            managerSetup.stop();
        }
    }
}
