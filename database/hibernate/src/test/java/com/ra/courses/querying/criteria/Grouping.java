package com.ra.courses.querying.criteria;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.querying.Bid;
import com.ra.courses.model.querying.Item;
import com.ra.courses.model.querying.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.List;

import com.ra.courses.JPATest;
import com.ra.courses.querying.QueryingTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Grouping extends QueryingTest {

    @Test
    public void executeQueries() throws Exception {
        storeTestData("QueryingPU");

        CriteriaBuilder cb =
                EntityManagerBuilder.buildPostgresEntityManager("QueryingPU").getCriteriaBuilder();

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("QueryingPU");

            { // Group

                CriteriaQuery criteria = cb.createQuery();
                Root<User> u = criteria.from(User.class);
                criteria.multiselect(
                    u.get("lastname"),
                    cb.count(u)
                );
                criteria.groupBy(u.get("lastname"));

                Query q = em.createQuery(criteria);
                List<Object[]> result = q.getResultList();
                assertEquals(result.size(), 2);
                for (Object[] row : result) {
                    assertTrue(row[0] instanceof String);
                    assertTrue(row[1] instanceof Long);
                }
            }
            em.clear();
            { // Average

                CriteriaQuery criteria = cb.createQuery();
                Root<Bid> b = criteria.from(Bid.class);
                criteria.multiselect(
                    b.get("item").get("name"),
                    cb.avg(b.<BigDecimal>get("amount"))
                );
                criteria.groupBy(b.get("item").get("name"));

                Query q = em.createQuery(criteria);
                List<Object[]> result = q.getResultList();
                assertEquals(result.size(), 2);
                for (Object[] row : result) {
                    assertTrue(row[0] instanceof String);
                    assertTrue(row[1] instanceof Double);
                }
            }
            em.clear();
            { // Average Workaround

                CriteriaQuery criteria = cb.createQuery();
                Root<Bid> b = criteria.from(Bid.class);
                Join<Bid, Item> i = b.join("item");
                criteria.multiselect(
                    i,
                    cb.avg(b.<BigDecimal>get("amount"))
                );
                criteria.groupBy(
                    i.get("id"), i.get("name"), i.get("createdOn"), i.get("auctionEnd"),
                    i.get("auctionType"), i.get("approved"), i.get("buyNowPrice"),
                    i.get("seller")
                );

                Query q = em.createQuery(criteria);
                List<Object[]> result = q.getResultList();
                assertEquals(result.size(), 2);
                for (Object[] row : result) {
                    assertTrue(row[0] instanceof Item);
                    assertTrue(row[1] instanceof Double);
                }
            }
            em.clear();
            {// Having

                CriteriaQuery criteria = cb.createQuery();
                Root<User> u = criteria.from(User.class);
                criteria.multiselect(
                    u.get("lastname"),
                    cb.count(u)
                );
                criteria.groupBy(u.get("lastname"));
                criteria.having(cb.like(u.<String>get("lastname"), "D%"));

                Query q = em.createQuery(criteria);
                List<Object[]> result = q.getResultList();
                assertEquals(result.size(), 1);
                for (Object[] row : result) {
                    assertTrue(row[0] instanceof String);
                    assertTrue(row[1] instanceof Long);
                }
            }
            em.clear();

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("SimplePU");
            transactionManagerSetup.stop();
        }
    }

}
