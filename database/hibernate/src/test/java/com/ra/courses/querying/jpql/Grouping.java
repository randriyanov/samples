package com.ra.courses.querying.jpql;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.querying.Item;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.util.List;

import com.ra.courses.JPATest;
import com.ra.courses.querying.QueryingTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Grouping extends QueryingTest {

    @Test
    public void executeQueries() throws Exception {
        storeTestData("QueryingPU");

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("QueryingPU");

            {
                Query q = em.createNamedQuery("group");
                List<Object[]> result = q.getResultList();
                assertEquals(result.size(), 2);
                for (Object[] row : result) {
                    assertTrue(row[0] instanceof String);
                    assertTrue(row[1] instanceof Long);
                }
            }
            em.clear();
            {
                Query q = em.createNamedQuery("average");
                List<Object[]> result = q.getResultList();
                assertEquals(result.size(), 2);
                for (Object[] row : result) {
                    assertTrue(row[0] instanceof String);
                    assertTrue(row[1] instanceof Double);
                }
            }
            em.clear();
            {
                Query q = em.createNamedQuery("averageWorkaround");
                List<Object[]> result = q.getResultList();
                assertEquals(result.size(), 2);
                for (Object[] row : result) {
                    assertTrue(row[0] instanceof Item);
                    assertTrue(row[1] instanceof Double);
                }
            }
            em.clear();
            {
                Query q = em.createNamedQuery("having");
                List<Object[]> result = q.getResultList();
                assertEquals(result.size(), 1);
                for (Object[] row : result) {
                    assertTrue(row[0] instanceof String);
                    assertTrue(row[1] instanceof Long);
                }
            }
            em.clear();

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("SimplePU");
            transactionManagerSetup.stop();
        }
    }

}
