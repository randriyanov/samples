package com.ra.courses.querying.jpql;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.querying.Bid;
import com.ra.courses.model.querying.Item;
import com.ra.courses.model.querying.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.util.List;

import com.ra.courses.JPATest;
import com.ra.courses.querying.QueryingTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Subselects extends QueryingTest {

    @Test
    public void executeQueries() throws Exception {
        TestDataCategoriesItems testData = storeTestData("QueryingPU");

        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("QueryingPU");
            {
                Query q = em.createNamedQuery("correlated");
                List<User> result = q.getResultList();
                assertEquals(result.size(), 1);
                User user = result.iterator().next();
                assertEquals(user.getId(), testData.users.getFirstId());
            }
            em.clear();
            {
                Query q = em.createNamedQuery("uncorrelated");
                List<Bid> result = q.getResultList();
                assertEquals(result.size(), 2);
            }
            {
                Query q = em.createNamedQuery("exists");
                List<Item> result = q.getResultList();
                assertEquals(result.size(), 2);
            }
            em.clear();
            {
                Query q = em.createNamedQuery("quantifyAll");
                List<Item> result = q.getResultList();
                assertEquals(result.size(), 2);
            }
            em.clear();
            {
                Query q = em.createNamedQuery("quantifyAny");
                List<Item> result = q.getResultList();
                assertEquals(result.size(), 1);
            }
            em.clear();

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("QueryingPU");
            transactionManagerSetup.stop();
        }
    }

}
