package com.ra.courses.simple;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.model.simple.Item;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CRUD {


    @Test
    public void storeAndQueryItems() throws Exception {
        storeAndQueryItems("findItems", "SimplePUNotJTATransactional");
    }

    public void storeAndQueryItems(String queryName, String persistenceUnitName, String... hbmResources) throws Exception {
        EntityManager em = EntityManagerBuilder.buildPostgresEntityManager(persistenceUnitName, hbmResources);
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        Item itemOne = new Item();
        itemOne.setName("Item One");
        itemOne.setAuctionEnd(new Date(System.currentTimeMillis() + 100000));
        em.persist(itemOne);

        Item itemTwo = new Item();
        itemTwo.setName("Item Two");
        itemTwo.setAuctionEnd(new Date(System.currentTimeMillis() + 100000));

        em.persist(itemTwo);

        tx.commit();
        em.close();
        em = EntityManagerBuilder.buildPostgresEntityManager(persistenceUnitName, hbmResources);

        Query q = em.createNamedQuery(queryName);
        List<Item> items = q.getResultList();

        assertEquals(items.size(), 2);

        em.close();
    }

    @AfterEach
    public void after() {
        EntityManagerBuilder.dropSchema("SimplePUNotJTATransactional");
    }
}
