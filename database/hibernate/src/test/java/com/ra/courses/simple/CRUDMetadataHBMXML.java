package com.ra.courses.simple;

import com.ra.courses.EntityManagerBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CRUDMetadataHBMXML extends CRUD {

    @BeforeEach
    public void before() {
        EntityManagerBuilder.dropSchema("SimpleXMLHibernatePU");
    }

    @Test
    @Override
    public void storeAndQueryItems() throws Exception {
       storeAndQueryItems("findItemsHibernate", "SimpleXMLHibernatePU", "simple/Native.hbm.xml");
    }

    @AfterEach
    public void after() {
        EntityManagerBuilder.dropSchema("SimpleXMLHibernatePU");
    }
}
