package com.ra.courses.simple;

import com.ra.courses.EntityManagerBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CRUDMetadataXML extends CRUD {

    @BeforeEach
    public void before() {
        EntityManagerBuilder.dropSchema("SimpleXMLCompletePU");
    }

    @Test
    @Override
    public void storeAndQueryItems() throws Exception {
        super.storeAndQueryItems("findItemsWithHints", "SimpleXMLCompletePU");
    }

    @AfterEach
    public void cleanDb() {
        EntityManagerBuilder.dropSchema("SimpleXMLCompletePU");
    }
}
