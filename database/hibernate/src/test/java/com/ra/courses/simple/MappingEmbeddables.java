package com.ra.courses.simple;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.TransactionManagerSetup;
import com.ra.courses.model.simple.Address;
import com.ra.courses.model.simple.User;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import static com.ra.courses.JPAUtility.unwrapCauseOfType;
import static org.junit.jupiter.api.Assertions.*;

public class MappingEmbeddables {


    @Test
    public void storeAndLoadUsers() throws Exception {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        try {
            EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("SimplePU");
            tx.begin();


            User user = new User();
            user.setUsername("johndoe");
            Address homeAddress = new Address("Some Street 123", "12345", "Some City");
            user.setHomeAddress(homeAddress);
            em.persist(user);

            tx.commit();
            em.close();

            em = EntityManagerBuilder.buildPostgresEntityManager("SimplePU");
            tx.begin();

            User u = em.find(User.class, user.getId());

            assertEquals(u.getUsername(), "johndoe");
            assertEquals(u.getHomeAddress().getStreet(), "Some Street 123");

            tx.commit();
            em.close();
        } finally {
            EntityManagerBuilder.dropSchema("SimplePU");
            transactionManagerSetup.stop();
        }
    }

    //After bean validation is shown
    @Test
    public void storeAndLoadInvalidUsers() throws Throwable {
        TransactionManagerSetup transactionManagerSetup = new TransactionManagerSetup(null, null);
        UserTransaction tx = transactionManagerSetup.getUserTransaction();
        EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("SimplePU");
        try {


            tx.begin();

            User user = new User();
            user.setUsername("johndoe");
            Address homeAddress = new Address("Some Street 123", "12345", null); // NULL city!
            user.setHomeAddress(homeAddress);
            em.persist(user);

            try {
                // Hibernate tries the INSERT but fails
                em.flush();

                // Note: If you try instead with tx.commit() and a flush side-effect, you won't
                // get the ConstraintViolationException. Hibernate will catch it internally and
                // simply mark the transaction for rollback.

            } catch (Exception ex) {
                tx.rollback();//Without it get dead lock
                throw unwrapCauseOfType(ex, org.hibernate.exception.ConstraintViolationException.class);
            }
        }finally {
            EntityManagerBuilder.dropSchema("SimplePU");
            transactionManagerSetup.stop();
        }
    }

}
