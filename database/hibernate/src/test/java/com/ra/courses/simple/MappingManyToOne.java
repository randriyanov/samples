package com.ra.courses.simple;

import com.ra.courses.EntityManagerBuilder;
import com.ra.courses.JPATest;
import com.ra.courses.model.simple.Bid;
import com.ra.courses.model.simple.Item;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MappingManyToOne {

    @BeforeEach
    public void cleanDb() {
        EntityManagerBuilder.dropSchema("SimplePUNotJTATransactional");
    }

    @Test
    public void storeAndLoadBids() throws Exception {
        EntityManager em = EntityManagerBuilder.buildPostgresEntityManager("SimplePUNotJTATransactional");
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        // Store in one persistence context (transaction)
        Item anItem = new Item();
        anItem.setName("Example Item");

        Bid firstBid = new Bid(new BigDecimal("123.00"), anItem);

        Bid secondBid = new Bid(new BigDecimal("456.00"), anItem);

        // Order is important here, Hibernate isn't smart enough anymore!
        em.persist(anItem);
        em.persist(firstBid);
        em.persist(secondBid);

        tx.commit();
        em.close();


        em = EntityManagerBuilder.buildPostgresEntityManager("SimplePUNotJTATransactional");
        tx = em.getTransaction();
        tx.begin();
        Long BID_ID = firstBid.getId();

        // Load in another persistence context
        Bid someBid = em.find(Bid.class, BID_ID); // SQL SELECT

        // Initializes the Item proxy because we call getId(), which is
        // not mapped as an identifier property (the field is!)
        assertEquals(someBid.getItem().getId(), anItem.getId()); // SQL SELECT

        tx.commit();
        em.close();
    }

    @AfterEach
    public void after() {
        EntityManagerBuilder.dropSchema("SimplePUNotJTATransactional");
    }

}
