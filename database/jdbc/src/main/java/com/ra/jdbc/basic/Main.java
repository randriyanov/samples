package com.ra.jdbc.basic;

import com.zaxxer.hikari.HikariDataSource;
import org.h2.tools.RunScript;

import javax.sql.DataSource;
import java.io.FileReader;


public class Main {

    public static void main(String[] args) throws Exception {
        DataSource dataSource = dataSource();
        RunScript.execute(dataSource.getConnection(), new FileReader("database/jdbc/src/main/resources/create_vehicle_table.sql"));

        //PLAIN JDBC
        VehicleDao vehicleDao = new PlainJdbcVehicleDao(dataSource);
        Vehicle vehicle = new Vehicle("TEM0001", "Red", 4, 4);
        vehicleDao.insert(vehicle);

        vehicle = vehicleDao.findByVehicleNo("TEM0001");
        System.out.println("Vehicle No: " + vehicle.getVehicleNo());
        System.out.println("Color: " + vehicle.getColor());
        System.out.println("Wheel: " + vehicle.getWheel());
        System.out.println("Seat: " + vehicle.getSeat());

    }

    public static DataSource dataSource() {

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setUsername("postgres");
        dataSource.setPassword("password");
        dataSource.setJdbcUrl("jdbc:h2:mem:vehicle");
        dataSource.setMinimumIdle(2);
        dataSource.setMaximumPoolSize(5);
        return dataSource;
    }
}
