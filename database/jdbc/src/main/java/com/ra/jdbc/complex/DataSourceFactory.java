package com.ra.jdbc.complex;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.mariadb.jdbc.MariaDbPoolDataSource;
import org.postgresql.ds.PGSimpleDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;

public enum DataSourceFactory {

    MARIA(new DataSourceConfiguration() {
        @Override
        public DataSource configure() {
            MariaDbPoolDataSource dataSource = new MariaDbPoolDataSource();
            try {
                dataSource.setUrl("jdbc:mariadb://localhost:3307/sample");
                dataSource.setPassword("root");
                dataSource.setUser("root");
            } catch (SQLException e) {
                throw new IllegalArgumentException(e);
            }
            return dataSource;
        }
    }),

    POSTGRESQL(new DataSourceConfiguration() {
        @Override
        public DataSource configure() {
            PGSimpleDataSource source = new PGSimpleDataSource();
            source.setURL("jdbc:postgresql://localhost:5432/sample");
            source.setUser("postgres");
            source.setPassword("postgres");
            return source;
        }
    }),

    MYSQL(
            new DataSourceConfiguration() {
                @Override
                public DataSource configure() {
                    MysqlDataSource dataSource = new MysqlDataSource();
                    dataSource.setURL("jdbc:mysql://localhost:3306/sample");
                    dataSource.setPassword("root");
                    dataSource.setUser("root");
                    return dataSource;
                }
            }),

    MSSQL(
            new DataSourceConfiguration() {
                @Override
                public DataSource configure() {
                    SQLServerDataSource ds = new SQLServerDataSource();
                    ds.setUser("sa");
                    ds.setPassword("rootRootRoot#");
                    ds.setURL("jdbc:sqlserver://localhost:1433;databaseName=sample");
                    return ds;
                }
            });

    public DataSourceConfiguration configuration;

    DataSourceFactory(DataSourceConfiguration configuration) {
        this.configuration = configuration;
    }

    public interface DataSourceConfiguration {

        DataSource configure();
    }

}
