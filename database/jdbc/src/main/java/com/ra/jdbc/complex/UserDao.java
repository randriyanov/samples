package com.ra.jdbc.complex;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class UserDao {

    private final DataSource dataSource;

    private final String INSERT_ITEM = "insert into Item (buyer_id, name, id) values (?, ?, ?)";
    private final String INSERT_USER = "insert into USERS (username, id) values (?, ?)";
    private final String SELECT_USER = "SELECT * FROM USERS WHERE id = ?";
    private final String SELECT_ITEM = "SELECT * FROM Item WHERE buyer_id = ?";

    public UserDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public User getUser(Long id) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement userStatement = conn.prepareStatement(SELECT_USER);
            userStatement.setLong(1, id);
            User user = new User();
            ResultSet rs = userStatement.executeQuery();
            if (rs.next()) {
                user.setId(id);
                user.setUsername(rs.getString("USERNAME"));
            }
            PreparedStatement itemStatement = conn.prepareStatement(SELECT_ITEM);
            itemStatement.setLong(1, user.getId());
            rs = itemStatement.executeQuery();
            Set<Item> items = new HashSet<>();
            while (rs.next()) {
                Item item = new Item();
                item.setId(rs.getLong("id"));
                item.setName(rs.getString("name"));
                item.setBuyer(user);
                items.add(item);
            }
            user.setBoughtItems(items);
            return user;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int insertUser(User user) {
        int result = 0;
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            PreparedStatement userStatement = connection.prepareStatement(INSERT_USER);
            userStatement.setString(1, user.getUsername());
            userStatement.setLong(2, user.getId());
            result += userStatement.executeUpdate();
            final Set<Item> items = user.getBoughtItems();
            for (Item item : items) {
                PreparedStatement itemStatement = connection.prepareStatement(INSERT_ITEM);
                itemStatement.setLong(1, item.getBuyer().getId());
                itemStatement.setString(2, item.getName());
                itemStatement.setLong(3, item.getId());
                result += itemStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return result;
    }
}
