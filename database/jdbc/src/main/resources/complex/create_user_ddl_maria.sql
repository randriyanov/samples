CREATE DATABASE sample;

create table hibernate_sequence (next_val bigint) engine=InnoDB
insert into hibernate_sequence values ( 1 )

create table Item (id bigint not null, name varchar(255), buyer_id bigint, primary key (id)) engine=InnoDB
create table USERS (id bigint not null, username varchar(255), primary key (id)) engine=InnoDB

alter table Item add constraint ITEM_CONSTRAINT foreign key (buyer_id) references USERS (id)