create table hibernate_sequence (next_val numeric(19,0));
insert into hibernate_sequence values ( 1 );

create table Item (id numeric(19,0) not null, name varchar(255), buyer_id numeric(19,0), primary key (id));
create table USERS (id numeric(19,0) not null, username varchar(255), primary key (id));
alter table Item add constraint ITEM_CONSTRAINT foreign key (buyer_id) references USERS;