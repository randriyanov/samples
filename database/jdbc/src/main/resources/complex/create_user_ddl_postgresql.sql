create table Item (id int8 not null, name varchar(255), buyer_id int8, primary key (id));
create table USERS (id int8 not null, username varchar(255), primary key (id));
alter table if exists Item add constraint ITEM_CONSTRAINT foreign key (buyer_id) references USERS;