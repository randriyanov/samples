package com.ra.jdbc.basic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PlainJdbcVehicleDaoTest {
    private static final String SELECT_ONE_SQL = "SELECT * FROM VEHICLE WHERE VEHICLE_NO = ?";
    private PlainJdbcVehicleDao vehicleDao;
    private DataSource mockDataSource;
    private Connection mockConnection;

    @BeforeEach
    public void before() throws Exception {
        mockDataSource = mock(DataSource.class);
        vehicleDao = new PlainJdbcVehicleDao(mockDataSource);
        mockConnection = mock(Connection.class);
        when(mockDataSource.getConnection()).thenReturn(mockConnection);
    }


    @Test
    public void whenCallInsertThenSaveItInDB() {

    }

    @Test
    public void whenCallFindByVehicleNoThenReturnItFromDb() throws Exception {

        var prepStatement = mock(PreparedStatement.class);
        when(mockConnection.prepareStatement(SELECT_ONE_SQL)).thenReturn(prepStatement);
        var resSet = mock(ResultSet.class);
        when(prepStatement.executeQuery()).thenReturn(resSet);
        when(resSet.next()).thenReturn(true);

        var testNumber = "1";
        when(resSet.getString("VEHICLE_NO")).thenReturn(testNumber);
        var vehicle = vehicleDao.findByVehicleNo(testNumber);

        assertThat(vehicle.getVehicleNo()).isEqualTo(testNumber);


        assertThrows(RuntimeException.class, () -> vehicleDao.findByVehicleNo(testNumber));

    }
}
