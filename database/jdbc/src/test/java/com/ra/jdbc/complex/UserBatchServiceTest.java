package com.ra.jdbc.complex;

import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class UserBatchServiceTest {

    private static BlockingQueue<DataSource> dataSources = new ArrayBlockingQueue<>(4);

    static {
        dataSources.add(DataSourceFactory.MARIA.configuration.configure());
        dataSources.add(DataSourceFactory.POSTGRESQL.configuration.configure());
        dataSources.add(DataSourceFactory.MYSQL.configuration.configure());
        dataSources.add(DataSourceFactory.MSSQL.configuration.configure());
    }

    @Test
    public void getOneUserFromPostgres() {
        User user = getUser(DataSourceFactory.POSTGRESQL.configuration.configure(), 1);
        System.out.println(user);
    }

    @Test
    public void insertOneUserWithItemsInAllDataBases() throws Exception {


    }

    @Test
    public void insertOneUserWithItemsInPostgres() throws Exception {
        int result = insertUser(DataSourceFactory.POSTGRESQL.configuration.configure());
        System.out.println(result);
    }

    @Test
    public void insertOneUserWithItemsInMariaDb() throws Exception {
        int result = insertUser(DataSourceFactory.MARIA.configuration.configure());
        System.out.println(result);
    }

    @Test
    public void insertOneUserWithItemsInMySQLDb() throws Exception {
        int result = insertUser(DataSourceFactory.MYSQL.configuration.configure());
        System.out.println(result);
    }

    @Test
    public void insertOneUserWithItemsInMSSQLDb() throws Exception {
        int result = insertUser(DataSourceFactory.MSSQL.configuration.configure());
        System.out.println(result);
    }


    public User getUser(DataSource dataSource, long id) {
        UserDao userDao = new UserDao(dataSource);
        return userDao.getUser(id);
    }


    private int insertUser(DataSource dataSource) {
        Item someItem = new Item();
        someItem.setId(1L);
        someItem.setName("Some Item");

        Item otherItem = new Item();
        otherItem.setName("Other Item");
        otherItem.setId(2L);

        User someUser = new User();
        someUser.setId(1L);
        someUser.setUsername("johndoe");
        someUser.getBoughtItems().add(someItem); // Link
        someItem.setBuyer(someUser); // Link
        someUser.getBoughtItems().add(otherItem);
        otherItem.setBuyer(someUser);

        UserDao userDao = new UserDao(dataSource);
        return userDao.insertUser(someUser);
    }


}
