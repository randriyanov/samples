package com.ra.jdbc.connectionpool;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class C3poDataSourceUnitTest {
    
    @Test
    public void givenC3poDataSourceClass_whenCalledgetConnection_thenCorrect() throws SQLException {
        assertTrue(C3poDataSource.getConnection().isValid(1));
    }   
}