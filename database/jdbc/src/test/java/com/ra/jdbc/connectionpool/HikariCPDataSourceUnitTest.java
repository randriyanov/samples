package com.ra.jdbc.connectionpool;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class HikariCPDataSourceUnitTest {
    
    @Test
    public void givenHikariDataSourceClass_whenCalledgetConnection_thenCorrect() throws SQLException {
        assertTrue(HikariCPDataSource.getConnection().isValid(1));
    }   
}