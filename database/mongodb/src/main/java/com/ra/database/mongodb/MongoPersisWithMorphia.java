package com.ra.database.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import dev.morphia.Datastore;
import dev.morphia.Key;
import dev.morphia.Morphia;

public class MongoPersisWithMorphia {

    private MongoClient mongoClient = new MongoClient("localhost:27017");
    private MongoDatabase database = mongoClient.getDatabase("Sample");
    final Morphia morphia = new Morphia();

    public MongoPersisWithMorphia() {
        morphia.mapPackage("com.ra.database.mongodb");
    }


    public Long saveUser(User user) {
        final Datastore datastore = morphia.createDatastore(mongoClient, "Sample");
        Key<User> key = datastore.save(user);
        return (Long)key.getId();
    }


}
