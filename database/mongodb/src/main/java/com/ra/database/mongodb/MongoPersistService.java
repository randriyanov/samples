package com.ra.database.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.Function;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.stream.Collectors;

public class MongoPersistService {
    private MongoClient mongoClient = new MongoClient("localhost:27017");
    private MongoDatabase database = mongoClient.getDatabase("Sample");


    public void saveUser(User user) {
        Document document = new Document();
        var items = user.getBoughtItems().stream()
                .map(item -> new Document()
                        .append("_id", item.getId())
                        .append("buyer", item.getBuyer().getUsername())
                        .append("name", item.getName())).collect(Collectors.toList());
        document.append("_id", user.getId())
                .append("username", user.getUsername())
                .append("items", items);

        MongoCollection<Document> collection = database.getCollection("users");
        collection.insertOne(document);
    }

    public User getUser(Long id) {
        BasicDBObject query = new BasicDBObject("_id", id);
        MongoCollection<Document> collection = database.getCollection("users");
        User user = (User)collection.find(query).map(new Function<Document, Object>() {
            @Override
            public User apply(Document document) {
                User user = new User();
                user.setId((Long)(document.get("_id")));
                user.setUsername(document.getString("username"));
               return user;
            }
        }).first();
        return user;
    }

}
