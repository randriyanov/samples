package com.ra.database.mongodb;

import org.junit.jupiter.api.Test;

public class MongoPersistServiceTest {
    private MongoPersistService service = new MongoPersistService();
    private MongoPersisWithMorphia persisWithMorphia = new MongoPersisWithMorphia();

    @Test
    public void whenSaveNewUserWithMorphiaThenReturnNewId() {
        User toSave = getUser();
        Long userId = persisWithMorphia.saveUser(toSave);
        System.out.println(userId);
    }

    @Test
    public void whenRequestUserByIdThenReturnNewUser() {
        User resultUser = service.getUser(1L);
        System.out.println(resultUser.getId());
    }


    @Test
    public void whenSaveNewUserThenReturnNewId() {
        User toSave = getUser();
        service.saveUser(toSave);
    }

    private User getUser() {
        Item someItem = new Item();
        someItem.setId(1L);
        someItem.setName("Some Item");

        Item otherItem = new Item();
        otherItem.setName("Other Item");
        otherItem.setId(2L);

        User someUser = new User();
        someUser.setId(1L);
        someUser.setUsername("johndoe");
        someUser.getBoughtItems().add(someItem); // Link
        someItem.setBuyer(someUser); // Link
        someUser.getBoughtItems().add(otherItem);
        otherItem.setBuyer(someUser);

        return someUser;
    }
}
