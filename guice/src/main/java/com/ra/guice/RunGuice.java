package com.ra.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.ra.guice.basic.Communication;
import com.ra.guice.binding.AOPModule;
import com.ra.guice.modules.BasicModule;

import java.util.Scanner;


public class RunGuice {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new BasicModule(), new AOPModule());
        Communication comms = injector.getInstance(Communication.class);
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String input = scanner.nextLine();
            if (input.equalsIgnoreCase("q")) {
                System.exit(0);
            } else {
                comms.sendMessage(input);
            }

        }

    }
}
