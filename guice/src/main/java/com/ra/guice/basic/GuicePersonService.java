package com.ra.guice.basic;

import com.google.inject.Inject;
import com.ra.guice.common.PersonDao;

public class GuicePersonService {

	@Inject
	private PersonDao personDao;

	public PersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(PersonDao personDao) {
		this.personDao = personDao;
	}

}
