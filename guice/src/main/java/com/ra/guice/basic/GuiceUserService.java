package com.ra.guice.basic;

import com.google.inject.Inject;
import com.ra.guice.common.AccountService;

public class GuiceUserService {

	@Inject
	private AccountService accountService;

	public AccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}

}
