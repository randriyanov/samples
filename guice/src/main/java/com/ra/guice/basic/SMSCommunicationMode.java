
package com.ra.guice.basic;

import com.google.inject.Inject;
import com.ra.guice.aop.MessageSentLoggable;
import com.ra.guice.constant.CommunicationModel;

import java.util.logging.Logger;

public class SMSCommunicationMode implements CommunicationMode {
    
    @Inject
    private Logger logger;

    @Override
    public CommunicationModel getMode() {
        return CommunicationModel.SMS;
    }

    @Override
    @MessageSentLoggable
    public boolean sendMessage(String message) {
        logger.info("SMS message sent");
        return true;
    }

}
