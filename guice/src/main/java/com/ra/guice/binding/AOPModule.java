package com.ra.guice.binding;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;
import com.ra.guice.aop.MessageLogger;
import com.ra.guice.aop.MessageSentLoggable;


public class AOPModule extends AbstractModule {

    @Override
    protected void configure() {
        bindInterceptor(Matchers.any(),
                Matchers.annotatedWith(MessageSentLoggable.class),
                new MessageLogger()
        );
    }

}
