package com.ra.guice.email;

import com.google.inject.Inject;

public class AnonymEmail implements Email {
    private MessageService messageService;
    private Parser parserService;

    @Inject
    public AnonymEmail(MessageService messageService, Parser parserService) {
        this.messageService = messageService;
        this.parserService = parserService;
    }

    @Override
    public void sendEmail(String message) {
        System.out.println("In anonym class " + parserService.parse(message));
        messageService.send(message);
    }


}
