package com.ra.guice.email;

public interface Email {

    void sendEmail(String message);
}
