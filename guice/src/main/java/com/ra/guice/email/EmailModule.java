package com.ra.guice.email;

import com.google.inject.AbstractModule;

public class EmailModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(MessageService.class).to(SmtpMessageService.class);
        bind(Parser.class).toInstance(new StandardParserService());
        bind(Email.class).to(AnonymEmail.class);
    }
}
