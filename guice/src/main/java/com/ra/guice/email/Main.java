package com.ra.guice.email;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new EmailModule());
        Email email = injector.getInstance(Email.class);
        email.sendEmail("Test");
    }
}
