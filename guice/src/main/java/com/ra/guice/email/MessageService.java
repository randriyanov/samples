package com.ra.guice.email;

public interface MessageService {
    void send(String text);
}
