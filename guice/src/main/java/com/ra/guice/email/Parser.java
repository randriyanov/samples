package com.ra.guice.email;

public interface Parser {

    String parse(String str);
}
