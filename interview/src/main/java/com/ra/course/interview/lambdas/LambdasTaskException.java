package com.ra.course.interview.lambdas;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

public class LambdasTaskException {

    public static void main(String[] args) {
        List<String> urls = new ArrayList<>();
        LambdasTaskException webCrawler = new LambdasTaskException();
        webCrawler.crawl(urls);
    }


    public void crawlException(List<String> urlsToCrawl) {
        urlsToCrawl.stream()
                .map(urlToCrawl -> {
                    try {
                        return new URL(urlToCrawl);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .forEach(url -> {
                    try {
                        save(url);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    private void save(URL url) throws IOException {
        String uuid = UUID.randomUUID().toString();
        InputStream inputStream = url.openConnection().getInputStream();
        Files.copy(inputStream, Paths.get(uuid + ".txt"), StandardCopyOption.REPLACE_EXISTING);
    }

    public void crawl(List<String> urls) {
        urls.stream()
                .map(ThrowingFunction.unchecked(URL::new))
                .forEach(ThrowingConsumer.unchecked(this::save));
    }



    @FunctionalInterface
    public interface ThrowingFunction<T, R, E extends Throwable> {
        R apply(T t) throws E;

        static <T, R, E extends Throwable> Function<T, R> unchecked(ThrowingFunction<T, R, E> f) {
            return t -> {
                try {
                    return f.apply(t);
                } catch (Throwable e) {
                    throw new RuntimeException(e);
                }
            };
        }
    }

    @FunctionalInterface
    public interface ThrowingConsumer<T, E extends Throwable> {
        void accept(T t) throws E;

        static <T, E extends Throwable> Consumer<T> unchecked(ThrowingConsumer<T, E> f) {
            return t -> {
                try {
                    f.accept(t);
                } catch (Throwable e) {
                    throw new RuntimeException(e);
                }
            };
        }
    }

}
