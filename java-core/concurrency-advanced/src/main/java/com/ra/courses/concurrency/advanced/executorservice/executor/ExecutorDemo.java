package com.ra.courses.concurrency.advanced.executorservice.executor;

import java.util.concurrent.Executor;

public class ExecutorDemo {

	public void execute() {
		Executor executor = new Invoker();
		executor.execute(()->{
			// task to be performed
		});
	}

}
