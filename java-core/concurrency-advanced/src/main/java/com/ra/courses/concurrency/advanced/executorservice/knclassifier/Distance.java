package com.ra.courses.concurrency.advanced.executorservice.knclassifier;

public class Distance implements Comparable<Double> {
    private Double distance;
    private int index;

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int compareTo(Double o) {
        return distance.compareTo(o);
    }
}
