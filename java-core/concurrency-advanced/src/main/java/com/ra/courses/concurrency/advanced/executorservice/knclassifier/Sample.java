package com.ra.courses.concurrency.advanced.executorservice.knclassifier;

public class Sample {

    private final double[] example;
    private final String tag;

    public Sample(double[] data, String tag) {
        this.example = data;
        this.tag = tag;
    }

    public double[] getExample() {
        return example;
    }

    public String getTag() {
        return tag;
    }
}
