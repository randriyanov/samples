package com.ra.samples.concurrency.basic.patterns.doublechecked;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DoubleCheckedBeforeSingleton {
    private DoubleCheckedBeforeSingleton reference;
    private Lock lock = new ReentrantLock();

    public Object getReference() {
        if (reference == null) {
            lock.lock();
            try {
                if (reference == null) {
                    reference = new DoubleCheckedBeforeSingleton();
                }
            } finally {
                lock.unlock();
            }
        }
        return reference;
    }
}
