package com.ra.samples.concurrency.basic.patterns.doublechecked;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DoubleCheckedSingleton {
    private static DoubleCheckedSingleton reference;
    private static final Lock lock = new ReentrantLock();

    public static DoubleCheckedSingleton getReference() {
        lock.lock();
        try {
            if (reference == null) {
                reference = new DoubleCheckedSingleton();
            }
        } finally {
            lock.unlock();
        }
        return reference;
    }
}
