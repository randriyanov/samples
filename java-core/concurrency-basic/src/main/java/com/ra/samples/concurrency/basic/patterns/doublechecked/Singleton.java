package com.ra.samples.concurrency.basic.patterns.doublechecked;

public class Singleton {

    private static class LazySingleton {
        private static final Singleton INSTANCE = new Singleton();
    }
    public static Singleton getSingleton() {
        return LazySingleton.INSTANCE;
    }
}
