package com.ra.samples.concurrency.basic.patterns.signaling;

import java.util.concurrent.ThreadLocalRandom;

public class Receiver implements Runnable {

    private final DataQueue dataQueue;

    public Receiver(DataQueue dataQueue) {
        this.dataQueue = dataQueue;
    }

    @Override
    public void run() {
        String receivedMessage;
        while (!(receivedMessage = dataQueue.receive()).equals("End")) {
            System.out.println(receivedMessage);
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 5000));
            } catch (InterruptedException e) {
                System.out.println("Thread Interrupted in Receiver");
                Thread.currentThread().interrupt();
            }
        }
    }
}
