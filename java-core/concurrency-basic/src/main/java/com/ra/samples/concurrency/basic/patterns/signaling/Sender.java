package com.ra.samples.concurrency.basic.patterns.signaling;

import java.util.concurrent.ThreadLocalRandom;

public class Sender implements Runnable {
    private final DataQueue dataQueue;

    public Sender(DataQueue dataQueue) {
        this.dataQueue = dataQueue;
    }

    public void run() {
        String packets[] = {
                "First packet",
                "Second packet",
                "Third packet",
                "Fourth packet",
                "End"
        };

        for (String packet : packets) {
            dataQueue.send(packet);
            //Thread.sleep() to mimic heavy server-side processing
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 5000));
            } catch (InterruptedException e) {
                System.out.println("Thread Interrupted in Sender");
                Thread.currentThread().interrupt();
            }
        }
    }
}
