package com.ra.samples.concurrency.basic.patterns.signaling;


public class SignalingStarter {
    public static void main(String[] args) {
        DataQueue data = new DataQueue();
        Thread sender = new Thread(new Sender(data));
        Thread receiver = new Thread(new Receiver(data));

        sender.start();
        receiver.start();
    }
}
