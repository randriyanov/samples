package com.ra.samples.concurrency.basic.waitandnotify.second;

public class ThreadA {

    final static Object monitor = new Object();

    public static void main(String[] args) {
        ThreadB b = new ThreadB();
        b.start();
        synchronized (monitor) {
            try {
                System.out.println("Waiting for b to complete... " + Thread.currentThread().getName());
                monitor.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Total is: " + b.total);
        }
    }
}

class ThreadB extends Thread {
    int total;

    @Override
    public void run() {
        synchronized (ThreadA.monitor) {
            for (int i = 0; i < 100; i++) {
                System.out.println("Thread " + Thread.currentThread().getName());
                total += i;
                if (i == 50) {
                    ThreadA.monitor.notify();
                    try {
                        sleep(10000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
