package core.java9.features.threads.async;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;
import java.util.concurrent.atomic.AtomicInteger;

public class EndSubscriber<T> implements Subscriber<T> {

    private final AtomicInteger howMuchMessagesToConsume;
    private Subscription subscription;
    private List<T> consumedElements;

    public EndSubscriber(Integer messagesToConsume) {
        this.howMuchMessagesToConsume = new AtomicInteger(messagesToConsume);
        this.consumedElements = new LinkedList<>();
    }

    @Override
    public void onSubscribe(Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(T item) {
        //Increment received messages
        howMuchMessagesToConsume.decrementAndGet();

        System.out.println("Got : " + item);

        consumedElements.add(item);

        if (howMuchMessagesToConsume.get() > 0) {
            subscription.request(1);
        }
    }

    @Override
    public void onError(Throwable t) {
        System.out.println("We are in error block");
    }

    @Override
    public void onComplete() {
        System.out.println("Done");
    }

    public List<T> getConsumedElements() {
        return consumedElements;
    }
}