package core.java9.features.threads.compare;

import java.util.concurrent.Flow;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class PeriodicRandomNumberGenerator extends PeriodicSubmissionPublisher<Long> {

    public PeriodicRandomNumberGenerator() {
        super((publisher) ->
                        publisher.submit(ThreadLocalRandom.current().nextLong()),
                Flow.defaultBufferSize(),
                10,
                1,
                TimeUnit.SECONDS);
    }

    public static void main(final String[] args) {
        final PeriodicRandomNumberGenerator generator = new PeriodicRandomNumberGenerator();

        generator.subscribe(new Subscriber<>() {
            @Override
            public void onSubscribe(final Subscription subscription) {
                subscription.request(Long.MAX_VALUE);
            }

            @Override
            public void onNext(final Long item) {
                System.out.printf("Received: %s%n", item);
            }

            @Override
            public void onError(final Throwable throwable) {
                throwable.printStackTrace();
            }

            @Override
            public void onComplete() {
                System.out.println("Completed!");
            }
        });
        generator.waitForCompletion();
    }
}
