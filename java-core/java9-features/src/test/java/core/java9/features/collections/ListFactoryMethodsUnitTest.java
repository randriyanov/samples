package core.java9.features.collections;


import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class ListFactoryMethodsUnitTest {

    @Test
    public void whenListCreated_thenSuccess() {
        List<String> traditionlList = new ArrayList<String>();
        traditionlList.add("foo");
        traditionlList.add("bar");
        traditionlList.add("baz");
        List<String> factoryCreatedList = List.of("foo", "bar", "baz");
        assertEquals(traditionlList, factoryCreatedList);
    }

    @Test
    public void onElemAddDirectExpnThrown() {
        List<String> list = List.of("foo", "bar");
        assertThrows(UnsupportedOperationException.class, () -> list.add("baz"));
    }

    @Test
    public void onElemModifyUnSupportedThrown() {
        List<String> list = List.of("foo", "bar");
        assertThrows(UnsupportedOperationException.class, () -> list.set(0, "baz"));
    }

    @Test
    public void onElemRemoveSupportedOpExpnThrown() {
        List<String> list = List.of("foo", "bar");
        assertThrows(UnsupportedOperationException.class, () -> list.remove("foo"));
    }

    @Test
    public void onNullElemExpnThrown() {
        assertThrows(NullPointerException.class, () -> List.of("foo", "bar", null));
    }

    @Test
    public void immutableListOnListOfCreated() {
        List<String> list = List.of("foo", "bar");
        assertFalse(list instanceof ArrayList);
    }

}
