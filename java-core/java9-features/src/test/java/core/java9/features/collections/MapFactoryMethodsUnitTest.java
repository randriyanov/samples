package core.java9.features.collections;


import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


public class MapFactoryMethodsUnitTest {

    @Test
    public void mapCreated() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("foo", "a");
        map.put("bar", "b");
        map.put("baz", "c");
        Map<String, String> factoryCreatedMap = Map.of("foo", "a", "bar", "b", "baz", "c");
        assertEquals(map, factoryCreatedMap);
    }

    @Test
    public void elemAddDirectExpnThrown() {
        Map<String, String> map = Map.of("foo", "a", "bar", "b");
        assertThrows(UnsupportedOperationException.class, () -> map.put("baz", "c"));
    }


    @Test
    public void onElemRemoveExpnThrown() {
        Map<String, String> map = Map.of("foo", "a", "bar", "b");
        assertThrows(UnsupportedOperationException.class, () -> map.remove("foo"));
    }

    @Test
    public void givenDuplicateKeyIllegalArgExp() {
        assertThrows(IllegalArgumentException.class, () -> Map.of("foo", "a", "foo", "b"));
    }

    @Test
    public void onNullKeyNullPtrThrown() {
        assertThrows(NullPointerException.class, () -> Map.of("foo", "a", null, "b"));
    }

    @Test
    public void onNullValueNullPtrExp() {
        assertThrows(NullPointerException.class, () -> Map.of("foo", "a", "bar", null));
    }

    @Test
    public void mapOfReturnImmutableMap() {
        Map<String, String> map = Map.of("foo", "a", "bar", "b");
        assertFalse(map instanceof HashMap);
    }

}
