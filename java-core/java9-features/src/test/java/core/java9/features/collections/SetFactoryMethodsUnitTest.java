package core.java9.features.collections;


import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


public class SetFactoryMethodsUnitTest {

    @Test
    public void setCreated() {
        Set<String> traditionlSet = new HashSet<String>();
        traditionlSet.add("foo");
        traditionlSet.add("bar");
        traditionlSet.add("baz");
        Set<String> factoryCreatedSet = Set.of("foo", "bar", "baz");
        assertEquals(traditionlSet, factoryCreatedSet);
    }

    @Test
    public void onDuplicateElemThenException() {
        assertThrows(IllegalArgumentException.class, () -> Set.of("foo", "bar", "baz", "foo"));
    }

    @Test
    public void onElemAddUnSupportedOpExpnThrown() {
        Set<String> set = Set.of("foo", "bar");
        assertThrows(UnsupportedOperationException.class, () -> set.add("baz"));
    }

    @Test
    public void onElemRemoveUnSupportedOpExpnThrown() {
        Set<String> set = Set.of("foo", "bar", "baz");
        assertThrows(UnsupportedOperationException.class, () -> set.remove("foo"));
    }

    @Test
    public void onNullElemNullPtrExpnThrown() {
        assertThrows(NullPointerException.class, () -> Set.of("foo", "bar", null));
    }

    @Test
    public void setOfReturnImmutableSet() {
        Set<String> list = Set.of("foo", "bar");
        assertFalse(list instanceof HashSet);
    }

    @Test
    public void ifSetSizeIsOne_thenSuccess() {
        int[] arr = { 1, 2, 3, 4 };
        Set<int[]> set = Set.of(arr);
        assertEquals(1, set.size());
        assertArrayEquals(arr, set.iterator().next());
    }

}
