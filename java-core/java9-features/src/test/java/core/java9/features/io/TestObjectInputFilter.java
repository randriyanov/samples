package core.java9.features.io;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputFilter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestObjectInputFilter {

    static class A implements Serializable {

        public int a = 1;
        public B b = new B();
    }

    static class B implements Serializable {

        public String b = "hello";
        public C c = new C();
    }

    static class C implements Serializable {

        public int c = 2;
        public D[] ds = new D[]{new D(), new D()};
    }

    static class D implements Serializable {

        public String d = "world";
    }

    private ByteArrayInputStream objectInput;

    @BeforeEach
    public void setUp() throws Exception {
        final A a = new A();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final ObjectOutputStream outputStream = new ObjectOutputStream(baos);
        outputStream.writeObject(a);
        this.objectInput = new ByteArrayInputStream(baos.toByteArray());
    }

    @Test
    public void testFilterByClass() throws Exception {
        final ObjectInputStream inputStream =
                new ObjectInputStream(this.objectInput);
        inputStream.setObjectInputFilter(filterInfo -> {
            if (B.class.isAssignableFrom(filterInfo.serialClass())) {
                return ObjectInputFilter.Status.REJECTED;
            }
            return ObjectInputFilter.Status.UNDECIDED;
        });
        final A a = (A) inputStream.readObject();
    }

    @Test
    public void testFilterByDepth() throws Exception {
        final ObjectInputStream inputStream = new
                ObjectInputStream(this.objectInput);
        inputStream.setObjectInputFilter(filterInfo -> {
            if (filterInfo.depth() > 6) {
                return ObjectInputFilter.Status.REJECTED;
            }
            return ObjectInputFilter.Status.UNDECIDED;
        });
        final A a = (A) inputStream.readObject();
        assertEquals(1, a.a);
    }

    @Test
    public void testProcessWideFilter() throws Exception {
        ObjectInputFilter.Config.setSerialFilter(filterInfo -> {
            if (C.class.isAssignableFrom(filterInfo.serialClass())) {
                return ObjectInputFilter.Status.REJECTED;
            }
            return ObjectInputFilter.Status.ALLOWED;
        });
        final ObjectInputStream inputStream =
                new ObjectInputStream(this.objectInput);
        final A a = (A) inputStream.readObject();

    }

}
