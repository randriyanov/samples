package core.java9.features.methodhandlers;


import org.junit.jupiter.api.Test;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.invoke.WrongMethodTypeException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


public class MethodHandlesTest {
    public class Book {

        String id;
        String title;

        public Book(String id, String title) {
            this.id = id;
            this.title = title;
        }

        @SuppressWarnings("unused")
        private String formatBook() {
            return id + " > " + title;
        }
    }

    @Test
    public void сoncatMethodHandle() throws Throwable {

        MethodHandles.Lookup publicLookup = MethodHandles.publicLookup();
        MethodType mt = MethodType.methodType(String.class, String.class);
        MethodHandle concatMH = publicLookup.findVirtual(String.class, "concat", mt);

        String output = (String) concatMH.invoke("Effective ", "Java");

        assertThat("Effective Java").isEqualTo(output);
    }

    @Test
    public void invokeListWithArgumentsMethodHandle() throws Throwable {
        MethodHandles.Lookup publicLookup = MethodHandles.publicLookup();
        MethodType mt = MethodType.methodType(List.class, Object[].class);
        MethodHandle asListMH = publicLookup.findStatic(Arrays.class, "asList", mt);

        List<Integer> list = (List<Integer>) asListMH.invokeWithArguments(1, 2);

        assertThat(Arrays.asList(1, 2)).containsAll(list);
    }

    @Test
    public void constructorCreateObjectMethodHandle() throws Throwable {
        MethodHandles.Lookup publicLookup = MethodHandles.publicLookup();
        MethodType mt = MethodType.methodType(void.class, String.class);
        MethodHandle newIntegerMH = publicLookup.findConstructor(Integer.class, mt);

        Integer integer = (Integer) newIntegerMH.invoke("1");

        assertThat(1).isEqualTo(integer);
    }

    @Test
    public void createGetterMethodHandle() throws Throwable {
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodHandle getTitleMH = lookup.findGetter(Book.class, "title", String.class);

        Book book = new Book("ISBN-1234", "Effective Java");

        assertThat("Effective Java").isEqualTo(getTitleMH.invoke(book));
    }

    @Test
    public void invokePrivateMethodMethodHandle() throws Throwable {
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        Method formatBookMethod = Book.class.getDeclaredMethod("formatBook");
        formatBookMethod.setAccessible(true);

        MethodHandle formatBookMH = lookup.unreflect(formatBookMethod);

        Book book = new Book("ISBN-123", "Java in Action");

        assertThat("ISBN-123 > Java in Action").isEqualTo(formatBookMH.invoke(book));
    }

    @Test
    public void stringReplaceMethodMethodHandle() throws Throwable {
        Method replaceMethod = String.class.getMethod("replace", char.class, char.class);

        String string = (String) replaceMethod.invoke("jovo", 'o', 'a');

        assertThat("java").isEqualTo(string);
    }

    @Test
    public void findAndInvokeMethodWithTypesMethodHandle() throws Throwable {
        MethodHandles.Lookup publicLookup = MethodHandles.publicLookup();
        MethodType mt = MethodType.methodType(String.class, char.class, char.class);
        MethodHandle replaceMH = publicLookup.findVirtual(String.class, "replace", mt);

        String replacedString = (String) replaceMH.invoke("jovo", 'o', 'a');

        assertThat("java").isEqualTo(replacedString);
    }

    @Test
    public void replaceMethodWithInvokeExact() throws Throwable {
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodType mt = MethodType.methodType(String.class, char.class, char.class);
        MethodHandle replaceMH = lookup.findVirtual(String.class, "replace", mt);

        String s = (String) replaceMH.invokeExact("jovo", 'o', 'a');

        assertThat("java").isEqualTo(s);

    }

    @Test
    public void methodHandleOnStatic() throws Throwable {
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodType mt = MethodType.methodType(int.class, int.class, int.class);
        MethodHandle sumMH = lookup.findStatic(Integer.class, "sum", mt);

        int sum = (int) sumMH.invokeExact(1, 11);
        assertThat(12).isEqualTo(sum);
    }

    @Test
    public void incompatibleArgumentsInvokingExact() throws Throwable {
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodType mt = MethodType.methodType(int.class, int.class, int.class);
        MethodHandle sumMH = lookup.findStatic(Integer.class, "sum", mt);

        assertThrows(WrongMethodTypeException.class, () -> sumMH.invokeExact(Integer.valueOf(1), 11));
    }

    @Test
    public void givenSpreadedEqualsMethodHandle_whenInvokedOnArray_thenCorrectlyEvaluated() throws Throwable {
        MethodHandles.Lookup publicLookup = MethodHandles.publicLookup();
        MethodType mt = MethodType.methodType(boolean.class, Object.class);
        MethodHandle equalsMH = publicLookup.findVirtual(String.class, "equals", mt);

        MethodHandle methodHandle = equalsMH.asSpreader(Object[].class, 2);

        assertTrue((boolean) methodHandle.invoke(new Object[]{"java", "java"}));
        assertFalse((boolean) methodHandle.invoke(new Object[]{"java", "jova"}));
    }


}
