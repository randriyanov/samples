package core.java9.features.misc;


import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class TestObjects {

    private List<String> aMethodReturningNullList(){
        return null;
    }

    @Test
    public void whenNullObjectThenElse() {
        List<String> aList = Objects.requireNonNullElse(aMethodReturningNullList(), Collections.emptyList());

        assertThat(aList).isEmpty();
    }

    private List<String> aMethodReturningNonNullList() {
        return List.of("item1", "item2");
    }


    @Test
    public void requireNonNullElseThrowException() {
         assertThrows(NullPointerException.class, () -> Objects.requireNonNullElse(null, null));
    }

    @Test
    public void requireNonNullElseReturnObject() {
        List<String> aList = Objects.requireNonNullElseGet(null, List::of);
        assertThat(aList).isEqualTo(List.of());
    }

    @Test
    //it returns the index if 0 <= index < length
    public void whenInvokeCheckIndexThenReturnNumber() {
        int length = 5;

        assertThat(Objects.checkIndex(4, length)).isEqualTo(4);
    }

    //If the sub-range is valid, then it returns the lower bound
    @Test
    public void givenOutOfRangeNumberThenException() {
        int length = 5;
        assertThrows(IndexOutOfBoundsException.class, () -> Objects.checkIndex(5, length));
    }

    @Test
    public void givenSubRangeWhenCheckThenNumber() {
        int length = 6;

        assertThat(Objects.checkFromIndexSize(2,3,length)).isEqualTo(2);
    }

    @Test
    public void givenInvalidSubRange_whenCheckFromIndexSize_thenException() {
        int length = 6;

        assertThrows(IndexOutOfBoundsException.class, () -> Objects.checkFromIndexSize(2, 6, length));
    }

    @Test
    public void testCheckIndex() throws Exception {
        assertEquals(0, Objects.checkIndex(0, 1));
        assertThrows(IndexOutOfBoundsException.class, () -> Objects.checkIndex(3, 1));

        assertEquals(1, Objects.checkFromIndexSize(0, 2, 5));
        Objects.checkFromIndexSize(0, 3, 1);

        assertEquals(1, Objects.checkFromToIndex(1, 3, 5));
        Objects.checkFromToIndex(0, 3, 2);
    }
}
