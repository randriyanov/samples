package core.java9.features.misc.optionals;

import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class Java9OptionalTest {


    @Test
    public void optionalIfPresentOrElseSuccess() {
        //given
        Optional<String> value = Optional.of("properValue");
        AtomicInteger successCounter = new AtomicInteger(0);
        AtomicInteger onEmptyOptionalCounter = new AtomicInteger(0);

        //when
        value.ifPresentOrElse((v) -> successCounter.incrementAndGet(), onEmptyOptionalCounter::incrementAndGet);

        //then
        assertThat(successCounter.get()).isEqualTo(1);
        assertThat(onEmptyOptionalCounter.get()).isEqualTo(0);
    }

    @Test
    public void optionalIfPresentOrElseFail() {
        //given
        Optional<String> value = Optional.empty();
        AtomicInteger successCounter = new AtomicInteger(0);
        AtomicInteger onEmptyOptionalCounter = new AtomicInteger(0);

        //when
        value.ifPresentOrElse((v) -> successCounter.incrementAndGet(), onEmptyOptionalCounter::incrementAndGet);

        //then
        assertThat(successCounter.get()).isEqualTo(0);
        assertThat(onEmptyOptionalCounter.get()).isEqualTo(1);
    }

    @Test
    public void ifOptionalValueIsPresentOrNotReturnValue() {
        //given
        String expected = "properValue";
        Optional<String> value = Optional.of(expected);
        Optional<String> defaultValue = Optional.of("default");

        //when
        Optional<String> result = value.or(() -> defaultValue);

        //then
        assertThat(result.get()).isEqualTo(expected);
    }

    @Test
    public void ifOptionalValueNotPresentOrReturnValue() {
        //given
        String defaultString = "default";
        Optional<String> value = Optional.empty();
        Optional<String> defaultValue = Optional.of(defaultString);

        //when
        Optional<String> result = value.or(() -> defaultValue);

        //then
        assertThat(result.get()).isEqualTo(defaultString);
    }

    @Test
    public void testOptionalToStream() {
        Optional<String> op = Optional.ofNullable("String value");
        Stream<String> strOptionalStream = op.stream();
        Stream<String> filteredStream = strOptionalStream.filter((str) -> str != null && str.startsWith("String"));
        assertThat(1).isEqualTo(filteredStream.count());

    }
}