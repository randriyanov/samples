package core.java9.features.module;


import org.junit.jupiter.api.Test;

import java.lang.module.ModuleFinder;
import java.lang.module.ModuleReference;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ModuleFinderTest {

  @Test
  public void testFindModule() throws Exception {
    final ModuleFinder moduleFinder = ModuleTestSupport.getModuleFinder();
    assertTrue(moduleFinder.find(ModuleTestSupport.MODULE_NAME).isPresent());
    final Set<ModuleReference> allModules = moduleFinder.findAll();
    assertEquals(4, allModules.size());
  }
}
