package core.java9.features.process;


import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class ProcessBuilderUnitTest {


    @Test
    public void processBuilderInvokeStart() throws Exception {
        ProcessBuilder processBuilder = new ProcessBuilder("java", "-version");
        processBuilder.redirectErrorStream(true);

        Process process = processBuilder.start();

        List<String> results = readOutput(process.getInputStream());
        assertThat(results.size()).isEqualTo(3);
        System.out.println(results);

        int exitCode = process.waitFor();
        assertEquals(0, exitCode, "No errors should be detected");
    }

    @Test
    public void processBuilderModifyEnvironment() throws Exception {
        ProcessBuilder processBuilder = new ProcessBuilder();
        Map<String, String> environment = processBuilder.environment();
        environment.forEach((key, value) -> System.out.println(key + value));

        environment.put("GREETING", "Hi All");

        List<String> command = Arrays.asList("/bin/bash", "-c", "echo $GREETING");
        processBuilder.command(command);
        Process process = processBuilder.start();

        List<String> results = readOutput(process.getInputStream());
        assertThat(results.size()).isEqualTo(1);
        assertThat(results).contains("Hi All");

        int exitCode = process.waitFor();
        assertThat(exitCode).isEqualTo(0);
    }

    @Test
    public void givenProcessBuilder_whenModifyWorkingDir_thenSuccess() throws Exception {
        List<String> command = Arrays.asList("/bin/sh", "-c", "ls");
        ProcessBuilder processBuilder = new ProcessBuilder(command);

        processBuilder.directory(new File("src"));
        Process process = processBuilder.start();

        List<String> results = readOutput(process.getInputStream());

        assertThat(results).contains("main", "test");

        int exitCode = process.waitFor();
        assertEquals(0, exitCode, "No errors should be detected");
    }

    @Test
    public void processBuilderOutputRedirect() throws Exception {
        ProcessBuilder processBuilder = new ProcessBuilder("java", "-version");

        processBuilder.redirectErrorStream(true);
        File log = new File("java-version.log");
        processBuilder.directory(log.getParentFile());
        processBuilder.redirectOutput(log);

        Process process = processBuilder.start();

        assertEquals(-1, process.getInputStream().read(), "If redirected, should be -1 ");
        int exitCode = process.waitFor();
        assertEquals(0, exitCode, "No errors should be detected");

        List<String> lines = Files.lines(log.toPath())
                .peek(System.out::println)
                .collect(Collectors.toList());


        assertThat(lines.toString()).contains("openjdk version");
    }

    @Test
    public void givenProcessBuilder_whenRedirectStandardOutput_thenSuccessAppending() throws Exception {
        ProcessBuilder processBuilder = new ProcessBuilder("java", "-version");

        File log = new File("java-version-append.log");
        processBuilder.redirectErrorStream(true);
        processBuilder.directory(log.getParentFile());
        processBuilder.redirectOutput(Redirect.appendTo(log));

        Process process = processBuilder.start();

        assertEquals(-1, process.getInputStream()
                .read(), "If redirected output, should be -1 ");

        int exitCode = process.waitFor();
        assertEquals(0, exitCode, "No errors should be detected");

        List<String> lines = Files.lines(log.toPath())
                .collect(Collectors.toList());

        System.out.println(lines);
    }

    //startPipeline since Java 9
    //equals to find src -name *.java -type f| wc -l
    @Test
    public void givenProcessBuilder_whenStartingPipeline_thenSuccess() throws IOException {
        List<ProcessBuilder> builders = Arrays.asList(
                new ProcessBuilder("find", "src", "-name", "*.java", "-type", "f"),
                new ProcessBuilder("wc", "-l"));
        List<Process> processes = ProcessBuilder.startPipeline(builders);
        Process last = processes.get(processes.size() - 1);

        List<String> output = readOutput(last.getInputStream());
        assertThat(output.size()).isEqualTo(1);
    }

    @Test
    public void givenProcessBuilder_whenInheritIO_thenSuccess() throws IOException, InterruptedException {
        List<String> command = Arrays.asList("/bin/sh", "-c", "echo hello");;
        ProcessBuilder processBuilder = new ProcessBuilder(command);

        processBuilder.inheritIO();
        Process process = processBuilder.start();

        int exitCode = process.waitFor();
        assertEquals(0, exitCode, "No errors should be detected");
    }

    private List<String> readOutput(InputStream inputStream) throws IOException {
        try (BufferedReader output = new BufferedReader(new InputStreamReader(inputStream))) {
            return output.lines()
                    .collect(Collectors.toList());
        }
    }

}
