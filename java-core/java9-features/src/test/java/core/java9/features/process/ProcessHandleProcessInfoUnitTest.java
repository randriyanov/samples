package core.java9.features.process;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class ProcessHandleProcessInfoUnitTest {

    Logger log = LoggerFactory.getLogger(ProcessHandleProcessInfoUnitTest.class);

    //1
    @Test
    public void givenCurrentProcessInfo() {
        ProcessHandle processHandle = ProcessHandle.current();
        ProcessHandle.Info processInfo = processHandle.info();

        assertTrue(processInfo.command().isPresent());
        assertTrue(processInfo.command().get().contains("java"));

        assertTrue(processInfo.startInstant().isPresent());
        assertTrue(processInfo.totalCpuDuration().isPresent());
        assertTrue(processInfo.user().isPresent());
    }

    //2
    @Test
    public void processToProcessHandle() throws IOException {

        String javaCmd = ProcessUtils.getJavaCmd().getAbsolutePath();

        ProcessBuilder processBuilder = new ProcessBuilder(javaCmd, "-version");
        Process process = processBuilder.inheritIO().start();
        ProcessHandle processHandle = process.toHandle();
        ProcessHandle.Info processInfo = processHandle.info();

        assertTrue(processInfo.command().isPresent());
        assertTrue(processInfo.command().get().contains("java"));
        assertTrue(processInfo.startInstant().isPresent());
        assertTrue(processInfo.user().isPresent());
    }

    //3
    @Test
    public void processHandleGetAllProcesses() throws Exception {
        String javaCmd = ProcessUtils.getJavaCmd().getAbsolutePath();
        ProcessBuilder processBuilder = new ProcessBuilder(javaCmd, "-version");
        Process process = processBuilder.inheritIO().start();

        Stream<ProcessHandle> liveProcesses = ProcessHandle.allProcesses();
        liveProcesses.filter(ProcessHandle::isAlive)
                .forEach(ph -> {
                    assertTrue(ph.info().startInstant().isPresent());
                    assertTrue(ph.info().user().isPresent());
                });
    }

    //4
    @Test
    public void processHandleGetChildProcess() throws IOException {
        String javaHome = System.getProperty("java.home");
        int childProcessCount = 5;
        for (int i = 0; i < childProcessCount; i++) {
            String javaCmd = new File(javaHome, "bin/java")
                    .getAbsolutePath();
            ProcessBuilder processBuilder
                    = new ProcessBuilder(javaCmd, "-version");
            processBuilder.inheritIO().start();
        }

        Stream<ProcessHandle> children = ProcessHandle.current()
                .children();
        children.filter(ProcessHandle::isAlive)
                .forEach(ph -> log.info("PID: {}, Cmd: {}", ph.pid(), ph.info()
                        .command()));
        Stream<ProcessHandle> descendants = ProcessHandle.current()
                .descendants();
        descendants.filter(ProcessHandle::isAlive)
                .forEach(ph -> log.info("PID: {}, Cmd: {}", ph.pid(), ph.info()
                        .command()));
    }



    @Test
    @Disabled("To run you need to pass valid pid")
    public void processHandleDestroy() {
        Optional<ProcessHandle> optionalProcessHandle = ProcessHandle.of(2737);
        ProcessHandle processHandle = optionalProcessHandle.get();
        processHandle.destroy();
        assertFalse(processHandle.isAlive());
    }


    @Test
    public void processHandleAllProcess() {
        ProcessHandle.allProcesses()
                .filter(ph -> (ph.pid() > 10000 && ph.pid() < 50000)).forEach(System.out::println);
        assertThat(((int) ProcessHandle.allProcesses()
                .filter(ph -> (ph.pid() > 10000 && ph.pid() < 50000))
                .count()) > 0);
    }

    @Test
    public void processHandleExitCallback() throws Exception {
        String javaCmd = ProcessUtils.getJavaCmd().getAbsolutePath();
        ProcessBuilder processBuilder = new ProcessBuilder(javaCmd, "-version");
        Process process = processBuilder.inheritIO().start();
        ProcessHandle processHandle = process.toHandle();

        log.info("PID: {} has started", processHandle.pid());
        CompletableFuture<ProcessHandle> onProcessExit = processHandle.onExit();
        onProcessExit.get();
        assertFalse(processHandle.isAlive());
        onProcessExit.thenAccept(ph -> {
            log.info("PID: {} has stopped", ph.pid());
        });
    }

    @Test
    public void processInfoExample() throws NoSuchAlgorithmException {
        ProcessHandle self = ProcessHandle.current();

        ProcessHandle.Info procInfo = self.info();
        Optional<String[]> args = procInfo.arguments();
        Optional<String> cmd = procInfo.commandLine();
        Optional<Instant> startTime = procInfo.startInstant();
        Optional<Duration> cpuUsage = procInfo.totalCpuDuration();

        waistCPU();
        System.out.println("Args " + args);
        System.out.println("Command " + cmd.orElse("EmptyCmd"));
        System.out.println("Start time: " + startTime.get().toString());
        System.out.println(cpuUsage.get().toMillis());

        Stream<ProcessHandle> allProc = ProcessHandle.current().children();
        allProc.forEach(p -> {
            System.out.println("Proc " + p.pid());
        });

    }

    @Test
    public void createAndDestroyProcess() throws IOException, InterruptedException {
        int numberOfChildProcesses = 5;
        for (int i = 0; i < numberOfChildProcesses; i++) {
            createNewJVM(i).pid();
        }

        Stream<ProcessHandle> childProc = ProcessHandle.current().children();
        assertEquals(childProc.count(), numberOfChildProcesses);

        childProc = ProcessHandle.current().children();
        childProc.forEach(processHandle -> {
            assertTrue(processHandle.isAlive(), "Process " + processHandle.pid() + " should be alive!");
            CompletableFuture<ProcessHandle> onProcExit = processHandle.onExit();
            onProcExit.thenAccept(procHandle -> {
                System.out.println("Process with PID " + procHandle.pid() + " has stopped");
            });
        });

        Thread.sleep(10000);

        childProc = ProcessHandle.current().children();
        childProc.forEach(procHandle -> {
            assertTrue(procHandle.destroy(), "Could not kill process ");
        });

        Thread.sleep(5000);

        childProc = ProcessHandle.current().children();
        childProc.forEach(procHandle -> {
            assertFalse(procHandle.isAlive(), "Process " + procHandle.pid() + " should not be alive!");
        });

    }

    private Process createNewJVM(int number) throws IOException {
        List<String> cmdParams = new ArrayList<>(5);
        cmdParams.add(ProcessUtils.getJavaCmd().getAbsolutePath());
        cmdParams.add("-cp");
        cmdParams.add(ProcessUtils.getClassPath());
        cmdParams.add(ServiceMain.class.getName());
        cmdParams.add("Service " + number);
        ProcessBuilder myService = new ProcessBuilder(cmdParams);
        myService.inheritIO();
        return myService.start();
    }

    private void waistCPU() throws NoSuchAlgorithmException {
        List<Integer> randArr = new ArrayList<>();
        SecureRandom sr = SecureRandom.getInstanceStrong();
        Duration somecpu = Duration.ofMillis(4200L);
        Instant end = Instant.now().plus(somecpu);
        while (Instant.now().isBefore(end)) {
            randArr.add(sr.nextInt());
        }
    }

}
