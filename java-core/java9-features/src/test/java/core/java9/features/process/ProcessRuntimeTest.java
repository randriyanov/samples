package core.java9.features.process;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ProcessRuntimeTest {

    @Test
    public void runtimeTopExample() throws IOException {
        Process process = Runtime.getRuntime().exec("top");
        BufferedReader output = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String firstLine = output.readLine();
        System.out.println(firstLine);
        assertThat(firstLine).contains("Processes");
    }


    @Test
    public void processWithErrorStream() throws IOException {
        String path = System.getProperty("user.dir") + "/process/ProcessCompilationError.java";
        Process process = Runtime.getRuntime().exec("javac " + path);
        BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        String errorString = error.readLine();
        System.out.println(errorString);
        assertNotNull(errorString);
    }
}
