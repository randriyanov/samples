package core.java9.features.string;

import org.junit.jupiter.api.Test;

public class StringTest {

    @Test
    public void testCodePointsAndChars() {
        String test = "ABCD";
        test.codePoints().boxed().map(Character::toString).forEach(System.out::println);
        test.chars().boxed().map(Character::toString).forEach(System.out::println);
    }
}
