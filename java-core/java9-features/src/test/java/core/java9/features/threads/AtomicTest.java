package core.java9.features.threads;


import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class AtomicTest {

    @Test
    public void testGetAcquire() {
        final AtomicBoolean value = new AtomicBoolean();
        value.setRelease(false);
        assertEquals(false, value.getAcquire());
    }

    @Test
    public void testCompareAndExchange() {
        final AtomicInteger value = new AtomicInteger(10);
        final int returned = value.compareAndExchange(10, 5);
        assertEquals(10, returned);
        assertEquals(5, value.getPlain());
    }
}
