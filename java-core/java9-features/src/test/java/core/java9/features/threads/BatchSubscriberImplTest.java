package core.java9.features.threads;


import core.java9.features.threads.async.BatchSubscriberImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.SubmissionPublisher;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;


class BatchSubscriberImplTest {

    private static final int ITEM_SIZE = 10;
    private SubmissionPublisher<String> publisher;
    private BatchSubscriberImpl<String> subscriber;

    @BeforeEach
    void initialize() {
        this.publisher = new SubmissionPublisher<>(ForkJoinPool.commonPool(), 6);
        this.subscriber = new BatchSubscriberImpl<>();
        publisher.subscribe(subscriber);
    }

    @Test
    void testReactiveStreamCount() throws Exception {
        IntStream.range(0, ITEM_SIZE).forEach(item -> publisher.submit(item + ""));

        publisher.close();

        while (!subscriber.isCompleted()) {
            Thread.sleep(100);
        }

        int count = subscriber.getCounter();

        assertEquals(ITEM_SIZE, count);
    }

}
