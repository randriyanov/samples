package core.java9.features.threads;


import core.java9.features.threads.async.SubscriberImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.SubmissionPublisher;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class SubscriberImplIntegrationTest {

    private static final int ITEM_SIZE = 10;
    private SubmissionPublisher<String> publisher;
    private SubscriberImpl<String> subscriber;

    @BeforeEach
    void initialize() {
        // create Publisher with max buffer capacity 3.
        this.publisher = new SubmissionPublisher<>(ForkJoinPool.commonPool(), 3);
        this.subscriber = new SubscriberImpl<String>();
        publisher.subscribe(subscriber);
    }


    @Test
    public void testReactiveStreamCount() throws Exception {
        IntStream.range(0, ITEM_SIZE).forEach(item -> publisher.submit(item + ""));
        publisher.close();

        do {
            Thread.sleep(100);
        } while (!subscriber.isCompleted());

        int count = subscriber.getCounter();

        assertEquals(ITEM_SIZE, count);
    }

    @Test
    public void testReactiveStreamTime() throws Exception {
        IntStream.range(0, ITEM_SIZE).forEach(item -> publisher.submit(item + ""));
        publisher.close();

        do {
            // wait for subscribers to complete all processing.

            Thread.sleep(100);

        } while (!subscriber.isCompleted());

        // The runtime in seconds should be equal to the number of items.
    }

    @Test
    public void testReactiveStreamOffer() throws Exception {
        IntStream.range(0, ITEM_SIZE)
                .forEach(item -> publisher.offer(item + "", (subscriber, string) -> {
                    // Returning false means this item will be dropped (no retry), if blocked.
                    return false;
                }));
        publisher.close();

        do {
            // wait for subscribers to complete all processing.
            Thread.sleep(100);
        } while (!subscriber.isCompleted());

        int count = subscriber.getCounter();
        // Because 10 items were offered and the buffer capacity was 3, few items will not be processed.
        assertTrue(ITEM_SIZE > count);
    }

}
