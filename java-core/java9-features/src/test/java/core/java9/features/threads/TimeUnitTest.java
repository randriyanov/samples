package core.java9.features.threads;


import org.junit.jupiter.api.Test;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TimeUnitTest {

  @Test
  public void testChronoUnit() throws Exception {
    assertEquals(TimeUnit.MINUTES, TimeUnit.of(ChronoUnit.MINUTES));
    assertEquals(ChronoUnit.SECONDS, TimeUnit.SECONDS.toChronoUnit());
  }
}
