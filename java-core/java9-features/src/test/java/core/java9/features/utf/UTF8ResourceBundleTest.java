package core.java9.features.utf;


import org.junit.jupiter.api.Test;

import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class UTF8ResourceBundleTest {

  @Test
  public void testGetProperty() {
    final ResourceBundle resourceBundle = ResourceBundle.getBundle("demo");
    assertEquals("你好", resourceBundle.getString("greeting"));
  }
}
