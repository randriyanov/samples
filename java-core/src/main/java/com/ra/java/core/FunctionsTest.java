package com.ra.java.core;

import java.util.function.Function;

public class FunctionsTest {
    public static void main(String[] args) {
        Function<Integer, Function<Integer, Integer>> function =
                (first) -> second -> first + second;
        Function<Integer, Integer> part = function.apply(1);
        System.out.println(part.apply(2));
    }
}
