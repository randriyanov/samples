package com.ra.java.core;

import java.util.UUID;

public class UuidMain {
    public static void main(String[] args) {

        UUID uuid = UUID.fromString("e6b89707-f085-4ed4-a963-9712217889d3");
        System.out.println(uuid.timestamp());
    }
}
