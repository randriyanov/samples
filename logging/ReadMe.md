- [Compare Logging ](https://stackify.com/compare-java-logging-frameworks/) 
- [Introduction to Java Logging](http://www.baeldung.com/java-logging-intro)
- [Introduction to SLF4J](http://www.baeldung.com/slf4j-with-log4j2-logback)
- [A Guide to Rolling File Appenders](http://www.baeldung.com/java-logging-rolling-file-appenders)
- [Logging Exceptions Using SLF4J](https://www.baeldung.com/slf4j-log-exceptions)
- [Intro to Log4j2 – Appenders, Layouts and Filters](http://www.baeldung.com/log4j2-appenders-layouts-filters)
- [Log4j 2 and Lambda Expressions](http://www.baeldung.com/log4j-2-lazy-logging)
- [Programmatic Configuration with Log4j 2](http://www.baeldung.com/log4j2-programmatic-config)
- [Creating a Custom Log4j2 Appender](https://www.baeldung.com/log4j2-custom-appender)
- [Creating a Custom Logback Appender](http://www.baeldung.com/custom-logback-appender)
- [Get Log Output in JSON Format](http://www.baeldung.com/java-log-json-output)
- [A Guide To Logback](http://www.baeldung.com/logback)