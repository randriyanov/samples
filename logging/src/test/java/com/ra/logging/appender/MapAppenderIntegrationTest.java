package com.ra.logging.appender;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.ra.logging.log4j2.MapAppender;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MapAppenderIntegrationTest {

    private Logger logger;

    @BeforeEach
    public void setup() {
        logger = LogManager.getLogger(MapAppenderIntegrationTest.class);
    }

    @Test
    public void whenLoggerEmitsLoggingEvent_thenAppenderReceivesEvent() throws Exception {
        logger.info("Test from {}", this.getClass()
                .getSimpleName());
        LoggerContext context = LoggerContext.getContext(false);
        Configuration config = context.getConfiguration();
        MapAppender appender = config.getAppender("MapAppender");
        assertEquals(appender.getEventMap().size(), 1);
    }

}
