#Tutorials

#https://www.javacodegeeks.com/2014/12/java-servlet-tutorial.html
#http://tutorials.jenkov.com/software-architecture/index.html
#https://tekslate.com/tutorials/weblogic-tutorials-how-to/
#JNDI
#http://www.codejava.net/servers/tomcat/configuring-jndi-datasource-for-database-connection-pooling-in-tomcat

[https://www.javacodegeeks.com/java-servlet-tutorials]

1. Servlets -> process http requests
2. Servlets container vs web server
3. Dependencies
    a.javax.servlet:javax.servlet-api:4.0.1
    b.javax.servlet.jsp:javax.servlet.jsp-api:2.3.1
    c.javax.servlet:jstl:1.2
4. Plugin war plugin
5. Tomcat, jetty or any other servlet container
6. web.xml vs annotation
7. main -> webapp -> WEB-INF -> web.xml
8. web.xml -> startup descriptor
9. Application servlet: (HttpServlet, GenericServlet) -> classes, 
                            Servlet -> interface
10. web.xml servlet-name, servlet-class, url-pattern
11. gradle build -> ***.war -> put in tomcat -> webapps
12. on webapp level everything is open
13. WEB-INF can't have direct access(only via RequestDispatcher)
14. WEB-INF: classes, jsp(jsf), lib(third party added via gradle or maven)
15. tomcat -> unzip war -> search web.xml -> 
                                something like map -> "url-pattern": servlet-mapping
16. Servlets life cycle:
            1. init -> only once and it is thread safe!!! load-on-startup:0
            2. service -> POST: doPost
                           GET: doGet
                           PUT: doPut
            3. destroy
17. Spring Bean Life Cycle, Servlets Life Cycle, Hibernate Entity Life Cycle
18. @WebServlet(no need web.xml at all), but should implement or extend servlet
19. Search all with WebServlet -> 
                                 something like map -> "url-pattern": servlet-mapping
20. EventListener(callbacks)
21. Embedded servers: Akka server, Vertx, Grizly, Tomcat, Jetty...
22. extend servlet: doPost, doGet etc
            HttpServletRequest request, HttpServletResponse response
23. request.getInputStream()
    response.getWriter.print
24. Security:
        Authentication: Login: login and password are correct!
        Authorization: What we are allowed to do in system
24. Redirect(one of 3* http code) vs Forward(inside our web server)
