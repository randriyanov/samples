package com.ra.courses.filter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(urlPatterns = {"/TestServlet", "/filtered/TestServlet"})
public class TestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println(request.getMethod());
        out.println(request.getRequestURL());
        out.println(request.getProtocol());
        out.println(request.getRemoteAddr());
        out.println(request.getContextPath());
        out.println(request.getScheme());
        response.setContentType("text/html;charset=UTF-8");
        out.print("bar");
    }
}
