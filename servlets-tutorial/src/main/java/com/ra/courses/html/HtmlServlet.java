package com.ra.courses.html;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/html-servlet", "/servlet-html"})
public class HtmlServlet extends HttpServlet {

	@Override
	protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		String login = req.getParameter("login");
		String password = req.getParameter("password");
		User user = new User(login, password);
		req.setAttribute("user", user);
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login-html.jsp").forward(req, resp);
	}
}
