package com.ra.courses.model;

public class UserResponseDto {
    private Integer id;
    private String identification;
    private Integer qtyUsers;
    private String name;

    public UserResponseDto() {
    }

    public UserResponseDto(String identification, Integer qtyUsers) {
        this.identification = identification;
        this.qtyUsers = qtyUsers;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public Integer getQtyUsers() {
        return qtyUsers;
    }

    public void setQtyUsers(Integer qtyUsers) {
        this.qtyUsers = qtyUsers;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
