package com.ra.courses.mvcfactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ra.courses.mvcfactory.dto.AutosalonDTO;
import com.ra.courses.mvcfactory.service.AutosalonService;

public class AutosalonHandler implements InternalHandler {

	private AutosalonService service;

	@Override
	public void post(final HttpServletRequest request, final HttpServletResponse response) {
		String autosalonName = request.getParameter("autosalon_name");
		AutosalonDTO dto = new AutosalonDTO();
		dto.setName(autosalonName);

		service.saveAutosalon(dto);

	}

	@Override
	public void get(final HttpServletRequest request, final HttpServletResponse response) {

	}


}
