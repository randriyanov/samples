package com.ra.courses.mvcfactory;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HandlerFactory {

	private static Map<String, InternalHandler> handlers = new HashMap<>();

	static {
		handlers.put("/autosalon", new AutosalonHandler());
	}

	public void handleGetRequest(String path, HttpServletRequest request, HttpServletResponse response) {
		if(handlers.containsKey(path)) {
			InternalHandler handler = handlers.get(path);
			handler.get(request, response);
		}
	}

}
