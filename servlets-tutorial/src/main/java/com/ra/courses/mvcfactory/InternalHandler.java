package com.ra.courses.mvcfactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface InternalHandler {

	void post(HttpServletRequest request, HttpServletResponse response);

	void get(HttpServletRequest request, HttpServletResponse response);

}
