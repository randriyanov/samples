package com.ra.courses.mvcfactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/all")
public class InternalServlet extends HttpServlet {

	private HandlerFactory handlerFactory;

	@Override
	public void init()
			throws ServletException {
		handlerFactory = new HandlerFactory();
	}

	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		handlerFactory.handleGetRequest(getPath(req), req, resp);
	}

	@Override
	protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		super.doPost(req, resp);
	}

	private String getPath(final HttpServletRequest req) {
		return req.getPathInfo();
	}
}
