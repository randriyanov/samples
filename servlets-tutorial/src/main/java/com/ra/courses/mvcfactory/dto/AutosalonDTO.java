package com.ra.courses.mvcfactory.dto;

public class AutosalonDTO {
	private String name;
	private String phone;
	private String address;

	public AutosalonDTO() {
	}

	public AutosalonDTO(final String name, final String phone, final String address) {
		this.name = name;
		this.phone = phone;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}
}
