package com.ra.courses.mvcfactory.service;

import com.ra.courses.mvcfactory.dao.AutosalonDao;
import com.ra.courses.mvcfactory.dto.AutosalonDTO;
import com.ra.courses.mvcfactory.entity.Autosalon;

public class AutosalonService {

	private AutosalonDao dao;

	public int saveAutosalon(AutosalonDTO dto) {
		Autosalon autosalon = new Autosalon();
		return dao.saveAutosalon(autosalon);
	}
}
