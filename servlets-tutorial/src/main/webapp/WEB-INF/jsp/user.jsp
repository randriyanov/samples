<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>${pageContext.request.serverPort}</h1>

<table border="1" cellpadding="5">
    <caption><h2>List of users</h2></caption>
    <tr>
        <th>Name</th>
        <th>Email</th>
    </tr>
    <c:forEach var="user" items="${nikolay}">
        <tr>
            <td><c:out value="${user.name}" /></td>
            <td><c:out value="${user.secondName}" /></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
