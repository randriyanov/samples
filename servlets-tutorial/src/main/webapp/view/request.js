function getParams() {
  let password = document.getElementById('pwd').value;
  let name = document.getElementById('usr').value;
  let body = 'password=' + password + '&' + 'name=' + name;
  req('http://localhost:8081/user', body, 'POST')
}


function req(url, body, method) {
  return new Promise(function (resolve, reject) {
    let req = new XMLHttpRequest();
    req.open(method, url, true);
    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    req.onload = function () {
      if (req.status === 200) {
        resolve(req.response);
      } else {
        reject(console.log(Error(req.statusText)));
      }
    };
    req.onerror = function () {
      reject(Error("Network Error"));
    };
    req.send(body);
  });
}
