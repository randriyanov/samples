package com.ra.courses.entity;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
