1. Spring in Action
2. Spring Recipes 5th
3. Pro Spring 5th
4. https://www.youtube.com/watch?v=cou_qomYLNU

Spring Framework Architecture -> [https://www.javacodegeeks.com/2019/02/spring-framework-architecture.html?utm_source=sendpulse&utm_medium=push&utm_campaign=855709]

Spring stages -> [https://habr.com/ru/post/222579/]
[https://en.wikipedia.org/wiki/Inversion_of_control]

Spring configuration -> [https://www.logicbig.com/tutorials/spring-framework/spring-core/configuration-metadata.html]

Spring Java-Based Configuration -> [https://www.logicbig.com/tutorials/spring-framework/spring-core/java-config.html]

Spring @Bean -> [https://www.logicbig.com/tutorials/spring-framework/spring-core/using-bean-annotation.html]