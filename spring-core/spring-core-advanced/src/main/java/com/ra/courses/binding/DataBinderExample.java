package com.ra.courses.binding;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.validation.DataBinder;

public class DataBinderExample {
    public static void main (String[] args) {

        MutablePropertyValues mpv = new MutablePropertyValues();
        mpv.add("anInt", "10");

        TestBean testBean = new TestBean();
        System.out.println(testBean.getAnInt());
        DataBinder db = new DataBinder(testBean);

        db.bind(mpv);
        System.out.println(testBean.getAnInt());

    }
}
