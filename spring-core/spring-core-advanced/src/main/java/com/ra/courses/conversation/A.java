package com.ra.courses.conversation;

public class A {

    private String test;

    public A(String test) {
        this.test = test;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
