package com.ra.courses.conversation;

public class B {
    private String test;

    public B(String test) {
        this.test = test;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
