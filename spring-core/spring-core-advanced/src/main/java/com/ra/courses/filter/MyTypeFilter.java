package com.ra.courses.filter;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

import java.io.IOException;
import java.util.Arrays;


public class MyTypeFilter implements TypeFilter {
    private static final String RunnableName = Runnable.class.getName();

    @Override
    public boolean match(MetadataReader metadataReader,
                         MetadataReaderFactory metadataReaderFactory) throws IOException {
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        String[] interfaceNames = classMetadata.getInterfaceNames();
        if (Arrays.stream(interfaceNames).anyMatch(RunnableName::equals)) {
            return true;
        }
        return false;
    }

    @Configuration
    @ComponentScan(useDefaultFilters = false,
            includeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, classes = MyAnnotation.class)})
    public static class FilterTypeAnnotationExample {
        public static void main(String[] args) {
            ApplicationContext context =
                    new AnnotationConfigApplicationContext(FilterTypeAnnotationExample.class);
            Util.printBeanNames(context);
        }
    }
}