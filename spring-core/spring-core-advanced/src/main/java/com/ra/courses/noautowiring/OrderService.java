package com.ra.courses.noautowiring;

public interface OrderService {

    String getOrderDetails(String orderId);
}
