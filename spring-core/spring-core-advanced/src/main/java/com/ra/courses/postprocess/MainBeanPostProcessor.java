package com.ra.courses.postprocess;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainBeanPostProcessor {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SimpleContext.class);
        MyBean myBean = context.getBean(MyBean.class);
        System.out.println(myBean.getValue2());
    }
}
