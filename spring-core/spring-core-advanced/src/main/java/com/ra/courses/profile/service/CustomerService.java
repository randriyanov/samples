package com.ra.courses.profile.service;


import com.ra.courses.profile.Customer;

import java.util.List;

public class CustomerService extends DataService<Customer> {

    //not showing transactional stuff

    public Customer getCustomerById(long id) {
        return getDao().load(id);
    }

    public void updateCustomer(Customer customer) {
        getDao().saveOrUpdate(customer);
    }

    public void saveCustomer(Customer customer) {
        getDao().saveOrUpdate(customer);
    }

    public List<Customer> getAllCustomers() {
        return getDao().loadAll();
    }
}
