package com.ra.courses.profile.service;

import org.springframework.beans.factory.annotation.Autowired;

public abstract class DataService<T> {

    @Autowired
    private Dao<T> dao;

    protected Dao<T> getDao() {
        return dao;
    }
}
