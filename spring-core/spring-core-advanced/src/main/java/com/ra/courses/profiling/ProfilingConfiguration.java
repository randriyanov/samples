package com.ra.courses.profiling;

import com.ra.courses.profiling.classes.Quotes;
import com.ra.courses.profiling.classes.TerminalQuoter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.ra.courses.profiling"})
public class ProfilingConfiguration {

    @Bean
    public Quotes terminalQuoter() {
        return new TerminalQuoter();
    }

}
