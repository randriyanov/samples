package com.ra.courses.profiling.classes;

public interface ProfilingControllerMBean {
    void setEnabled(boolean enabled);
}
