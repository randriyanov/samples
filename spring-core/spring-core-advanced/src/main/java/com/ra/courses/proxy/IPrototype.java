package com.ra.courses.proxy;

public interface IPrototype {

    String getDateTime();
}
