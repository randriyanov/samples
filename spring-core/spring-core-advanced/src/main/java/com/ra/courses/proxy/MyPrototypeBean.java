package com.ra.courses.proxy;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

public class MyPrototypeBean implements IPrototype {

    private String dateTimeString = LocalDateTime.now().toString();

    @Override
    public String getDateTime() {
        return dateTimeString;
    }
}
