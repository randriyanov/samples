package com.ra.courses.spel;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CarApplication {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(GlobalConfiguration.class);
        Car car = context.getBean(Car.class);
        System.out.println(car.getMake());

    }
}
