package com.ra.courses.spel;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.ra.courses.spel")
public class GlobalConfiguration {
}
