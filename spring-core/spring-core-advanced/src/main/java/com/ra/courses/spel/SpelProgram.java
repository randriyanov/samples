package com.ra.courses.spel;

import com.ra.courses.resources.InjectFileResourceDataExample;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpelProgram {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        GlobalConfiguration.class);
        SpelCollections spelCollections = (SpelCollections) context.getBean("spelCollections");

        // Here you can choose which bean do you want to load instead of spelConditional: spelCollections, spelLogical, etc.

        System.out.println(spelCollections);
    }

}
