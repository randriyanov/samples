import com.ra.courses.profile.DataConfig;
import com.ra.courses.profile.service.Dao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;


@ContextConfiguration(classes = DataConfig.class)
public class ProfileTesting {

    @Autowired
    private Dao dao;

    @Test
    public void testProfile() {
        System.out.println(dao);
    }

}
