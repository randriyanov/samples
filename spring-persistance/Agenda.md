1. Hibernate(entity life cycle)
2. Hibernate main methods(persist, detach,delete, find)
3. Hibernate first level cache(Second Level cache, Query Cache(Third Level Cache))
4. HibernateSessionFactory
5. spring-orm(
LocalContainerEntityManagerFactoryBean(no xml),
LocalEntityManagerFactoryBean(persistence.xml)
LocalSessionFactoryBean(HibernateSessionFactory)
)
6. Autowire EntityManager, or SessionFactory
7.spring-data(
LocalContainerEntityManagerFactoryBean,
JpaTransactionManager
@EnableJpaRepositories("com.ra.courses.data") -> interfaces extends Repository
)
8. JpaRepository extends PagingAndSorting extends CrudRepository extends Repository
Query Creation
@Query
9. Transaction API