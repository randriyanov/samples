package com.ra.sample.boot.jdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.stream.Stream;

@SpringBootApplication
//@ComponentScan(basePackages = "com.ra.sample.boot.jdbc")
public class SpringBootJdbcSample {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SpringBootJdbcSample.class);

//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringBootJdbcSample.class);
//        context.getBean(VehicleDao.class).insert(new Vehicle("1", "black", 4, 4));

        Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);
    }
}
