CREATE TABLE public.vehicle
(
    vehicle_id INTEGER PRIMARY KEY,
    vehicle_no varchar(255),
    color      varchar(255),
    wheel      INTEGER,
    seat       INTEGER
);