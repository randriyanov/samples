package com.ra.sample.boot.jdbc;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = SpringBootJdbcSample.class)
@TestPropertySource("classpath:application.yml")
public class JdbcVehicelDaoImplIntegrationTest {

    @Autowired
    private JdbcVehicleDaoImpl jdbcVehicleDao;

    @Test
    public void testInsert() {
        jdbcVehicleDao.insert(new Vehicle("4", "black", 4, 5));

        Vehicle vehicle = jdbcVehicleDao.findByVehicleNo("4");

        assertThat(vehicle).isNotNull();
    }
}
