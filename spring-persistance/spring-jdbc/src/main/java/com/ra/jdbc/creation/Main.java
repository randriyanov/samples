package com.ra.jdbc.creation;

import com.ra.jdbc.model.Vehicle;
import com.ra.jdbc.model.VehicleDao;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(VehicleConfiguration.class);
        VehicleDao vehicleDao = context.getBean(VehicleDao.class);
        List<Vehicle> allVehicles = vehicleDao.findAll();
        System.out.println(allVehicles.size());
        vehicleDao.insert(new Vehicle("1234556", "red", 4, 5));
        allVehicles = vehicleDao.findAll();
        System.out.println(allVehicles.size());
    }
}
