CREATE TABLE vehicle
(
    vehicle_id bigint identity not null,
    vehicle_no varchar(255),
    color      varchar(255),
    wheel      INTEGER,
    seat       INTEGER
);