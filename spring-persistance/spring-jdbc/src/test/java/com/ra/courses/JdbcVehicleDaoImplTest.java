package com.ra.courses;


import com.ra.jdbc.creation.JdbcVehicleDaoImpl;
import com.ra.jdbc.creation.VehicleConfiguration;
import com.ra.jdbc.model.Vehicle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

//@ExtendWith(SpringExtension.class) --> Junit 4
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {VehicleConfiguration.class})
//@TestPropertySource("classpath:test_jdbc.properties")
@Sql(statements = {"CREATE TABLE Vehicle(\n" +
        "    vehicle_id SERIAL PRIMARY KEY,\n" +
        "    vehicle_no varchar(255),\n" +
        "    color varchar(255),\n" +
        "    wheel INTEGER,\n" +
        "    seat INTEGER\n" +
        ");"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(statements = {"DROP TABLE Vehicle"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class JdbcVehicleDaoImplTest {

    @Autowired
    private JdbcVehicleDaoImpl jdbcVehicleDao;

    @Test
    public void testInsert() {
        Vehicle vehicle = new Vehicle();
        vehicle.setColor("Red");
        vehicle.setSeat(5);
        vehicle.setVehicleNo("12345");
        vehicle.setWheel(4);
        jdbcVehicleDao.insert(vehicle);
        int all = jdbcVehicleDao.countAll();
        System.out.println(all);
    }
}
