package com.ra.courses;

import com.ra.jdbc.basic.PlainJdbcVehicleDao;
import com.ra.jdbc.config.VehicleConfiguration;
import com.ra.jdbc.model.Vehicle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;;

import java.util.List;

public class PlainJdbcVehicleDaoTest {
    private VehicleConfiguration configuration = new VehicleConfiguration();
    private PlainJdbcVehicleDao vehicleDao;


    @BeforeEach
    public void before() {
        vehicleDao = new PlainJdbcVehicleDao(configuration.simpleDataSource());
    }

    @Test
    @Disabled
    public void testInsertIsSuccessful() {
        Vehicle vehicle = new Vehicle();
        vehicle.setColor("Red");
        vehicle.setSeat(5);
        vehicle.setVehicleNo("12345");
        vehicle.setWheel(4);
        vehicleDao.insert(vehicle);
        List<Vehicle> list = vehicleDao.findAll();
        Assertions.assertTrue(list.size() == 2);
    }
}
