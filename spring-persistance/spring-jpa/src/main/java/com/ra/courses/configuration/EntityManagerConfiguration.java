package com.ra.courses.configuration;

import javax.persistence.EntityManagerFactory;

import com.ra.courses.basic.JpaCourseDao;
import com.ra.courses.basic.JpaCourseDaoEntityManger;
import com.ra.courses.model.CourseDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;

public class EntityManagerConfiguration {

    @Bean
    public JpaCourseDaoEntityManger courseDao(EntityManagerFactory entityManagerFactory) {
        return new JpaCourseDaoEntityManger(entityManagerFactory);
    }

    @Bean
    public LocalEntityManagerFactoryBean entityManagerFactory() {

        LocalEntityManagerFactoryBean emf = new LocalEntityManagerFactoryBean();
        emf.setPersistenceUnitName("course");
        return emf;
    }
}
