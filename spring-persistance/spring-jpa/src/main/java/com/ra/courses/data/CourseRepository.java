package com.ra.courses.data;

import com.ra.courses.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.NamedQuery;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Course findCourseByTitle(String title);
 *
 *     Course findCourseByTitleAndFee(String title, int fee);
 *
 *     List<Course> findByTitleAndFee(String title, int fee);
 *
 *     Stream<Course> findAllByTitle(String title);
 *
 *     @Query("select c from Course c where c.fee = ?1 and c.title = ?2")
 *     Course findByCustomParams(long fee, String title);
 *
 *     Course findByTitle(String title);
 */
public interface CourseRepository extends JpaRepository<Course, Long> {

    List<Course> findAllByBeginDateBetween(Date begin, Date endDate);

    Course namedFindByTitle(String title);

}
