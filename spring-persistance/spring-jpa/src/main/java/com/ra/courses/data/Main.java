package com.ra.courses.data;

import com.ra.courses.model.Course;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(CourseConfiguration.class);
        CourseRepository courseDao = context.getBean(CourseRepository.class);

        Course course = new Course();
        course.setTitle("Core Spring");
        course.setBeginDate(new GregorianCalendar(2007, 8, 1).getTime());
        course.setEndDate(new GregorianCalendar(2007, 9, 1).getTime());
        course.setFee(1000);
        courseDao.save(course);

        Course courseTwo = new Course();
        courseTwo.setTitle("Core hibernate");
        courseTwo.setBeginDate(new GregorianCalendar(2008, 10, 1).getTime());
        courseTwo.setEndDate(new GregorianCalendar(2008, 11, 1).getTime());
        courseTwo.setFee(5000);
        List<Course> courses = List.of(course, courseTwo);
        courseDao.saveAll(courses);

        List<Course> coursesByDate = courseDao.findAllByBeginDateBetween(
                new GregorianCalendar(2006, 8, 1).getTime(),
                new GregorianCalendar(2008, 11, 1).getTime());
        Pageable pageable = PageRequest.of(1, 10);
        Page<Course> page = courseDao.findAll(pageable);
        System.out.println(page.getTotalPages());
        System.out.println(coursesByDate.size());



//        System.out.println("\nCourse before persisting");
//        System.out.println(course);
//
//        Course persisted = courseDao.save(course);
//
//        System.out.println("\nCourse after persisting");
//        System.out.println(persisted);
//
//        Long courseId = persisted.getId();
//
//        System.out.println("\nCourse fresh from database");
//        courseDao.findById(courseId).ifPresent(System.out::println);
//
//        //courseDao.deleteById(courseId);
//        Course findTitle = courseDao.findCourseByTitle("Core Spring");
//        System.out.println("We have found course");
//        System.out.println(findTitle);
    }
}
