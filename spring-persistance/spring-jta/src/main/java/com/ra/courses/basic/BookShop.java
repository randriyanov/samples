package com.ra.courses.basic;

public interface BookShop {

    void purchase(String isbn, String username);

    void purchaseWithTransactionControll(String isbn, String username);

}
