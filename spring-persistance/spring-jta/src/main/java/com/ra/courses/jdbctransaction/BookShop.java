package com.ra.courses.jdbctransaction;

public interface BookShop {

    void purchase(String isbn, String username);

}
