package com.ra.courses.jdbctransactional;

public interface BookShop {

    public void purchase(String isbn, String username);

}
