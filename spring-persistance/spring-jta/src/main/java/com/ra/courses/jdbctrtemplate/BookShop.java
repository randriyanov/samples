package com.ra.courses.jdbctrtemplate;

public interface BookShop {

    public void purchase(String isbn, String username);

}
