package com.ra.courses.propogation;

public interface BookShop {

    public void purchase(String isbn, String username);

}
