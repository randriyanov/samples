package com.ra.courses.propogation;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement()
public class BookstoreConfiguration {

    @Bean
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(org.postgresql.Driver.class.getName());
        dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres");
        return dataSource;
    }

    @Bean
    public DataSourceTransactionManager transactionManager(DataSource dataSource) {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource);
        return transactionManager;
    }

    @Bean
    public BookShop bookShop(DataSource dataSource, JdbcBookShopUser bookShopUser ) {
        JdbcBookShop bookShop = new JdbcBookShop(bookShopUser);
        bookShop.setDataSource(dataSource);
        return bookShop;
    }

    @Bean
    public JdbcBookShopUser bookShopUser(DataSource dataSource) {
        JdbcBookShopUser bookShop = new JdbcBookShopUser();
        bookShop.setDataSource(dataSource);
        return bookShop;
    }

    @Bean
    public Cashier cashier(BookShop bookShop) {
        BookShopCashier cashier = new BookShopCashier();
        cashier.setBookShop(bookShop);
        return cashier;
    }
}
