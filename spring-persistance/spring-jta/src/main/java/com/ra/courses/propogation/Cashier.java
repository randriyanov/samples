package com.ra.courses.propogation;

import java.util.List;


public interface Cashier {

    void checkout(List<String> isbns, String username);
}
