package com.ra.courses.propogation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class JdbcBookShop extends JdbcDaoSupport implements BookShop {

    @Autowired
    private final JdbcBookShopUser bookShopUser;

    public JdbcBookShop(JdbcBookShopUser bookShopUser) {
        this.bookShopUser = bookShopUser;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void purchase(final String isbn, final String username) {
        getJdbcTemplate().update(
                "UPDATE BOOK_STOCK SET STOCK = STOCK - 1 WHERE ISBN = ?", isbn );
        bookShopUser.purchase(isbn, username);
    }
}
