package com.ra.courses.uncommited;

import java.util.List;


public interface Cashier {
    void checkout(List<String> isbns, String username);
}
