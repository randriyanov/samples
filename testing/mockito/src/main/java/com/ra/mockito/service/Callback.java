package com.ra.mockito.service;

public interface Callback<T> {

    void reply(T response);
}
