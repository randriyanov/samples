package com.ra.mockito.service;

public interface Service {

    void doAction(String request, Callback<Response> callback);

}
