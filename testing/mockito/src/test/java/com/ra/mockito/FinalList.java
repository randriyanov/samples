package com.ra.mockito;

public final class FinalList extends MyList {

    @Override
    public int size() {
        return 1;
    }

}
