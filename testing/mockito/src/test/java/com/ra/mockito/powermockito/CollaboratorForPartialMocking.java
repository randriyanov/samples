package com.ra.mockito.powermockito;

class CollaboratorForPartialMocking {

    static String staticMethod() {
        return "Hello Static Method!";
    }

    final String finalMethod() {
        return "Hello Final Method!";
    }

    private String privateMethod() {
        return "Hello Provate Method!";
    }

    String privateMethodCaller() {
        return privateMethod() + " Welcome to the Java world.";
    }
}