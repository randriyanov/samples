package com.ra.mockito.powermockito;

class CollaboratorWithFinalMethods {

    final String helloMethod() {
        return "Hello World!";
    }

}
