package com.ra.utility.jackson.xmlToJson;

public enum Color {
    PINK, BLUE, YELLOW, RED;
}
