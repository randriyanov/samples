package com.ra.utility.jackson.enums;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

class JacksonEnumSerializationUnitTest {

    @Test
    final void givenEnum_whenSerializingJson_thenCorrectRepresentation() throws JsonParseException, IOException {
        final String dtoAsString = new ObjectMapper().writeValueAsString(Distance.MILE);

        assertThat(dtoAsString).contains("1609.34");
    }

}
