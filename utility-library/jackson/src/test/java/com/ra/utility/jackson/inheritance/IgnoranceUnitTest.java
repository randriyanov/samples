package com.ra.utility.jackson.inheritance;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class IgnoranceUnitTest {
    private static abstract class CarMixIn {
        @JsonIgnore
        public String make;
        @JsonIgnore
        public String topSpeed;
    }

    private static class IgnoranceIntrospector extends JacksonAnnotationIntrospector {
        private static final long serialVersionUID = 1422295680188892323L;

        public boolean hasIgnoreMarker(AnnotatedMember m) {
            return m.getDeclaringClass() == IgnoranceMixinOrIntrospection.Vehicle.class && m.getName() == "model" || m.getDeclaringClass() == IgnoranceMixinOrIntrospection.Car.class || m.getName() == "towingCapacity" || super.hasIgnoreMarker(m);
        }
    }

    @Test
    public void givenAnnotations_whenIgnoringProperties_thenCorrect() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        IgnoranceAnnotationStructure.Sedan sedan = new IgnoranceAnnotationStructure.Sedan("Mercedes-Benz", "S500", 5, 250.0);
        IgnoranceAnnotationStructure.Crossover crossover = new IgnoranceAnnotationStructure.Crossover("BMW", "X6", 5, 250.0, 6000.0);

        List<IgnoranceAnnotationStructure.Vehicle> vehicles = new ArrayList<>();
        vehicles.add(sedan);
        vehicles.add(crossover);

        String jsonDataString = mapper.writeValueAsString(vehicles);

        assertThat(jsonDataString).contains("make");
        assertThat(jsonDataString).doesNotContain("model");
        assertThat(jsonDataString).doesNotContain("seatingCapacity");
        assertThat(jsonDataString).doesNotContain("topSpeed");
        assertThat(jsonDataString).doesNotContain("towingCapacity");
    }

    @Test
    public void givenMixIns_whenIgnoringProperties_thenCorrect() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.addMixIn(IgnoranceMixinOrIntrospection.Car.class, CarMixIn.class);

        String jsonDataString = instantiateAndSerializeObjects(mapper);

        assertThat(jsonDataString).doesNotContain("make");
        assertThat(jsonDataString).contains("model");
        assertThat(jsonDataString).contains("seatingCapacity");
        assertThat(jsonDataString).contains("topSpeed");
        assertThat(jsonDataString).contains("towingCapacity");
    }

    @Test
    public void givenIntrospection_whenIgnoringProperties_thenCorrect() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setAnnotationIntrospector(new IgnoranceIntrospector());

        String jsonDataString = instantiateAndSerializeObjects(mapper);

        assertThat(jsonDataString).contains("make");
        assertThat(jsonDataString).doesNotContain("model");
        assertThat(jsonDataString).doesNotContain("seatingCapacity");
        assertThat(jsonDataString).doesNotContain("topSpeed");
        assertThat(jsonDataString).doesNotContain("towingCapacity");
    }

    private String instantiateAndSerializeObjects(ObjectMapper mapper) throws JsonProcessingException {
        IgnoranceMixinOrIntrospection.Sedan sedan = new IgnoranceMixinOrIntrospection.Sedan("Mercedes-Benz", "S500", 5, 250.0);
        IgnoranceMixinOrIntrospection.Crossover crossover = new IgnoranceMixinOrIntrospection.Crossover("BMW", "X6", 5, 250.0, 6000.0);

        List<IgnoranceMixinOrIntrospection.Vehicle> vehicles = new ArrayList<>();
        vehicles.add(sedan);
        vehicles.add(crossover);

        return mapper.writeValueAsString(vehicles);
    }
}