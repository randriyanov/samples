package com.ra.utility.jackson.test;


import com.ra.utility.jackson.field.MyDtoAccessLevel;
import com.ra.utility.jackson.field.MyDtoWithGetter;
import com.ra.utility.jackson.field.MyDtoWithSetter;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class JacksonFieldUnitTest {

    @Test
    public final void givenDifferentAccessLevels_whenPublic_thenSerializable() throws JsonProcessingException {
        final ObjectMapper mapper = new ObjectMapper();

        final MyDtoAccessLevel dtoObject = new MyDtoAccessLevel();

        final String dtoAsString = mapper.writeValueAsString(dtoObject);
        assertThat(dtoAsString).doesNotContain("stringValue");
        assertThat(dtoAsString).doesNotContain("intValue");
        assertThat(dtoAsString).doesNotContain("floatValue");
        assertThat(dtoAsString).contains("booleanValue");
        System.out.println(dtoAsString);
    }

    @Test
    public final void givenDifferentAccessLevels_whenGetterAdded_thenSerializable() throws JsonProcessingException {
        final ObjectMapper mapper = new ObjectMapper();

        final MyDtoWithGetter dtoObject = new MyDtoWithGetter();

        final String dtoAsString = mapper.writeValueAsString(dtoObject);
        assertThat(dtoAsString).contains("stringValue");
        assertThat(dtoAsString).doesNotContain("intValue");
        System.out.println(dtoAsString);
    }

    @Test
    public final void givenDifferentAccessLevels_whenGetterAdded_thenDeserializable() throws IOException {
        final String jsonAsString = "{\"stringValue\":\"dtoString\"}";
        final ObjectMapper mapper = new ObjectMapper();

        final MyDtoWithGetter dtoObject = mapper.readValue(jsonAsString, MyDtoWithGetter.class);

        assertNotNull(dtoObject);
        assertThat(dtoObject.getStringValue()).isEqualTo("dtoString");
    }

    @Test
    public final void givenDifferentAccessLevels_whenSetterAdded_thenDeserializable() throws IOException {
        final String jsonAsString = "{\"intValue\":1}";
        final ObjectMapper mapper = new ObjectMapper();

        final MyDtoWithSetter dtoObject = mapper.readValue(jsonAsString, MyDtoWithSetter.class);

        assertNotNull(dtoObject);
        assertThat(dtoObject.accessIntValue()).isEqualTo(1);
    }

    @Test
    public final void givenDifferentAccessLevels_whenSetterAdded_thenStillNotSerializable() throws IOException {
        final ObjectMapper mapper = new ObjectMapper();

        final MyDtoWithSetter dtoObject = new MyDtoWithSetter();

        final String dtoAsString = mapper.writeValueAsString(dtoObject);
        assertThat(dtoAsString).doesNotContain("intValue");
        System.out.println(dtoAsString);
    }

    @Test
    public final void givenDifferentAccessLevels_whenSetVisibility_thenSerializable() throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

        final MyDtoAccessLevel dtoObject = new MyDtoAccessLevel();

        final String dtoAsString = mapper.writeValueAsString(dtoObject);
        assertThat(dtoAsString).contains("stringValue");
        assertThat(dtoAsString).contains("intValue");
        assertThat(dtoAsString).contains("booleanValue");
        System.out.println(dtoAsString);
    }

}
