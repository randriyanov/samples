package com.ra.utility.jackson.test;


import com.ra.utility.jackson.jsonview.Item;
import com.ra.utility.jackson.jsonview.MyBeanSerializerModifier;
import com.ra.utility.jackson.jsonview.User;
import com.ra.utility.jackson.jsonview.Views;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.BeanSerializerFactory;
import com.fasterxml.jackson.databind.ser.SerializerFactory;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class JacksonJsonViewUnitTest {

    @Test
    public void whenUseJsonViewToSerialize_thenCorrect() throws JsonProcessingException {
        final User user = new User(1, "John");

        final ObjectMapper mapper = new ObjectMapper();
        mapper.disable(MapperFeature.DEFAULT_VIEW_INCLUSION);

        final String result = mapper.writerWithView(Views.Public.class)
            .writeValueAsString(user);

        assertThat(result).contains("John");
        assertThat(result).doesNotContain("1");
    }

    @Test
    public void whenUsePublicView_thenOnlyPublicSerialized() throws JsonProcessingException {
        final Item item = new Item(2, "book", "John");

        final ObjectMapper mapper = new ObjectMapper();
        final String result = mapper.writerWithView(Views.Public.class)
            .writeValueAsString(item);

        assertThat(result).contains("book");
        assertThat(result).contains("2");

        assertThat(result).doesNotContain("John");
    }

    @Test
    public void whenUseInternalView_thenAllSerialized() throws JsonProcessingException {
        final Item item = new Item(2, "book", "John");

        final ObjectMapper mapper = new ObjectMapper();
        final String result = mapper.writerWithView(Views.Internal.class)
            .writeValueAsString(item);

        assertThat(result).contains("book");
        assertThat(result).contains("2");

        assertThat(result).contains("John");
    }

    @Test
    public void whenUseJsonViewToDeserialize_thenCorrect() throws IOException {
        final String json = "{\"id\":1,\"name\":\"John\"}";

        final ObjectMapper mapper = new ObjectMapper();

        final User user = mapper.readerWithView(Views.Public.class)
            .forType(User.class)
            .readValue(json);
        assertEquals(1, user.getId());
        assertEquals("John", user.getName());
    }

    @Test
    public void whenUseCustomJsonViewToSerialize_thenCorrect() throws JsonProcessingException {
        final User user = new User(1, "John");
        final SerializerFactory serializerFactory = BeanSerializerFactory.instance.withSerializerModifier(new MyBeanSerializerModifier());

        final ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializerFactory(serializerFactory);

        final String result = mapper.writerWithView(Views.Public.class)
            .writeValueAsString(user);
        assertThat(result).contains("JOHN");
        assertThat(result).contains("1");
    }
}
