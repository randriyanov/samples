package com.ra.utility.jackson.test;


import com.ra.utility.jackson.dtos.enums.DistanceEnumSimple;
import com.ra.utility.jackson.dtos.enums.DistanceEnumWithJsonFormat;
import com.ra.utility.jackson.dtos.enums.DistanceEnumWithValue;
import com.ra.utility.jackson.dtos.enums.MyDtoWithEnumCustom;
import com.ra.utility.jackson.dtos.enums.MyDtoWithEnumJsonFormat;
import com.ra.utility.jackson.enums.Distance;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class JacksonSerializationEnumsUnitTest {

    // tests - simple enum

    @Test
    public final void whenSerializingASimpleEnum_thenCorrect() throws JsonParseException, IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final String enumAsString = mapper.writeValueAsString(DistanceEnumSimple.MILE);

        assertThat(enumAsString).contains("MILE");
    }

    // tests - enum with main value

    @Test
    public final void whenSerializingAEnumWithValue_thenCorrect() throws JsonParseException, IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final String enumAsString = mapper.writeValueAsString(DistanceEnumWithValue.MILE);

        assertThat(enumAsString).contains("1609.34");
    }

    // tests - enum

    @Test
    public final void whenSerializingAnEnum_thenCorrect() throws JsonParseException, IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final String enumAsString = mapper.writeValueAsString(DistanceEnumWithJsonFormat.MILE);

        assertThat(enumAsString).contains("\"meters\":1609.34");
    }

    @Test
    public final void whenSerializingEntityWithEnum_thenCorrect() throws JsonParseException, IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final String enumAsString = mapper.writeValueAsString(new MyDtoWithEnumJsonFormat("a", 1, true, DistanceEnumWithJsonFormat.MILE));

        assertThat(enumAsString).contains("\"meters\":1609.34");
    }

    @Test
    public final void whenSerializingArrayOfEnums_thenCorrect() throws JsonParseException, IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final String json = mapper.writeValueAsString(new DistanceEnumWithJsonFormat[] { DistanceEnumWithJsonFormat.MILE, DistanceEnumWithJsonFormat.KILOMETER });

        assertThat(json).contains("\"meters\":1609.34");
    }

    // tests - enum with custom serializer

    @Test
    public final void givenCustomSerializer_whenSerializingEntityWithEnum_thenCorrect() throws JsonParseException, IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final String enumAsString = mapper.writeValueAsString(new MyDtoWithEnumCustom("a", 1, true, Distance.MILE));

        assertThat(enumAsString).contains("\"meters\":1609.34");
    }

}
