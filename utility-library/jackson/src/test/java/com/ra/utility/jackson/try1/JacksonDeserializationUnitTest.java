package com.ra.utility.jackson.try1;


import com.ra.utility.jackson.dtos.ItemWithSerializer;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class JacksonDeserializationUnitTest {

    @Test
    public final void givenDeserializerIsOnClass_whenDeserializingCustomRepresentation_thenCorrect() throws JsonParseException, JsonMappingException, IOException {
        final String json = "{\"id\":1,\"itemName\":\"theItem\",\"owner\":2}";

        final ItemWithSerializer readValue = new ObjectMapper().readValue(json, ItemWithSerializer.class);
        assertThat(readValue).isNotNull();
    }

}
