package com.ra.courses.lombok.samples;

import com.ra.courses.lombok.samples.getter.GetterBoolean;

public class Main {
    public static void main(String[] args) {
        var getterBoolean = GetterBoolean.builder()
                .authToken("Test")
                .samples(new String[]{})
                .running(false)
                .build();
    }
}
