package com.ra.courses.lombok.samples.getter;


import lombok.Builder;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.extern.java.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;

@Value
@Builder
@Log
public class GetterBoolean {

    private Boolean running;
    private String authToken;
    private String[] samples;

    public void doSomething() {
        log.log(Level.ALL, "Test");
    }

    @SneakyThrows(value = {IOException.class})
    public void doSomethingWithException() {
        var file = new File("");
        new FileInputStream(file);
    }
}
