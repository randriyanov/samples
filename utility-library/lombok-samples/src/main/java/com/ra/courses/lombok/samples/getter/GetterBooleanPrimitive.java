package com.ra.courses.lombok.samples.getter;

import lombok.Getter;

public class GetterBooleanPrimitive {

    @Getter
    private boolean running;

}
