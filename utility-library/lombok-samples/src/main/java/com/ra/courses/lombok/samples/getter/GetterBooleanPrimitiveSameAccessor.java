package com.ra.courses.lombok.samples.getter;


import lombok.Getter;

public class GetterBooleanPrimitiveSameAccessor {

    @Getter
    boolean running = true;

    @Getter
    boolean isRunning = false;
}
