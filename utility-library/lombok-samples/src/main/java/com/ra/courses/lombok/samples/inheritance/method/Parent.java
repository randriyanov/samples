package com.ra.courses.lombok.samples.inheritance.method;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Parent {    
    private final String parentName;
    private final int parentAge;
}