package com.ra.courses.lombok.samples.inheritance.superbuilder;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder(toBuilder = true)
public class Student extends Child {
    private String schoolName;
}
