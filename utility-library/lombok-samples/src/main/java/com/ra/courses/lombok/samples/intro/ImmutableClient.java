package com.ra.courses.lombok.samples.intro;

import lombok.Value;

@Value
public final class ImmutableClient {

    private int id;
    private String name;

}
