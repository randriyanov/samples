package com.ra.courses.lombok.samples;


import com.ra.courses.lombok.samples.singular.Person;
import com.ra.courses.lombok.samples.singular.Sea;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class BuilderWithSingularSupportForCollectionsUnitTest {

    @Test
    public void canAddMultipleElementsAsNewCollection() throws Exception {
        Person person = Person.builder()
            .givenName("Aaron")
            .additionalName("A")
            .familyName("Aardvark")
            .tags(Arrays.asList("fictional", "incidental"))
            .build();

    }

    @Test
    public void canUpdateCollectionAfterBuildIfMutableCollectionPassedToBuilder() throws Exception {

        List<String> tags = new ArrayList();
        tags.add("fictional");
        tags.add("incidental");
        Person person = Person.builder()
            .givenName("Aaron")
            .additionalName("A")
            .familyName("Aardvark")
            .tags(tags)
            .build();
        person.getTags()
            .clear();
        person.getTags()
            .add("non-fictional");
        person.getTags()
            .add("important");


    }

    @Test
    public void cannotUpdateCollectionAfterBuildIfImmutableCollectionPassedToBuilder() throws Exception {
        List<String> tags = Arrays.asList("fictional", "incidental");
        Person person = Person.builder()
            .givenName("Aaron")
            .additionalName("A")
            .familyName("Aardvark")
            .tags(tags)
            .build();

    }

    @Test
    public void canAssignToSingularAnnotatedCollectionOneByOne() throws Exception {

        Person person = Person.builder()
            .givenName("Aaron")
            .additionalName("A")
            .familyName("Aardvark")
            .interest("history")
            .interest("sport")
            .build();

    }

    @Test
    public void singularAnnotatedBuilderCreatesImmutableCollection() throws Exception {

        Person person = Person.builder()
            .givenName("Aaron")
            .additionalName("A")
            .familyName("Aardvark")
            .interest("history")
            .interest("sport")
            .build();
        person.getInterests()
            .clear();
    }

    @Test
    public void unpopulatedListsCreatedAsNullIfNotSingularButEmptyArrayIfSingular() throws Exception {

        Person person = Person.builder()
            .givenName("Aaron")
            .additionalName("A")
            .familyName("Aardvark")
            .build();

    }

    @Test
    public void singularSupportsSetsToo() throws Exception {

        Person person = Person.builder()
            .givenName("Aaron")
            .additionalName("A")
            .familyName("Aardvark")
            .skill("singing")
            .skill("dancing")
            .build();
    }

    @Test
    public void singularSetsAreLenientWithDuplicates() throws Exception {

        Person person = Person.builder()
            .givenName("Aaron")
            .additionalName("A")
            .familyName("Aardvark")
            .interest("singing")
            .interest("singing")
            .skill("singing")
            .skill("singing")
            .build();
    }

    @Test
    public void singularSupportsMapsToo() throws Exception {

        Person person = Person.builder()
            .givenName("Aaron")
            .additionalName("A")
            .familyName("Aardvark")
            .award("Singer of the Year", LocalDate.now()
                .minusYears(5))
            .award("Best Dancer", LocalDate.now()
                .minusYears(2))
            .build();
    }

    @Test
    public void singularIsLenientWithMapKeys() throws Exception {

        Person person = Person.builder()
            .givenName("Aaron")
            .additionalName("A")
            .familyName("Aardvark")
            .award("Best Dancer", LocalDate.now()
                .minusYears(5))
            .award("Best Dancer", LocalDate.now()
                .minusYears(4))
            .award("Best Dancer", LocalDate.now()
                .minusYears(3))
            .award("Best Dancer", LocalDate.now()
                .minusYears(2))
            .award("Best Dancer", LocalDate.now()
                .minusYears(1))
            .build();

    }

    @Test
    public void wordsWithNonStandardPlurals() throws Exception {
        Sea sea = Sea.builder()
            .grass("Dulse")
            .grass("Kelp")
            .oneFish("Cod")
            .oneFish("Mackerel")
            .build();
//        assertThat(sea.getGrasses(), contains("Dulse", "Kelp"));
//        assertThat(sea.getFish(), contains("Cod", "Mackerel"));
    }

}
